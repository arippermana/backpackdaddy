package com.app.backpackdaddy.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A AdmAuditTrail.
 */
@Entity
@Table(name = "adm_audit_trail")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "admaudittrail")
public class AdmAuditTrail extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "table_name", nullable = false)
    private String tableName;

    @NotNull
    @Column(name = "audit_status", nullable = false)
    private String auditStatus;

    @NotNull
    @Column(name = "old_values", nullable = false)
    private String oldValues;

    @NotNull
    @Column(name = "new_values", nullable = false)
    private String newValues;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTableName() {
        return tableName;
    }

    public AdmAuditTrail tableName(String tableName) {
        this.tableName = tableName;
        return this;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getAuditStatus() {
        return auditStatus;
    }

    public AdmAuditTrail auditStatus(String auditStatus) {
        this.auditStatus = auditStatus;
        return this;
    }

    public void setAuditStatus(String auditStatus) {
        this.auditStatus = auditStatus;
    }

    public String getOldValues() {
        return oldValues;
    }

    public AdmAuditTrail oldValues(String oldValues) {
        this.oldValues = oldValues;
        return this;
    }

    public void setOldValues(String oldValues) {
        this.oldValues = oldValues;
    }

    public String getNewValues() {
        return newValues;
    }

    public AdmAuditTrail newValues(String newValues) {
        this.newValues = newValues;
        return this;
    }

    public void setNewValues(String newValues) {
        this.newValues = newValues;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AdmAuditTrail admAuditTrail = (AdmAuditTrail) o;
        if (admAuditTrail.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), admAuditTrail.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AdmAuditTrail{" +
            "id=" + getId() +
            ", tableName='" + getTableName() + "'" +
            ", auditStatus='" + getAuditStatus() + "'" +
            ", oldValues='" + getOldValues() + "'" +
            ", newValues='" + getNewValues() + "'" +
            "}";
    }
}
