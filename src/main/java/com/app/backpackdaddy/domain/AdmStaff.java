package com.app.backpackdaddy.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A AdmStaff.
 */
@Entity
@Table(name = "adm_staff")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "admstaff")
public class AdmStaff extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "email", nullable = false)
    private String email;

    @NotNull
    @Column(name = "staff_name", nullable = false)
    private String staffName;

    @NotNull
    @Column(name = "jhi_password", nullable = false)
    private String password;

    @NotNull
    @Column(name = "hire_date", nullable = false)
    private ZonedDateTime hireDate;

    @NotNull
    @Column(name = "is_activated", nullable = false)
    private Boolean isActivated;

    @NotNull
    @Column(name = "is_active", nullable = false)
    private Boolean isActive;

    @ManyToOne(optional = false)
    @NotNull
    private AdmRefBranch branch;

    @ManyToOne(optional = false)
    @NotNull
    private AdmRefDepartment department;

    @ManyToOne(optional = false)
    @NotNull
    private AdmRefGroup group;

    @ManyToOne(optional = false)
    @NotNull
    private AdmRefPosition position;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public AdmStaff email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStaffName() {
        return staffName;
    }

    public AdmStaff staffName(String staffName) {
        this.staffName = staffName;
        return this;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public String getPassword() {
        return password;
    }

    public AdmStaff password(String password) {
        this.password = password;
        return this;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ZonedDateTime getHireDate() {
        return hireDate;
    }

    public AdmStaff hireDate(ZonedDateTime hireDate) {
        this.hireDate = hireDate;
        return this;
    }

    public void setHireDate(ZonedDateTime hireDate) {
        this.hireDate = hireDate;
    }

    public Boolean isIsActivated() {
        return isActivated;
    }

    public AdmStaff isActivated(Boolean isActivated) {
        this.isActivated = isActivated;
        return this;
    }

    public void setIsActivated(Boolean isActivated) {
        this.isActivated = isActivated;
    }

    public Boolean isIsActive() {
        return isActive;
    }

    public AdmStaff isActive(Boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public AdmRefBranch getBranch() {
        return branch;
    }

    public AdmStaff branch(AdmRefBranch admRefBranch) {
        this.branch = admRefBranch;
        return this;
    }

    public void setBranch(AdmRefBranch admRefBranch) {
        this.branch = admRefBranch;
    }

    public AdmRefDepartment getDepartment() {
        return department;
    }

    public AdmStaff department(AdmRefDepartment admRefDepartment) {
        this.department = admRefDepartment;
        return this;
    }

    public void setDepartment(AdmRefDepartment admRefDepartment) {
        this.department = admRefDepartment;
    }

    public AdmRefGroup getGroup() {
        return group;
    }

    public AdmStaff group(AdmRefGroup admRefGroup) {
        this.group = admRefGroup;
        return this;
    }

    public void setGroup(AdmRefGroup admRefGroup) {
        this.group = admRefGroup;
    }

    public AdmRefPosition getPosition() {
        return position;
    }

    public AdmStaff position(AdmRefPosition admRefPosition) {
        this.position = admRefPosition;
        return this;
    }

    public void setPosition(AdmRefPosition admRefPosition) {
        this.position = admRefPosition;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AdmStaff admStaff = (AdmStaff) o;
        if (admStaff.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), admStaff.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AdmStaff{" +
            "id=" + getId() +
            ", email='" + getEmail() + "'" +
            ", staffName='" + getStaffName() + "'" +
            ", password='" + getPassword() + "'" +
            ", hireDate='" + getHireDate() + "'" +
            ", isActivated='" + isIsActivated() + "'" +
            ", isActive='" + isIsActive() + "'" +
            "}";
    }
}
