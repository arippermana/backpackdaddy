package com.app.backpackdaddy.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A AttachmentDetails.
 */
@Entity
@Table(name = "trx_attachment_details")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "attachmentdetails")
public class AttachmentDetails extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "attachment_description", nullable = false)
    private String attachmentDescription;

    @NotNull
    @Column(name = "attachment_path", nullable = false)
    private String attachmentPath;

    @ManyToOne
    private Track track;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAttachmentDescription() {
        return attachmentDescription;
    }

    public AttachmentDetails attachmentDescription(String attachmentDescription) {
        this.attachmentDescription = attachmentDescription;
        return this;
    }

    public void setAttachmentDescription(String attachmentDescription) {
        this.attachmentDescription = attachmentDescription;
    }

    public String getAttachmentPath() {
        return attachmentPath;
    }

    public AttachmentDetails attachmentPath(String attachmentPath) {
        this.attachmentPath = attachmentPath;
        return this;
    }

    public void setAttachmentPath(String attachmentPath) {
        this.attachmentPath = attachmentPath;
    }

    public Track getTrack() {
        return track;
    }

    public AttachmentDetails track(Track track) {
        this.track = track;
        return this;
    }

    public void setTrack(Track track) {
        this.track = track;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AttachmentDetails attachmentDetails = (AttachmentDetails) o;
        if (attachmentDetails.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), attachmentDetails.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AttachmentDetails{" +
            "id=" + getId() +
            ", attachmentDescription='" + getAttachmentDescription() + "'" +
            ", attachmentPath='" + getAttachmentPath() + "'" +
            "}";
    }
}
