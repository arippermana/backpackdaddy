package com.app.backpackdaddy.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import com.app.backpackdaddy.domain.enumeration.ShippingMethod;

import com.app.backpackdaddy.domain.enumeration.PreferencePayment;

/**
 * A Request.
 */
@Entity
@Table(name = "trx_request")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "request")
public class Request extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "shipping_method")
    private ShippingMethod shippingMethod;

    @Column(name = "request_date")
    private LocalDate requestDate;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "address", nullable = false)
    private String address;

    @NotNull
    @Column(name = "product_price", nullable = false)
    private Double productPrice;

    @NotNull
    @Column(name = "product_quantity", nullable = false)
    private Integer productQuantity;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "preference_payment", nullable = false)
    private PreferencePayment preferencePayment;

    @NotNull
    @Column(name = "tip_amount", nullable = false)
    private Double tipAmount;

    @Column(name = "commision_fee")
    private Double commisionFee;

    @Column(name = "baggage_charge")
    private Double baggageCharge;

    @Column(name = "shipping_charge")
    private Double shippingCharge;

    @Column(name = "remarks")
    private String remarks;

    @Size(min = 1)
    @Column(name = "is_draft")
    private String isDraft;

    @ManyToOne
    private Product product;

    @ManyToOne
    private User user;

    @ManyToOne
    private City city;

    @ManyToOne
    private Currency tipCurrency;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ShippingMethod getShippingMethod() {
        return shippingMethod;
    }

    public Request shippingMethod(ShippingMethod shippingMethod) {
        this.shippingMethod = shippingMethod;
        return this;
    }

    public void setShippingMethod(ShippingMethod shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

    public LocalDate getRequestDate() {
        return requestDate;
    }

    public Request requestDate(LocalDate requestDate) {
        this.requestDate = requestDate;
        return this;
    }

    public void setRequestDate(LocalDate requestDate) {
        this.requestDate = requestDate;
    }

    public String getName() {
        return name;
    }

    public Request name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public Request address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getProductPrice() {
        return productPrice;
    }

    public Request productPrice(Double productPrice) {
        this.productPrice = productPrice;
        return this;
    }

    public void setProductPrice(Double productPrice) {
        this.productPrice = productPrice;
    }

    public Integer getProductQuantity() {
        return productQuantity;
    }

    public Request productQuantity(Integer productQuantity) {
        this.productQuantity = productQuantity;
        return this;
    }

    public void setProductQuantity(Integer productQuantity) {
        this.productQuantity = productQuantity;
    }

    public PreferencePayment getPreferencePayment() {
        return preferencePayment;
    }

    public Request preferencePayment(PreferencePayment preferencePayment) {
        this.preferencePayment = preferencePayment;
        return this;
    }

    public void setPreferencePayment(PreferencePayment preferencePayment) {
        this.preferencePayment = preferencePayment;
    }

    public Double getTipAmount() {
        return tipAmount;
    }

    public Request tipAmount(Double tipAmount) {
        this.tipAmount = tipAmount;
        return this;
    }

    public void setTipAmount(Double tipAmount) {
        this.tipAmount = tipAmount;
    }

    public Double getCommisionFee() {
        return commisionFee;
    }

    public Request commisionFee(Double commisionFee) {
        this.commisionFee = commisionFee;
        return this;
    }

    public void setCommisionFee(Double commisionFee) {
        this.commisionFee = commisionFee;
    }

    public Double getBaggageCharge() {
        return baggageCharge;
    }

    public Request baggageCharge(Double baggageCharge) {
        this.baggageCharge = baggageCharge;
        return this;
    }

    public void setBaggageCharge(Double baggageCharge) {
        this.baggageCharge = baggageCharge;
    }

    public Double getShippingCharge() {
        return shippingCharge;
    }

    public Request shippingCharge(Double shippingCharge) {
        this.shippingCharge = shippingCharge;
        return this;
    }

    public void setShippingCharge(Double shippingCharge) {
        this.shippingCharge = shippingCharge;
    }

    public String getRemarks() {
        return remarks;
    }

    public Request remarks(String remarks) {
        this.remarks = remarks;
        return this;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getIsDraft() {
        return isDraft;
    }

    public Request isDraft(String isDraft) {
        this.isDraft = isDraft;
        return this;
    }

    public void setIsDraft(String isDraft) {
        this.isDraft = isDraft;
    }

    public Product getProduct() {
        return product;
    }

    public Request product(Product product) {
        this.product = product;
        return this;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public User getUser() {
        return user;
    }

    public Request user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public City getCity() {
        return city;
    }

    public Request city(City city) {
        this.city = city;
        return this;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Currency getTipCurrency() {
        return tipCurrency;
    }

    public Request tipCurrency(Currency currency) {
        this.tipCurrency = currency;
        return this;
    }

    public void setTipCurrency(Currency currency) {
        this.tipCurrency = currency;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Request request = (Request) o;
        if (request.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), request.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Request{" +
            "id=" + getId() +
            ", shippingMethod='" + getShippingMethod() + "'" +
            ", requestDate='" + getRequestDate() + "'" +
            ", name='" + getName() + "'" +
            ", address='" + getAddress() + "'" +
            ", productPrice='" + getProductPrice() + "'" +
            ", productQuantity='" + getProductQuantity() + "'" +
            ", preferencePayment='" + getPreferencePayment() + "'" +
            ", tipAmount='" + getTipAmount() + "'" +
            ", commisionFee='" + getCommisionFee() + "'" +
            ", baggageCharge='" + getBaggageCharge() + "'" +
            ", shippingCharge='" + getShippingCharge() + "'" +
            ", remarks='" + getRemarks() + "'" +
            ", isDraft='" + getIsDraft() + "'" +
            "}";
    }
}
