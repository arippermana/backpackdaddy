package com.app.backpackdaddy.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Store.
 */
@Entity
@Table(name = "ref_store")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "store")
public class Store extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "offline_shop")
    private String offlineShop;

    @Column(name = "online_shop")
    private String onlineShop;

    @Column(name = "price")
    private Double price;

    @ManyToOne
    private Country country;

    @ManyToOne
    private Currency currency;

    @ManyToOne
    private Product product;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOfflineShop() {
        return offlineShop;
    }

    public Store offlineShop(String offlineShop) {
        this.offlineShop = offlineShop;
        return this;
    }

    public void setOfflineShop(String offlineShop) {
        this.offlineShop = offlineShop;
    }

    public String getOnlineShop() {
        return onlineShop;
    }

    public Store onlineShop(String onlineShop) {
        this.onlineShop = onlineShop;
        return this;
    }

    public void setOnlineShop(String onlineShop) {
        this.onlineShop = onlineShop;
    }

    public Double getPrice() {
        return price;
    }

    public Store price(Double price) {
        this.price = price;
        return this;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Country getCountry() {
        return country;
    }

    public Store country(Country country) {
        this.country = country;
        return this;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Currency getCurrency() {
        return currency;
    }

    public Store currency(Currency currency) {
        this.currency = currency;
        return this;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Product getProduct() {
        return product;
    }

    public Store product(Product product) {
        this.product = product;
        return this;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Store store = (Store) o;
        if (store.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), store.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Store{" +
            "id=" + getId() +
            ", offlineShop='" + getOfflineShop() + "'" +
            ", onlineShop='" + getOnlineShop() + "'" +
            ", price='" + getPrice() + "'" +
            "}";
    }
}
