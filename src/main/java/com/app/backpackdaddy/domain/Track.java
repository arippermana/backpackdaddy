package com.app.backpackdaddy.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Track.
 */
@Entity
@Table(name = "trx_track")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "track")
public class Track extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany(mappedBy = "track")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<AttachmentDetails> attachments = new HashSet<>();

    @ManyToOne
    private TransactionStatus status;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<AttachmentDetails> getAttachments() {
        return attachments;
    }

    public Track attachments(Set<AttachmentDetails> attachmentDetails) {
        this.attachments = attachmentDetails;
        return this;
    }

    public Track addAttachment(AttachmentDetails attachmentDetails) {
        this.attachments.add(attachmentDetails);
        attachmentDetails.setTrack(this);
        return this;
    }

    public Track removeAttachment(AttachmentDetails attachmentDetails) {
        this.attachments.remove(attachmentDetails);
        attachmentDetails.setTrack(null);
        return this;
    }

    public void setAttachments(Set<AttachmentDetails> attachmentDetails) {
        this.attachments = attachmentDetails;
    }

    public TransactionStatus getStatus() {
        return status;
    }

    public Track status(TransactionStatus transactionStatus) {
        this.status = transactionStatus;
        return this;
    }

    public void setStatus(TransactionStatus transactionStatus) {
        this.status = transactionStatus;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Track track = (Track) o;
        if (track.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), track.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Track{" +
            "id=" + getId() +
            "}";
    }
}
