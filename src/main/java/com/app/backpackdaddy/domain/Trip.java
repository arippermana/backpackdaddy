package com.app.backpackdaddy.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import com.app.backpackdaddy.domain.enumeration.PreferencePayment;

/**
 * A Trip.
 */
@Entity
@Table(name = "trx_trip")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "trip")
public class Trip extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "weight")
    private Integer weight;

    @Size(min = 1)
    @Column(name = "notice")
    private String notice;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "preference_payment", nullable = false)
    private PreferencePayment preferencePayment;

    @Column(name = "range_from")
    private Double rangeFrom;

    @Column(name = "range_to")
    private Double rangeTo;

    @Column(name = "fixed_amount")
    private Double fixedAmount;

    @Column(name = "percentage_amount")
    private Long percentageAmount;

    @Size(min = 1)
    @Column(name = "baggage_information")
    private String baggageInformation;

    @Column(name = "ship_date")
    private LocalDate shipDate;

    @Column(name = "remarks")
    private String remarks;

    @Size(min = 1)
    @Column(name = "is_draft")
    private String isDraft;

    @ManyToOne
    private User user;

    @ManyToOne
    private Country fromCountry;

    @ManyToOne
    private Country toCountry;

    @ManyToOne
    private Category category;

    @ManyToOne
    private Currency paymentCurrency;

    @ManyToOne
    private City shippingFrom;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getWeight() {
        return weight;
    }

    public Trip weight(Integer weight) {
        this.weight = weight;
        return this;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public String getNotice() {
        return notice;
    }

    public Trip notice(String notice) {
        this.notice = notice;
        return this;
    }

    public void setNotice(String notice) {
        this.notice = notice;
    }

    public PreferencePayment getPreferencePayment() {
        return preferencePayment;
    }

    public Trip preferencePayment(PreferencePayment preferencePayment) {
        this.preferencePayment = preferencePayment;
        return this;
    }

    public void setPreferencePayment(PreferencePayment preferencePayment) {
        this.preferencePayment = preferencePayment;
    }

    public Double getRangeFrom() {
        return rangeFrom;
    }

    public Trip rangeFrom(Double rangeFrom) {
        this.rangeFrom = rangeFrom;
        return this;
    }

    public void setRangeFrom(Double rangeFrom) {
        this.rangeFrom = rangeFrom;
    }

    public Double getRangeTo() {
        return rangeTo;
    }

    public Trip rangeTo(Double rangeTo) {
        this.rangeTo = rangeTo;
        return this;
    }

    public void setRangeTo(Double rangeTo) {
        this.rangeTo = rangeTo;
    }

    public Double getFixedAmount() {
        return fixedAmount;
    }

    public Trip fixedAmount(Double fixedAmount) {
        this.fixedAmount = fixedAmount;
        return this;
    }

    public void setFixedAmount(Double fixedAmount) {
        this.fixedAmount = fixedAmount;
    }

    public Long getPercentageAmount() {
        return percentageAmount;
    }

    public Trip percentageAmount(Long percentageAmount) {
        this.percentageAmount = percentageAmount;
        return this;
    }

    public void setPercentageAmount(Long percentageAmount) {
        this.percentageAmount = percentageAmount;
    }

    public String getBaggageInformation() {
        return baggageInformation;
    }

    public Trip baggageInformation(String baggageInformation) {
        this.baggageInformation = baggageInformation;
        return this;
    }

    public void setBaggageInformation(String baggageInformation) {
        this.baggageInformation = baggageInformation;
    }

    public LocalDate getShipDate() {
        return shipDate;
    }

    public Trip shipDate(LocalDate shipDate) {
        this.shipDate = shipDate;
        return this;
    }

    public void setShipDate(LocalDate shipDate) {
        this.shipDate = shipDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public Trip remarks(String remarks) {
        this.remarks = remarks;
        return this;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getIsDraft() {
        return isDraft;
    }

    public Trip isDraft(String isDraft) {
        this.isDraft = isDraft;
        return this;
    }

    public void setIsDraft(String isDraft) {
        this.isDraft = isDraft;
    }

    public User getUser() {
        return user;
    }

    public Trip user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Country getFromCountry() {
        return fromCountry;
    }

    public Trip fromCountry(Country country) {
        this.fromCountry = country;
        return this;
    }

    public void setFromCountry(Country country) {
        this.fromCountry = country;
    }

    public Country getToCountry() {
        return toCountry;
    }

    public Trip toCountry(Country country) {
        this.toCountry = country;
        return this;
    }

    public void setToCountry(Country country) {
        this.toCountry = country;
    }

    public Category getCategory() {
        return category;
    }

    public Trip category(Category category) {
        this.category = category;
        return this;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Currency getPaymentCurrency() {
        return paymentCurrency;
    }

    public Trip paymentCurrency(Currency currency) {
        this.paymentCurrency = currency;
        return this;
    }

    public void setPaymentCurrency(Currency currency) {
        this.paymentCurrency = currency;
    }

    public City getShippingFrom() {
        return shippingFrom;
    }

    public Trip shippingFrom(City city) {
        this.shippingFrom = city;
        return this;
    }

    public void setShippingFrom(City city) {
        this.shippingFrom = city;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Trip trip = (Trip) o;
        if (trip.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), trip.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Trip{" +
            "id=" + getId() +
            ", weight='" + getWeight() + "'" +
            ", notice='" + getNotice() + "'" +
            ", preferencePayment='" + getPreferencePayment() + "'" +
            ", rangeFrom='" + getRangeFrom() + "'" +
            ", rangeTo='" + getRangeTo() + "'" +
            ", fixedAmount='" + getFixedAmount() + "'" +
            ", percentageAmount='" + getPercentageAmount() + "'" +
            ", baggageInformation='" + getBaggageInformation() + "'" +
            ", shipDate='" + getShipDate() + "'" +
            ", remarks='" + getRemarks() + "'" +
            ", isDraft='" + getIsDraft() + "'" +
            "}";
    }
}
