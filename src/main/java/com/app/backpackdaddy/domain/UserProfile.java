package com.app.backpackdaddy.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A UserProfile.
 */
@Entity
@Table(name = "user_profile")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "userprofile")
public class UserProfile implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "birthdate", nullable = false)
    private LocalDate birthdate;

    @NotNull
    @Column(name = "is_age_show", nullable = false)
    private Boolean isAgeShow;

    @Column(name = "is_working_oversea")
    private Boolean isWorkingOversea;

    @Column(name = "profile_image")
    private String profileImage;

    @ManyToOne(optional = false)
    @NotNull
    private Gender gender;

    @ManyToOne(optional = false)
    @NotNull
    private Country nationality;

    @ManyToOne(optional = false)
    @NotNull
    private Country currentLocation;

    @ManyToOne(optional = false)
    @NotNull
    private Occupation occupation;

    @ManyToOne(optional = false)
    @NotNull
    private Language language;

    @ManyToOne(optional = false)
    @NotNull
    private Currency currency;

    @ManyToOne(optional = false)
    @NotNull
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public UserProfile name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirthdate() {
        return birthdate;
    }

    public UserProfile birthdate(LocalDate birthdate) {
        this.birthdate = birthdate;
        return this;
    }

    public void setBirthdate(LocalDate birthdate) {
        this.birthdate = birthdate;
    }

    public Boolean isIsAgeShow() {
        return isAgeShow;
    }

    public UserProfile isAgeShow(Boolean isAgeShow) {
        this.isAgeShow = isAgeShow;
        return this;
    }

    public void setIsAgeShow(Boolean isAgeShow) {
        this.isAgeShow = isAgeShow;
    }

    public Boolean isIsWorkingOversea() {
        return isWorkingOversea;
    }

    public UserProfile isWorkingOversea(Boolean isWorkingOversea) {
        this.isWorkingOversea = isWorkingOversea;
        return this;
    }

    public void setIsWorkingOversea(Boolean isWorkingOversea) {
        this.isWorkingOversea = isWorkingOversea;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public UserProfile profileImage(String profileImage) {
        this.profileImage = profileImage;
        return this;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public Gender getGender() {
        return gender;
    }

    public UserProfile gender(Gender gender) {
        this.gender = gender;
        return this;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Country getNationality() {
        return nationality;
    }

    public UserProfile nationality(Country country) {
        this.nationality = country;
        return this;
    }

    public void setNationality(Country country) {
        this.nationality = country;
    }

    public Country getCurrentLocation() {
        return currentLocation;
    }

    public UserProfile currentLocation(Country country) {
        this.currentLocation = country;
        return this;
    }

    public void setCurrentLocation(Country country) {
        this.currentLocation = country;
    }

    public Occupation getOccupation() {
        return occupation;
    }

    public UserProfile occupation(Occupation occupation) {
        this.occupation = occupation;
        return this;
    }

    public void setOccupation(Occupation occupation) {
        this.occupation = occupation;
    }

    public Language getLanguage() {
        return language;
    }

    public UserProfile language(Language language) {
        this.language = language;
        return this;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Currency getCurrency() {
        return currency;
    }

    public UserProfile currency(Currency currency) {
        this.currency = currency;
        return this;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public User getUser() {
        return user;
    }

    public UserProfile user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserProfile userProfile = (UserProfile) o;
        if (userProfile.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), userProfile.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UserProfile{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", birthdate='" + getBirthdate() + "'" +
            ", isAgeShow='" + isIsAgeShow() + "'" +
            ", isWorkingOversea='" + isIsWorkingOversea() + "'" +
            ", profileImage='" + getProfileImage() + "'" +
            "}";
    }
}
