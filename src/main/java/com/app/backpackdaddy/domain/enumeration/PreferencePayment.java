package com.app.backpackdaddy.domain.enumeration;

/**
 * The PreferencePayment enumeration.
 */
public enum PreferencePayment {
    NEGOTIABLE, RANGE, FIXED, PERCENTAGE
}
