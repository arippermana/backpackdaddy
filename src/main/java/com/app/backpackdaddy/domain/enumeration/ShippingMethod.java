package com.app.backpackdaddy.domain.enumeration;

/**
 * The ShippingMethod enumeration.
 */
public enum ShippingMethod {
    DIRECT_TRAVELLER, COMPANY_SERVICE
}
