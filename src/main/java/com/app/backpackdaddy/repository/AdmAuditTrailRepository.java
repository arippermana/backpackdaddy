package com.app.backpackdaddy.repository;

import com.app.backpackdaddy.domain.AdmAuditTrail;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the AdmAuditTrail entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AdmAuditTrailRepository extends JpaRepository<AdmAuditTrail, Long> {

}
