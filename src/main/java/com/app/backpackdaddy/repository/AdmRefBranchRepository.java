package com.app.backpackdaddy.repository;

import com.app.backpackdaddy.domain.AdmRefBranch;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the AdmRefBranch entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AdmRefBranchRepository extends JpaRepository<AdmRefBranch, Long> {

}
