package com.app.backpackdaddy.repository;

import com.app.backpackdaddy.domain.AdmRefDepartment;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the AdmRefDepartment entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AdmRefDepartmentRepository extends JpaRepository<AdmRefDepartment, Long> {

}
