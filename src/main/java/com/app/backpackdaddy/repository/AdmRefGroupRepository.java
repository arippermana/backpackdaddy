package com.app.backpackdaddy.repository;

import com.app.backpackdaddy.domain.AdmRefGroup;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the AdmRefGroup entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AdmRefGroupRepository extends JpaRepository<AdmRefGroup, Long> {

}
