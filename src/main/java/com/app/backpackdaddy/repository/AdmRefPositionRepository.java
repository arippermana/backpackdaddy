package com.app.backpackdaddy.repository;

import com.app.backpackdaddy.domain.AdmRefPosition;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the AdmRefPosition entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AdmRefPositionRepository extends JpaRepository<AdmRefPosition, Long> {

}
