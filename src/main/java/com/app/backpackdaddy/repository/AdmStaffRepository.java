package com.app.backpackdaddy.repository;

import com.app.backpackdaddy.domain.AdmStaff;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the AdmStaff entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AdmStaffRepository extends JpaRepository<AdmStaff, Long> {

}
