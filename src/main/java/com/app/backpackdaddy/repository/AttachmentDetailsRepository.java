package com.app.backpackdaddy.repository;

import com.app.backpackdaddy.domain.AttachmentDetails;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the AttachmentDetails entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AttachmentDetailsRepository extends JpaRepository<AttachmentDetails, Long> {

}
