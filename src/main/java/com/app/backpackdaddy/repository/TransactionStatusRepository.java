package com.app.backpackdaddy.repository;

import com.app.backpackdaddy.domain.TransactionStatus;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the TransactionStatus entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TransactionStatusRepository extends JpaRepository<TransactionStatus, Long> {

}
