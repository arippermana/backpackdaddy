package com.app.backpackdaddy.repository.search;

import com.app.backpackdaddy.domain.AdmAuditTrail;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the AdmAuditTrail entity.
 */
public interface AdmAuditTrailSearchRepository extends ElasticsearchRepository<AdmAuditTrail, Long> {
}
