package com.app.backpackdaddy.repository.search;

import com.app.backpackdaddy.domain.AdmRefBranch;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the AdmRefBranch entity.
 */
public interface AdmRefBranchSearchRepository extends ElasticsearchRepository<AdmRefBranch, Long> {
}
