package com.app.backpackdaddy.repository.search;

import com.app.backpackdaddy.domain.AdmRefDepartment;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the AdmRefDepartment entity.
 */
public interface AdmRefDepartmentSearchRepository extends ElasticsearchRepository<AdmRefDepartment, Long> {
}
