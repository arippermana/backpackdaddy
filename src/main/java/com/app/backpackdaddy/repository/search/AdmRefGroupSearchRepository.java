package com.app.backpackdaddy.repository.search;

import com.app.backpackdaddy.domain.AdmRefGroup;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the AdmRefGroup entity.
 */
public interface AdmRefGroupSearchRepository extends ElasticsearchRepository<AdmRefGroup, Long> {
}
