package com.app.backpackdaddy.repository.search;

import com.app.backpackdaddy.domain.AdmRefPosition;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the AdmRefPosition entity.
 */
public interface AdmRefPositionSearchRepository extends ElasticsearchRepository<AdmRefPosition, Long> {
}
