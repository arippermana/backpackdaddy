package com.app.backpackdaddy.repository.search;

import com.app.backpackdaddy.domain.AdmStaff;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the AdmStaff entity.
 */
public interface AdmStaffSearchRepository extends ElasticsearchRepository<AdmStaff, Long> {
}
