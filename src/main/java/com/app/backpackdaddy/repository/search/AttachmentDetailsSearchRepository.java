package com.app.backpackdaddy.repository.search;

import com.app.backpackdaddy.domain.AttachmentDetails;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the AttachmentDetails entity.
 */
public interface AttachmentDetailsSearchRepository extends ElasticsearchRepository<AttachmentDetails, Long> {
}
