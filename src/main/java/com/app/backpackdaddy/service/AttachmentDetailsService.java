package com.app.backpackdaddy.service;

import com.app.backpackdaddy.service.dto.AttachmentDetailsDTO;
import java.util.List;

/**
 * Service Interface for managing AttachmentDetails.
 */
public interface AttachmentDetailsService {

    /**
     * Save a attachmentDetails.
     *
     * @param attachmentDetailsDTO the entity to save
     * @return the persisted entity
     */
    AttachmentDetailsDTO save(AttachmentDetailsDTO attachmentDetailsDTO);

    /**
     *  Get all the attachmentDetails.
     *
     *  @return the list of entities
     */
    List<AttachmentDetailsDTO> findAll();

    /**
     *  Get the "id" attachmentDetails.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    AttachmentDetailsDTO findOne(Long id);

    /**
     *  Delete the "id" attachmentDetails.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the attachmentDetails corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @return the list of entities
     */
    List<AttachmentDetailsDTO> search(String query);
}
