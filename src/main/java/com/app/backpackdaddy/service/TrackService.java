package com.app.backpackdaddy.service;

import com.app.backpackdaddy.service.dto.TrackDTO;
import java.util.List;

/**
 * Service Interface for managing Track.
 */
public interface TrackService {

    /**
     * Save a track.
     *
     * @param trackDTO the entity to save
     * @return the persisted entity
     */
    TrackDTO save(TrackDTO trackDTO);

    /**
     *  Get all the tracks.
     *
     *  @return the list of entities
     */
    List<TrackDTO> findAll();

    /**
     *  Get the "id" track.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    TrackDTO findOne(Long id);

    /**
     *  Delete the "id" track.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the track corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @return the list of entities
     */
    List<TrackDTO> search(String query);
}
