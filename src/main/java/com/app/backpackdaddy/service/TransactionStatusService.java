package com.app.backpackdaddy.service;

import com.app.backpackdaddy.service.dto.TransactionStatusDTO;
import java.util.List;

/**
 * Service Interface for managing TransactionStatus.
 */
public interface TransactionStatusService {

    /**
     * Save a transactionStatus.
     *
     * @param transactionStatusDTO the entity to save
     * @return the persisted entity
     */
    TransactionStatusDTO save(TransactionStatusDTO transactionStatusDTO);

    /**
     *  Get all the transactionStatuses.
     *
     *  @return the list of entities
     */
    List<TransactionStatusDTO> findAll();

    /**
     *  Get the "id" transactionStatus.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    TransactionStatusDTO findOne(Long id);

    /**
     *  Delete the "id" transactionStatus.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the transactionStatus corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @return the list of entities
     */
    List<TransactionStatusDTO> search(String query);
}
