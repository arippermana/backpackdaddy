package com.app.backpackdaddy.service;

import com.app.backpackdaddy.domain.User;
import com.app.backpackdaddy.service.dto.UserProfileDTO;
import com.app.backpackdaddy.web.rest.vm.ManagedUserProfileVM;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing UserProfile.
 */
public interface UserProfileService {

    /**
     * Save a userProfile.
     *
     * @param userProfileDTO the entity to save
     * @return the persisted entity
     */
    UserProfileDTO save(UserProfileDTO userProfileDTO);

    Optional<User> activate(ManagedUserProfileVM managedUserProfileVM);

    /**
     *  Get all the userProfiles.
     *
     *  @return the list of entities
     */
    List<UserProfileDTO> findAll();

    /**
     *  Get the "id" userProfile.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    UserProfileDTO findOne(Long id);

    /**
     *  Delete the "id" userProfile.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the userProfile corresponding to the query.
     *
     *  @param query the query of the search
     *
     *  @return the list of entities
     */
    List<UserProfileDTO> search(String query);
}
