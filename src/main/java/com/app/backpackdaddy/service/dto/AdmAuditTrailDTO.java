package com.app.backpackdaddy.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the AdmAuditTrail entity.
 */
public class AdmAuditTrailDTO extends AbstractAuditingDTO implements Serializable {

    private Long id;

    @NotNull
    private String tableName;

    @NotNull
    private String auditStatus;

    @NotNull
    private String oldValues;

    @NotNull
    private String newValues;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(String auditStatus) {
        this.auditStatus = auditStatus;
    }

    public String getOldValues() {
        return oldValues;
    }

    public void setOldValues(String oldValues) {
        this.oldValues = oldValues;
    }

    public String getNewValues() {
        return newValues;
    }

    public void setNewValues(String newValues) {
        this.newValues = newValues;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AdmAuditTrailDTO admAuditTrailDTO = (AdmAuditTrailDTO) o;
        if(admAuditTrailDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), admAuditTrailDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AdmAuditTrailDTO{" +
            "id=" + getId() +
            ", tableName='" + getTableName() + "'" +
            ", auditStatus='" + getAuditStatus() + "'" +
            ", oldValues='" + getOldValues() + "'" +
            ", newValues='" + getNewValues() + "'" +
            "}";
    }
}
