package com.app.backpackdaddy.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the AdmRefBranch entity.
 */
public class AdmRefBranchDTO implements Serializable {

    private Long id;

    @NotNull
    private String branchName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AdmRefBranchDTO admRefBranchDTO = (AdmRefBranchDTO) o;
        if(admRefBranchDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), admRefBranchDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AdmRefBranchDTO{" +
            "id=" + getId() +
            ", branchName='" + getBranchName() + "'" +
            "}";
    }
}
