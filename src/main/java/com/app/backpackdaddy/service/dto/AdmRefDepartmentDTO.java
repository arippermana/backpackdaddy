package com.app.backpackdaddy.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the AdmRefDepartment entity.
 */
public class AdmRefDepartmentDTO implements Serializable {

    private Long id;

    @NotNull
    private String departmentName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AdmRefDepartmentDTO admRefDepartmentDTO = (AdmRefDepartmentDTO) o;
        if(admRefDepartmentDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), admRefDepartmentDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AdmRefDepartmentDTO{" +
            "id=" + getId() +
            ", departmentName='" + getDepartmentName() + "'" +
            "}";
    }
}
