package com.app.backpackdaddy.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the AdmRefGroup entity.
 */
public class AdmRefGroupDTO implements Serializable {

    private Long id;

    @NotNull
    private String groupName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AdmRefGroupDTO admRefGroupDTO = (AdmRefGroupDTO) o;
        if(admRefGroupDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), admRefGroupDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AdmRefGroupDTO{" +
            "id=" + getId() +
            ", groupName='" + getGroupName() + "'" +
            "}";
    }
}
