package com.app.backpackdaddy.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the AdmRefPosition entity.
 */
public class AdmRefPositionDTO implements Serializable {

    private Long id;

    @NotNull
    private String positionName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AdmRefPositionDTO admRefPositionDTO = (AdmRefPositionDTO) o;
        if(admRefPositionDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), admRefPositionDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AdmRefPositionDTO{" +
            "id=" + getId() +
            ", positionName='" + getPositionName() + "'" +
            "}";
    }
}
