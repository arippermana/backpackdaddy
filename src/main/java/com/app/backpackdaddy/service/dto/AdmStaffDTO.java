package com.app.backpackdaddy.service.dto;


import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the AdmStaff entity.
 */
public class AdmStaffDTO extends AbstractAuditingDTO implements Serializable {

    private Long id;

    @NotNull
    private String email;

    @NotNull
    private String staffName;

    @NotNull
    private String password;

    @NotNull
    private ZonedDateTime hireDate;

    @NotNull
    private Boolean isActivated;

    @NotNull
    private Boolean isActive;

    private Long branchId;

    private String branchBranchName;

    private Long departmentId;

    private String departmentDepartmentName;

    private Long groupId;

    private String groupGroupName;

    private Long positionId;

    private String positionPositionName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ZonedDateTime getHireDate() {
        return hireDate;
    }

    public void setHireDate(ZonedDateTime hireDate) {
        this.hireDate = hireDate;
    }

    public Boolean isIsActivated() {
        return isActivated;
    }

    public void setIsActivated(Boolean isActivated) {
        this.isActivated = isActivated;
    }

    public Boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Long getBranchId() {
        return branchId;
    }

    public void setBranchId(Long admRefBranchId) {
        this.branchId = admRefBranchId;
    }

    public String getBranchBranchName() {
        return branchBranchName;
    }

    public void setBranchBranchName(String admRefBranchBranchName) {
        this.branchBranchName = admRefBranchBranchName;
    }

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Long admRefDepartmentId) {
        this.departmentId = admRefDepartmentId;
    }

    public String getDepartmentDepartmentName() {
        return departmentDepartmentName;
    }

    public void setDepartmentDepartmentName(String admRefDepartmentDepartmentName) {
        this.departmentDepartmentName = admRefDepartmentDepartmentName;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long admRefGroupId) {
        this.groupId = admRefGroupId;
    }

    public String getGroupGroupName() {
        return groupGroupName;
    }

    public void setGroupGroupName(String admRefGroupGroupName) {
        this.groupGroupName = admRefGroupGroupName;
    }

    public Long getPositionId() {
        return positionId;
    }

    public void setPositionId(Long admRefPositionId) {
        this.positionId = admRefPositionId;
    }

    public String getPositionPositionName() {
        return positionPositionName;
    }

    public void setPositionPositionName(String admRefPositionPositionName) {
        this.positionPositionName = admRefPositionPositionName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AdmStaffDTO admStaffDTO = (AdmStaffDTO) o;
        if(admStaffDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), admStaffDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AdmStaffDTO{" +
            "id=" + getId() +
            ", email='" + getEmail() + "'" +
            ", staffName='" + getStaffName() + "'" +
            ", password='" + getPassword() + "'" +
            ", hireDate='" + getHireDate() + "'" +
            ", isActivated='" + isIsActivated() + "'" +
            ", isActive='" + isIsActive() + "'" +
            "}";
    }
}
