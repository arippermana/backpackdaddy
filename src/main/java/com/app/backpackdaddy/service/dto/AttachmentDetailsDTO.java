package com.app.backpackdaddy.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the AttachmentDetails entity.
 */
public class AttachmentDetailsDTO extends AbstractAuditingDTO implements Serializable {

    private Long id;

    @NotNull
    private String attachmentDescription;

    @NotNull
    private String attachmentPath;

    private Long trackId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAttachmentDescription() {
        return attachmentDescription;
    }

    public void setAttachmentDescription(String attachmentDescription) {
        this.attachmentDescription = attachmentDescription;
    }

    public String getAttachmentPath() {
        return attachmentPath;
    }

    public void setAttachmentPath(String attachmentPath) {
        this.attachmentPath = attachmentPath;
    }

    public Long getTrackId() {
        return trackId;
    }

    public void setTrackId(Long trackId) {
        this.trackId = trackId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AttachmentDetailsDTO attachmentDetailsDTO = (AttachmentDetailsDTO) o;
        if(attachmentDetailsDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), attachmentDetailsDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AttachmentDetailsDTO{" +
            "id=" + getId() +
            ", attachmentDescription='" + getAttachmentDescription() + "'" +
            ", attachmentPath='" + getAttachmentPath() + "'" +
            "}";
    }
}
