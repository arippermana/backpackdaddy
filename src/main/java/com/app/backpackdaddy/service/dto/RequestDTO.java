package com.app.backpackdaddy.service.dto;


import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import com.app.backpackdaddy.domain.enumeration.ShippingMethod;
import com.app.backpackdaddy.domain.enumeration.PreferencePayment;

/**
 * A DTO for the Request entity.
 */
public class RequestDTO extends AbstractAuditingDTO implements Serializable {

    private Long id;

    private ShippingMethod shippingMethod;

    private LocalDate requestDate;

    @NotNull
    private String name;

    @NotNull
    private String address;

    @NotNull
    private Double productPrice;

    @NotNull
    private Integer productQuantity;

    @NotNull
    private PreferencePayment preferencePayment;

    @NotNull
    private Double tipAmount;

    private Double commisionFee;

    private Double baggageCharge;

    private Double shippingCharge;

    private String remarks;

    @Size(min = 1)
    private String isDraft;

    private Long productId;

    private Long userId;

    private Long cityId;

    private Long tipCurrencyId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ShippingMethod getShippingMethod() {
        return shippingMethod;
    }

    public void setShippingMethod(ShippingMethod shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

    public LocalDate getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(LocalDate requestDate) {
        this.requestDate = requestDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(Double productPrice) {
        this.productPrice = productPrice;
    }

    public Integer getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(Integer productQuantity) {
        this.productQuantity = productQuantity;
    }

    public PreferencePayment getPreferencePayment() {
        return preferencePayment;
    }

    public void setPreferencePayment(PreferencePayment preferencePayment) {
        this.preferencePayment = preferencePayment;
    }

    public Double getTipAmount() {
        return tipAmount;
    }

    public void setTipAmount(Double tipAmount) {
        this.tipAmount = tipAmount;
    }

    public Double getCommisionFee() {
        return commisionFee;
    }

    public void setCommisionFee(Double commisionFee) {
        this.commisionFee = commisionFee;
    }

    public Double getBaggageCharge() {
        return baggageCharge;
    }

    public void setBaggageCharge(Double baggageCharge) {
        this.baggageCharge = baggageCharge;
    }

    public Double getShippingCharge() {
        return shippingCharge;
    }

    public void setShippingCharge(Double shippingCharge) {
        this.shippingCharge = shippingCharge;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getIsDraft() {
        return isDraft;
    }

    public void setIsDraft(String isDraft) {
        this.isDraft = isDraft;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public Long getTipCurrencyId() {
        return tipCurrencyId;
    }

    public void setTipCurrencyId(Long currencyId) {
        this.tipCurrencyId = currencyId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RequestDTO requestDTO = (RequestDTO) o;
        if(requestDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), requestDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RequestDTO{" +
            "id=" + getId() +
            ", shippingMethod='" + getShippingMethod() + "'" +
            ", requestDate='" + getRequestDate() + "'" +
            ", name='" + getName() + "'" +
            ", address='" + getAddress() + "'" +
            ", productPrice='" + getProductPrice() + "'" +
            ", productQuantity='" + getProductQuantity() + "'" +
            ", preferencePayment='" + getPreferencePayment() + "'" +
            ", tipAmount='" + getTipAmount() + "'" +
            ", commisionFee='" + getCommisionFee() + "'" +
            ", baggageCharge='" + getBaggageCharge() + "'" +
            ", shippingCharge='" + getShippingCharge() + "'" +
            ", remarks='" + getRemarks() + "'" +
            ", isDraft='" + getIsDraft() + "'" +
            "}";
    }
}
