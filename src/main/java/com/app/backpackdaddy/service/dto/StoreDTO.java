package com.app.backpackdaddy.service.dto;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Store entity.
 */
public class StoreDTO extends AbstractAuditingDTO implements Serializable {

    private Long id;

    private String offlineShop;

    private String onlineShop;

    private Double price;

    private Long countryId;

    private Long currencyId;

    private Long productId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOfflineShop() {
        return offlineShop;
    }

    public void setOfflineShop(String offlineShop) {
        this.offlineShop = offlineShop;
    }

    public String getOnlineShop() {
        return onlineShop;
    }

    public void setOnlineShop(String onlineShop) {
        this.onlineShop = onlineShop;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Long getCountryId() {
        return countryId;
    }

    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }

    public Long getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Long currencyId) {
        this.currencyId = currencyId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        StoreDTO storeDTO = (StoreDTO) o;
        if(storeDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), storeDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "StoreDTO{" +
            "id=" + getId() +
            ", offlineShop='" + getOfflineShop() + "'" +
            ", onlineShop='" + getOnlineShop() + "'" +
            ", price='" + getPrice() + "'" +
            "}";
    }
}
