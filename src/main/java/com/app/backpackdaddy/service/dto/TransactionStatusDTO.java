package com.app.backpackdaddy.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the TransactionStatus entity.
 */
public class TransactionStatusDTO extends AbstractAuditingDTO implements Serializable {

    private Long id;

    private String description;

    @Size(min = 1)
    private String flagId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFlagId() {
        return flagId;
    }

    public void setFlagId(String flagId) {
        this.flagId = flagId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TransactionStatusDTO transactionStatusDTO = (TransactionStatusDTO) o;
        if(transactionStatusDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), transactionStatusDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TransactionStatusDTO{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            ", flagId='" + getFlagId() + "'" +
            "}";
    }
}
