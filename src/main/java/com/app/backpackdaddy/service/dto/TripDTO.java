package com.app.backpackdaddy.service.dto;


import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import com.app.backpackdaddy.domain.enumeration.PreferencePayment;

/**
 * A DTO for the Trip entity.
 */
public class TripDTO extends AbstractAuditingDTO implements Serializable {

    private Long id;

    private Integer weight;

    @Size(min = 1)
    private String notice;

    @NotNull
    private PreferencePayment preferencePayment;

    private Double rangeFrom;

    private Double rangeTo;

    private Double fixedAmount;

    private Long percentageAmount;

    @Size(min = 1)
    private String baggageInformation;

    private LocalDate shipDate;

    private String remarks;

    @Size(min = 1)
    private String isDraft;

    private Long userId;

    private Long fromCountryId;

    private Long toCountryId;

    private Long categoryId;

    private Long paymentCurrencyId;

    private Long shippingFromId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public String getNotice() {
        return notice;
    }

    public void setNotice(String notice) {
        this.notice = notice;
    }

    public PreferencePayment getPreferencePayment() {
        return preferencePayment;
    }

    public void setPreferencePayment(PreferencePayment preferencePayment) {
        this.preferencePayment = preferencePayment;
    }

    public Double getRangeFrom() {
        return rangeFrom;
    }

    public void setRangeFrom(Double rangeFrom) {
        this.rangeFrom = rangeFrom;
    }

    public Double getRangeTo() {
        return rangeTo;
    }

    public void setRangeTo(Double rangeTo) {
        this.rangeTo = rangeTo;
    }

    public Double getFixedAmount() {
        return fixedAmount;
    }

    public void setFixedAmount(Double fixedAmount) {
        this.fixedAmount = fixedAmount;
    }

    public Long getPercentageAmount() {
        return percentageAmount;
    }

    public void setPercentageAmount(Long percentageAmount) {
        this.percentageAmount = percentageAmount;
    }

    public String getBaggageInformation() {
        return baggageInformation;
    }

    public void setBaggageInformation(String baggageInformation) {
        this.baggageInformation = baggageInformation;
    }

    public LocalDate getShipDate() {
        return shipDate;
    }

    public void setShipDate(LocalDate shipDate) {
        this.shipDate = shipDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getIsDraft() {
        return isDraft;
    }

    public void setIsDraft(String isDraft) {
        this.isDraft = isDraft;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getFromCountryId() {
        return fromCountryId;
    }

    public void setFromCountryId(Long countryId) {
        this.fromCountryId = countryId;
    }

    public Long getToCountryId() {
        return toCountryId;
    }

    public void setToCountryId(Long countryId) {
        this.toCountryId = countryId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getPaymentCurrencyId() {
        return paymentCurrencyId;
    }

    public void setPaymentCurrencyId(Long currencyId) {
        this.paymentCurrencyId = currencyId;
    }

    public Long getShippingFromId() {
        return shippingFromId;
    }

    public void setShippingFromId(Long cityId) {
        this.shippingFromId = cityId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TripDTO tripDTO = (TripDTO) o;
        if(tripDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), tripDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TripDTO{" +
            "id=" + getId() +
            ", weight='" + getWeight() + "'" +
            ", notice='" + getNotice() + "'" +
            ", preferencePayment='" + getPreferencePayment() + "'" +
            ", rangeFrom='" + getRangeFrom() + "'" +
            ", rangeTo='" + getRangeTo() + "'" +
            ", fixedAmount='" + getFixedAmount() + "'" +
            ", percentageAmount='" + getPercentageAmount() + "'" +
            ", baggageInformation='" + getBaggageInformation() + "'" +
            ", shipDate='" + getShipDate() + "'" +
            ", remarks='" + getRemarks() + "'" +
            ", isDraft='" + getIsDraft() + "'" +
            "}";
    }
}
