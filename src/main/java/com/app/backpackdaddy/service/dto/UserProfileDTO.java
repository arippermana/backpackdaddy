package com.app.backpackdaddy.service.dto;


import com.app.backpackdaddy.web.rest.vm.ManagedUserProfileVM;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the UserProfile entity.
 */
public class UserProfileDTO implements Serializable {

    public UserProfileDTO(){};

    public UserProfileDTO(ManagedUserProfileVM managedUserProfileVM) {
        this.id = managedUserProfileVM.getId();
        this.name = managedUserProfileVM.getName();
        this.birthdate = managedUserProfileVM.getBirthdate();
        this.isAgeShow = managedUserProfileVM.isIsAgeShow();
        this.isWorkingOversea = managedUserProfileVM.isIsWorkingOversea();
        this.profileImage = managedUserProfileVM.getProfileImage();
        this.genderId = managedUserProfileVM.getGenderId();
        this.nationalityId = managedUserProfileVM.getNationalityId();
        this.currentLocationId = managedUserProfileVM.getCurrentLocationId();
        this.occupationId = managedUserProfileVM.getOccupationId();
        this.languageId = managedUserProfileVM.getLanguageId();
        this.currencyId = managedUserProfileVM.getCurrencyId();
        this.userId = managedUserProfileVM.getUserId();
    }

    private Long id;

    @NotNull
    private String name;

    @NotNull
    private LocalDate birthdate;

    @NotNull
    private Boolean isAgeShow;

    private Boolean isWorkingOversea;

    private String profileImage;

    private Long genderId;

    private String genderName;

    private Long nationalityId;

    private String nationalityName;

    private Long currentLocationId;

    private String currentLocationName;

    private Long occupationId;

    private String occupationName;

    private Long languageId;

    private String languageName;

    private Long currencyId;

    private String currencyName;

    private Long userId;

    private String userLogin;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(LocalDate birthdate) {
        this.birthdate = birthdate;
    }

    public Boolean isIsAgeShow() {
        return isAgeShow;
    }

    public void setIsAgeShow(Boolean isAgeShow) {
        this.isAgeShow = isAgeShow;
    }

    public Boolean isIsWorkingOversea() {
        return isWorkingOversea;
    }

    public void setIsWorkingOversea(Boolean isWorkingOversea) {
        this.isWorkingOversea = isWorkingOversea;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public Long getGenderId() {
        return genderId;
    }

    public void setGenderId(Long genderId) {
        this.genderId = genderId;
    }

    public String getGenderName() {
        return genderName;
    }

    public void setGenderName(String genderName) {
        this.genderName = genderName;
    }

    public Long getNationalityId() {
        return nationalityId;
    }

    public void setNationalityId(Long countryId) {
        this.nationalityId = countryId;
    }

    public String getNationalityName() {
        return nationalityName;
    }

    public void setNationalityName(String countryName) {
        this.nationalityName = countryName;
    }

    public Long getCurrentLocationId() {
        return currentLocationId;
    }

    public void setCurrentLocationId(Long countryId) {
        this.currentLocationId = countryId;
    }

    public String getCurrentLocationName() {
        return currentLocationName;
    }

    public void setCurrentLocationName(String countryName) {
        this.currentLocationName = countryName;
    }

    public Long getOccupationId() {
        return occupationId;
    }

    public void setOccupationId(Long occupationId) {
        this.occupationId = occupationId;
    }

    public String getOccupationName() {
        return occupationName;
    }

    public void setOccupationName(String occupationName) {
        this.occupationName = occupationName;
    }

    public Long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Long languageId) {
        this.languageId = languageId;
    }

    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }

    public Long getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Long currencyId) {
        this.currencyId = currencyId;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserProfileDTO userProfileDTO = (UserProfileDTO) o;
        if(userProfileDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), userProfileDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UserProfileDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", birthdate='" + getBirthdate() + "'" +
            ", isAgeShow='" + isIsAgeShow() + "'" +
            ", isWorkingOversea='" + isIsWorkingOversea() + "'" +
            ", profileImage='" + getProfileImage() + "'" +
            "}";
    }
}
