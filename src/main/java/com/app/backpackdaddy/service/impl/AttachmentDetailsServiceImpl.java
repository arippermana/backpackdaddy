package com.app.backpackdaddy.service.impl;

import com.app.backpackdaddy.service.AttachmentDetailsService;
import com.app.backpackdaddy.domain.AttachmentDetails;
import com.app.backpackdaddy.repository.AttachmentDetailsRepository;
import com.app.backpackdaddy.repository.search.AttachmentDetailsSearchRepository;
import com.app.backpackdaddy.service.dto.AttachmentDetailsDTO;
import com.app.backpackdaddy.service.mapper.AttachmentDetailsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing AttachmentDetails.
 */
@Service
@Transactional
public class AttachmentDetailsServiceImpl implements AttachmentDetailsService{

    private final Logger log = LoggerFactory.getLogger(AttachmentDetailsServiceImpl.class);

    private final AttachmentDetailsRepository attachmentDetailsRepository;

    private final AttachmentDetailsMapper attachmentDetailsMapper;

    private final AttachmentDetailsSearchRepository attachmentDetailsSearchRepository;

    public AttachmentDetailsServiceImpl(AttachmentDetailsRepository attachmentDetailsRepository, AttachmentDetailsMapper attachmentDetailsMapper, AttachmentDetailsSearchRepository attachmentDetailsSearchRepository) {
        this.attachmentDetailsRepository = attachmentDetailsRepository;
        this.attachmentDetailsMapper = attachmentDetailsMapper;
        this.attachmentDetailsSearchRepository = attachmentDetailsSearchRepository;
    }

    /**
     * Save a attachmentDetails.
     *
     * @param attachmentDetailsDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public AttachmentDetailsDTO save(AttachmentDetailsDTO attachmentDetailsDTO) {
        log.debug("Request to save AttachmentDetails : {}", attachmentDetailsDTO);
        AttachmentDetails attachmentDetails = attachmentDetailsMapper.toEntity(attachmentDetailsDTO);
        attachmentDetails = attachmentDetailsRepository.save(attachmentDetails);
        AttachmentDetailsDTO result = attachmentDetailsMapper.toDto(attachmentDetails);
        attachmentDetailsSearchRepository.save(attachmentDetails);
        return result;
    }

    /**
     *  Get all the attachmentDetails.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<AttachmentDetailsDTO> findAll() {
        log.debug("Request to get all AttachmentDetails");
        return attachmentDetailsRepository.findAll().stream()
            .map(attachmentDetailsMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one attachmentDetails by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public AttachmentDetailsDTO findOne(Long id) {
        log.debug("Request to get AttachmentDetails : {}", id);
        AttachmentDetails attachmentDetails = attachmentDetailsRepository.findOne(id);
        return attachmentDetailsMapper.toDto(attachmentDetails);
    }

    /**
     *  Delete the  attachmentDetails by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete AttachmentDetails : {}", id);
        attachmentDetailsRepository.delete(id);
        attachmentDetailsSearchRepository.delete(id);
    }

    /**
     * Search for the attachmentDetails corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<AttachmentDetailsDTO> search(String query) {
        log.debug("Request to search AttachmentDetails for query {}", query);
        return StreamSupport
            .stream(attachmentDetailsSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(attachmentDetailsMapper::toDto)
            .collect(Collectors.toList());
    }
}
