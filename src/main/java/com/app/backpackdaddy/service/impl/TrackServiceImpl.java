package com.app.backpackdaddy.service.impl;

import com.app.backpackdaddy.service.TrackService;
import com.app.backpackdaddy.domain.Track;
import com.app.backpackdaddy.repository.TrackRepository;
import com.app.backpackdaddy.repository.search.TrackSearchRepository;
import com.app.backpackdaddy.service.dto.TrackDTO;
import com.app.backpackdaddy.service.mapper.TrackMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Track.
 */
@Service
@Transactional
public class TrackServiceImpl implements TrackService{

    private final Logger log = LoggerFactory.getLogger(TrackServiceImpl.class);

    private final TrackRepository trackRepository;

    private final TrackMapper trackMapper;

    private final TrackSearchRepository trackSearchRepository;

    public TrackServiceImpl(TrackRepository trackRepository, TrackMapper trackMapper, TrackSearchRepository trackSearchRepository) {
        this.trackRepository = trackRepository;
        this.trackMapper = trackMapper;
        this.trackSearchRepository = trackSearchRepository;
    }

    /**
     * Save a track.
     *
     * @param trackDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public TrackDTO save(TrackDTO trackDTO) {
        log.debug("Request to save Track : {}", trackDTO);
        Track track = trackMapper.toEntity(trackDTO);
        track = trackRepository.save(track);
        TrackDTO result = trackMapper.toDto(track);
        trackSearchRepository.save(track);
        return result;
    }

    /**
     *  Get all the tracks.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<TrackDTO> findAll() {
        log.debug("Request to get all Tracks");
        return trackRepository.findAll().stream()
            .map(trackMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one track by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public TrackDTO findOne(Long id) {
        log.debug("Request to get Track : {}", id);
        Track track = trackRepository.findOne(id);
        return trackMapper.toDto(track);
    }

    /**
     *  Delete the  track by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Track : {}", id);
        trackRepository.delete(id);
        trackSearchRepository.delete(id);
    }

    /**
     * Search for the track corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<TrackDTO> search(String query) {
        log.debug("Request to search Tracks for query {}", query);
        return StreamSupport
            .stream(trackSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(trackMapper::toDto)
            .collect(Collectors.toList());
    }
}
