package com.app.backpackdaddy.service.impl;

import com.app.backpackdaddy.service.TransactionStatusService;
import com.app.backpackdaddy.domain.TransactionStatus;
import com.app.backpackdaddy.repository.TransactionStatusRepository;
import com.app.backpackdaddy.repository.search.TransactionStatusSearchRepository;
import com.app.backpackdaddy.service.dto.TransactionStatusDTO;
import com.app.backpackdaddy.service.mapper.TransactionStatusMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing TransactionStatus.
 */
@Service
@Transactional
public class TransactionStatusServiceImpl implements TransactionStatusService{

    private final Logger log = LoggerFactory.getLogger(TransactionStatusServiceImpl.class);

    private final TransactionStatusRepository transactionStatusRepository;

    private final TransactionStatusMapper transactionStatusMapper;

    private final TransactionStatusSearchRepository transactionStatusSearchRepository;

    public TransactionStatusServiceImpl(TransactionStatusRepository transactionStatusRepository, TransactionStatusMapper transactionStatusMapper, TransactionStatusSearchRepository transactionStatusSearchRepository) {
        this.transactionStatusRepository = transactionStatusRepository;
        this.transactionStatusMapper = transactionStatusMapper;
        this.transactionStatusSearchRepository = transactionStatusSearchRepository;
    }

    /**
     * Save a transactionStatus.
     *
     * @param transactionStatusDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public TransactionStatusDTO save(TransactionStatusDTO transactionStatusDTO) {
        log.debug("Request to save TransactionStatus : {}", transactionStatusDTO);
        TransactionStatus transactionStatus = transactionStatusMapper.toEntity(transactionStatusDTO);
        transactionStatus = transactionStatusRepository.save(transactionStatus);
        TransactionStatusDTO result = transactionStatusMapper.toDto(transactionStatus);
        transactionStatusSearchRepository.save(transactionStatus);
        return result;
    }

    /**
     *  Get all the transactionStatuses.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<TransactionStatusDTO> findAll() {
        log.debug("Request to get all TransactionStatuses");
        return transactionStatusRepository.findAll().stream()
            .map(transactionStatusMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one transactionStatus by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public TransactionStatusDTO findOne(Long id) {
        log.debug("Request to get TransactionStatus : {}", id);
        TransactionStatus transactionStatus = transactionStatusRepository.findOne(id);
        return transactionStatusMapper.toDto(transactionStatus);
    }

    /**
     *  Delete the  transactionStatus by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete TransactionStatus : {}", id);
        transactionStatusRepository.delete(id);
        transactionStatusSearchRepository.delete(id);
    }

    /**
     * Search for the transactionStatus corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<TransactionStatusDTO> search(String query) {
        log.debug("Request to search TransactionStatuses for query {}", query);
        return StreamSupport
            .stream(transactionStatusSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(transactionStatusMapper::toDto)
            .collect(Collectors.toList());
    }
}
