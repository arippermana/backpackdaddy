package com.app.backpackdaddy.service.impl;

import com.app.backpackdaddy.domain.User;
import com.app.backpackdaddy.repository.UserRepository;
import com.app.backpackdaddy.service.UserProfileService;
import com.app.backpackdaddy.domain.UserProfile;
import com.app.backpackdaddy.repository.UserProfileRepository;
import com.app.backpackdaddy.repository.search.UserProfileSearchRepository;
import com.app.backpackdaddy.service.dto.UserProfileDTO;
import com.app.backpackdaddy.service.mapper.UserProfileMapper;
import com.app.backpackdaddy.web.rest.vm.ManagedUserProfileVM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing UserProfile.
 */
@Service
@Transactional
public class UserProfileServiceImpl implements UserProfileService{

    private final Logger log = LoggerFactory.getLogger(UserProfileServiceImpl.class);

    private final UserProfileRepository userProfileRepository;

    private final UserProfileMapper userProfileMapper;

    private final UserRepository userRepository;

    private final UserProfileSearchRepository userProfileSearchRepository;

    public UserProfileServiceImpl(UserProfileRepository userProfileRepository, UserProfileMapper userProfileMapper, UserProfileSearchRepository userProfileSearchRepository,
                                  UserRepository userRepository) {
        this.userProfileRepository = userProfileRepository;
        this.userProfileMapper = userProfileMapper;
        this.userProfileSearchRepository = userProfileSearchRepository;
        this.userRepository = userRepository;
    }

    /**
     * Save a userProfile.
     *
     * @param userProfileDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public UserProfileDTO save(UserProfileDTO userProfileDTO) {
        log.debug("Request to save UserProfile : {}", userProfileDTO);
        UserProfile userProfile = userProfileMapper.toEntity(userProfileDTO);
        userProfile = userProfileRepository.save(userProfile);
        UserProfileDTO result = userProfileMapper.toDto(userProfile);
        userProfileSearchRepository.save(userProfile);
        return result;
    }

    @Override
    public Optional<User> activate(ManagedUserProfileVM managedUserProfileVM) {
        log.debug("Activate User Profile");
        return userRepository.findOneByActivationKey(managedUserProfileVM.getKey())
            .map(user -> {
                // activate given user for the registration key.
                managedUserProfileVM.setUserId(user.getId());
                UserProfileDTO userProfileDTO = new UserProfileDTO(managedUserProfileVM);
                UserProfile userProfile = userProfileMapper.toEntity(userProfileDTO);
                userProfile = userProfileRepository.save(userProfile);
                UserProfileDTO result = userProfileMapper.toDto(userProfile);
                userProfileSearchRepository.save(userProfile);
                return user;
            });
    }

    /**
     *  Get all the userProfiles.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<UserProfileDTO> findAll() {
        log.debug("Request to get all UserProfiles");
        return userProfileRepository.findAll().stream()
            .map(userProfileMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one userProfile by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public UserProfileDTO findOne(Long id) {
        log.debug("Request to get UserProfile : {}", id);
        UserProfile userProfile = userProfileRepository.findOne(id);
        return userProfileMapper.toDto(userProfile);
    }

    /**
     *  Delete the  userProfile by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete UserProfile : {}", id);
        userProfileRepository.delete(id);
        userProfileSearchRepository.delete(id);
    }

    /**
     * Search for the userProfile corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<UserProfileDTO> search(String query) {
        log.debug("Request to search UserProfiles for query {}", query);
        return StreamSupport
            .stream(userProfileSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(userProfileMapper::toDto)
            .collect(Collectors.toList());
    }
}
