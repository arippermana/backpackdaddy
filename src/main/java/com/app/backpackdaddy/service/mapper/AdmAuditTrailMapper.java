package com.app.backpackdaddy.service.mapper;

import com.app.backpackdaddy.domain.*;
import com.app.backpackdaddy.service.dto.AdmAuditTrailDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity AdmAuditTrail and its DTO AdmAuditTrailDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AdmAuditTrailMapper extends EntityMapper<AdmAuditTrailDTO, AdmAuditTrail> {

    

    

    default AdmAuditTrail fromId(Long id) {
        if (id == null) {
            return null;
        }
        AdmAuditTrail admAuditTrail = new AdmAuditTrail();
        admAuditTrail.setId(id);
        return admAuditTrail;
    }
}
