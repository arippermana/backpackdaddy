package com.app.backpackdaddy.service.mapper;

import com.app.backpackdaddy.domain.*;
import com.app.backpackdaddy.service.dto.AdmRefBranchDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity AdmRefBranch and its DTO AdmRefBranchDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AdmRefBranchMapper extends EntityMapper<AdmRefBranchDTO, AdmRefBranch> {

    

    

    default AdmRefBranch fromId(Long id) {
        if (id == null) {
            return null;
        }
        AdmRefBranch admRefBranch = new AdmRefBranch();
        admRefBranch.setId(id);
        return admRefBranch;
    }
}
