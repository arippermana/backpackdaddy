package com.app.backpackdaddy.service.mapper;

import com.app.backpackdaddy.domain.*;
import com.app.backpackdaddy.service.dto.AdmRefDepartmentDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity AdmRefDepartment and its DTO AdmRefDepartmentDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AdmRefDepartmentMapper extends EntityMapper<AdmRefDepartmentDTO, AdmRefDepartment> {

    

    

    default AdmRefDepartment fromId(Long id) {
        if (id == null) {
            return null;
        }
        AdmRefDepartment admRefDepartment = new AdmRefDepartment();
        admRefDepartment.setId(id);
        return admRefDepartment;
    }
}
