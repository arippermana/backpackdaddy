package com.app.backpackdaddy.service.mapper;

import com.app.backpackdaddy.domain.*;
import com.app.backpackdaddy.service.dto.AdmRefGroupDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity AdmRefGroup and its DTO AdmRefGroupDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AdmRefGroupMapper extends EntityMapper<AdmRefGroupDTO, AdmRefGroup> {

    

    

    default AdmRefGroup fromId(Long id) {
        if (id == null) {
            return null;
        }
        AdmRefGroup admRefGroup = new AdmRefGroup();
        admRefGroup.setId(id);
        return admRefGroup;
    }
}
