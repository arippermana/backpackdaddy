package com.app.backpackdaddy.service.mapper;

import com.app.backpackdaddy.domain.*;
import com.app.backpackdaddy.service.dto.AdmRefPositionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity AdmRefPosition and its DTO AdmRefPositionDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AdmRefPositionMapper extends EntityMapper<AdmRefPositionDTO, AdmRefPosition> {

    

    

    default AdmRefPosition fromId(Long id) {
        if (id == null) {
            return null;
        }
        AdmRefPosition admRefPosition = new AdmRefPosition();
        admRefPosition.setId(id);
        return admRefPosition;
    }
}
