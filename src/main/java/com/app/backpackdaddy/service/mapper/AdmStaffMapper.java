package com.app.backpackdaddy.service.mapper;

import com.app.backpackdaddy.domain.*;
import com.app.backpackdaddy.service.dto.AdmStaffDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity AdmStaff and its DTO AdmStaffDTO.
 */
@Mapper(componentModel = "spring", uses = {AdmRefBranchMapper.class, AdmRefDepartmentMapper.class, AdmRefGroupMapper.class, AdmRefPositionMapper.class})
public interface AdmStaffMapper extends EntityMapper<AdmStaffDTO, AdmStaff> {

    @Mapping(source = "branch.id", target = "branchId")
    @Mapping(source = "branch.branchName", target = "branchBranchName")
    @Mapping(source = "department.id", target = "departmentId")
    @Mapping(source = "department.departmentName", target = "departmentDepartmentName")
    @Mapping(source = "group.id", target = "groupId")
    @Mapping(source = "group.groupName", target = "groupGroupName")
    @Mapping(source = "position.id", target = "positionId")
    @Mapping(source = "position.positionName", target = "positionPositionName")
    AdmStaffDTO toDto(AdmStaff admStaff); 

    @Mapping(source = "branchId", target = "branch")
    @Mapping(source = "departmentId", target = "department")
    @Mapping(source = "groupId", target = "group")
    @Mapping(source = "positionId", target = "position")
    AdmStaff toEntity(AdmStaffDTO admStaffDTO);

    default AdmStaff fromId(Long id) {
        if (id == null) {
            return null;
        }
        AdmStaff admStaff = new AdmStaff();
        admStaff.setId(id);
        return admStaff;
    }
}
