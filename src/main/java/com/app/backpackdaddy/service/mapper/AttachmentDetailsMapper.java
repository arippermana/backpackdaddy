package com.app.backpackdaddy.service.mapper;

import com.app.backpackdaddy.domain.*;
import com.app.backpackdaddy.service.dto.AttachmentDetailsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity AttachmentDetails and its DTO AttachmentDetailsDTO.
 */
@Mapper(componentModel = "spring", uses = {TrackMapper.class})
public interface AttachmentDetailsMapper extends EntityMapper<AttachmentDetailsDTO, AttachmentDetails> {

    @Mapping(source = "track.id", target = "trackId")
    AttachmentDetailsDTO toDto(AttachmentDetails attachmentDetails); 

    @Mapping(source = "trackId", target = "track")
    AttachmentDetails toEntity(AttachmentDetailsDTO attachmentDetailsDTO);

    default AttachmentDetails fromId(Long id) {
        if (id == null) {
            return null;
        }
        AttachmentDetails attachmentDetails = new AttachmentDetails();
        attachmentDetails.setId(id);
        return attachmentDetails;
    }
}
