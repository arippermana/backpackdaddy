package com.app.backpackdaddy.service.mapper;

import com.app.backpackdaddy.domain.*;
import com.app.backpackdaddy.service.dto.CityDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity City and its DTO CityDTO.
 */
@Mapper(componentModel = "spring", uses = {RegionMapper.class})
public interface CityMapper extends EntityMapper<CityDTO, City> {

    @Mapping(source = "region.id", target = "regionId")
    CityDTO toDto(City city); 

    @Mapping(source = "regionId", target = "region")
    City toEntity(CityDTO cityDTO);

    default City fromId(Long id) {
        if (id == null) {
            return null;
        }
        City city = new City();
        city.setId(id);
        return city;
    }
}
