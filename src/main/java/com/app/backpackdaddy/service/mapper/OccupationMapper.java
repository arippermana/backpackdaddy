package com.app.backpackdaddy.service.mapper;

import com.app.backpackdaddy.domain.*;
import com.app.backpackdaddy.service.dto.OccupationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Occupation and its DTO OccupationDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface OccupationMapper extends EntityMapper<OccupationDTO, Occupation> {

    

    

    default Occupation fromId(Long id) {
        if (id == null) {
            return null;
        }
        Occupation occupation = new Occupation();
        occupation.setId(id);
        return occupation;
    }
}
