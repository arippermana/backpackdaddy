package com.app.backpackdaddy.service.mapper;

import com.app.backpackdaddy.domain.*;
import com.app.backpackdaddy.service.dto.RequestDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Request and its DTO RequestDTO.
 */
@Mapper(componentModel = "spring", uses = {ProductMapper.class, UserMapper.class, CityMapper.class, CurrencyMapper.class})
public interface RequestMapper extends EntityMapper<RequestDTO, Request> {

    @Mapping(source = "product.id", target = "productId")
    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "city.id", target = "cityId")
    @Mapping(source = "tipCurrency.id", target = "tipCurrencyId")
    RequestDTO toDto(Request request); 

    @Mapping(source = "productId", target = "product")
    @Mapping(source = "userId", target = "user")
    @Mapping(source = "cityId", target = "city")
    @Mapping(source = "tipCurrencyId", target = "tipCurrency")
    Request toEntity(RequestDTO requestDTO);

    default Request fromId(Long id) {
        if (id == null) {
            return null;
        }
        Request request = new Request();
        request.setId(id);
        return request;
    }
}
