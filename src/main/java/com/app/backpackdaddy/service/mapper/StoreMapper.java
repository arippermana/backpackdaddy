package com.app.backpackdaddy.service.mapper;

import com.app.backpackdaddy.domain.*;
import com.app.backpackdaddy.service.dto.StoreDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Store and its DTO StoreDTO.
 */
@Mapper(componentModel = "spring", uses = {CountryMapper.class, CurrencyMapper.class, ProductMapper.class})
public interface StoreMapper extends EntityMapper<StoreDTO, Store> {

    @Mapping(source = "country.id", target = "countryId")
    @Mapping(source = "currency.id", target = "currencyId")
    @Mapping(source = "product.id", target = "productId")
    StoreDTO toDto(Store store); 

    @Mapping(source = "countryId", target = "country")
    @Mapping(source = "currencyId", target = "currency")
    @Mapping(source = "productId", target = "product")
    Store toEntity(StoreDTO storeDTO);

    default Store fromId(Long id) {
        if (id == null) {
            return null;
        }
        Store store = new Store();
        store.setId(id);
        return store;
    }
}
