package com.app.backpackdaddy.service.mapper;

import com.app.backpackdaddy.domain.*;
import com.app.backpackdaddy.service.dto.TrackDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Track and its DTO TrackDTO.
 */
@Mapper(componentModel = "spring", uses = {TransactionStatusMapper.class})
public interface TrackMapper extends EntityMapper<TrackDTO, Track> {

    @Mapping(source = "status.id", target = "statusId")
    TrackDTO toDto(Track track); 

    @Mapping(target = "attachments", ignore = true)
    @Mapping(source = "statusId", target = "status")
    Track toEntity(TrackDTO trackDTO);

    default Track fromId(Long id) {
        if (id == null) {
            return null;
        }
        Track track = new Track();
        track.setId(id);
        return track;
    }
}
