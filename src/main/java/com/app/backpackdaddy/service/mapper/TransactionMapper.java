package com.app.backpackdaddy.service.mapper;

import com.app.backpackdaddy.domain.*;
import com.app.backpackdaddy.service.dto.TransactionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Transaction and its DTO TransactionDTO.
 */
@Mapper(componentModel = "spring", uses = {TrackMapper.class, RequestMapper.class, TripMapper.class})
public interface TransactionMapper extends EntityMapper<TransactionDTO, Transaction> {

    @Mapping(source = "track.id", target = "trackId")
    @Mapping(source = "request.id", target = "requestId")
    @Mapping(source = "trip.id", target = "tripId")
    TransactionDTO toDto(Transaction transaction); 

    @Mapping(source = "trackId", target = "track")
    @Mapping(source = "requestId", target = "request")
    @Mapping(source = "tripId", target = "trip")
    Transaction toEntity(TransactionDTO transactionDTO);

    default Transaction fromId(Long id) {
        if (id == null) {
            return null;
        }
        Transaction transaction = new Transaction();
        transaction.setId(id);
        return transaction;
    }
}
