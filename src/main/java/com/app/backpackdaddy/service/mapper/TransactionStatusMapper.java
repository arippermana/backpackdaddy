package com.app.backpackdaddy.service.mapper;

import com.app.backpackdaddy.domain.*;
import com.app.backpackdaddy.service.dto.TransactionStatusDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity TransactionStatus and its DTO TransactionStatusDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TransactionStatusMapper extends EntityMapper<TransactionStatusDTO, TransactionStatus> {

    

    

    default TransactionStatus fromId(Long id) {
        if (id == null) {
            return null;
        }
        TransactionStatus transactionStatus = new TransactionStatus();
        transactionStatus.setId(id);
        return transactionStatus;
    }
}
