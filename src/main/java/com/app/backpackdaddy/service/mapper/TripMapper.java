package com.app.backpackdaddy.service.mapper;

import com.app.backpackdaddy.domain.*;
import com.app.backpackdaddy.service.dto.TripDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Trip and its DTO TripDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, CountryMapper.class, CategoryMapper.class, CurrencyMapper.class, CityMapper.class})
public interface TripMapper extends EntityMapper<TripDTO, Trip> {

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "fromCountry.id", target = "fromCountryId")
    @Mapping(source = "toCountry.id", target = "toCountryId")
    @Mapping(source = "category.id", target = "categoryId")
    @Mapping(source = "paymentCurrency.id", target = "paymentCurrencyId")
    @Mapping(source = "shippingFrom.id", target = "shippingFromId")
    TripDTO toDto(Trip trip); 

    @Mapping(source = "userId", target = "user")
    @Mapping(source = "fromCountryId", target = "fromCountry")
    @Mapping(source = "toCountryId", target = "toCountry")
    @Mapping(source = "categoryId", target = "category")
    @Mapping(source = "paymentCurrencyId", target = "paymentCurrency")
    @Mapping(source = "shippingFromId", target = "shippingFrom")
    Trip toEntity(TripDTO tripDTO);

    default Trip fromId(Long id) {
        if (id == null) {
            return null;
        }
        Trip trip = new Trip();
        trip.setId(id);
        return trip;
    }
}
