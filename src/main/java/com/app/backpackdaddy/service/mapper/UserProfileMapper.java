package com.app.backpackdaddy.service.mapper;

import com.app.backpackdaddy.domain.*;
import com.app.backpackdaddy.service.dto.UserProfileDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity UserProfile and its DTO UserProfileDTO.
 */
@Mapper(componentModel = "spring", uses = {GenderMapper.class, CountryMapper.class, OccupationMapper.class, LanguageMapper.class, CurrencyMapper.class, UserMapper.class})
public interface UserProfileMapper extends EntityMapper<UserProfileDTO, UserProfile> {

    @Mapping(source = "gender.id", target = "genderId")
    @Mapping(source = "gender.name", target = "genderName")
    @Mapping(source = "nationality.id", target = "nationalityId")
    @Mapping(source = "nationality.name", target = "nationalityName")
    @Mapping(source = "currentLocation.id", target = "currentLocationId")
    @Mapping(source = "currentLocation.name", target = "currentLocationName")
    @Mapping(source = "occupation.id", target = "occupationId")
    @Mapping(source = "occupation.name", target = "occupationName")
    @Mapping(source = "language.id", target = "languageId")
    @Mapping(source = "language.name", target = "languageName")
    @Mapping(source = "currency.id", target = "currencyId")
    @Mapping(source = "currency.name", target = "currencyName")
    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "user.login", target = "userLogin")
    UserProfileDTO toDto(UserProfile userProfile); 

    @Mapping(source = "genderId", target = "gender")
    @Mapping(source = "nationalityId", target = "nationality")
    @Mapping(source = "currentLocationId", target = "currentLocation")
    @Mapping(source = "occupationId", target = "occupation")
    @Mapping(source = "languageId", target = "language")
    @Mapping(source = "currencyId", target = "currency")
    @Mapping(source = "userId", target = "user")
    UserProfile toEntity(UserProfileDTO userProfileDTO);

    default UserProfile fromId(Long id) {
        if (id == null) {
            return null;
        }
        UserProfile userProfile = new UserProfile();
        userProfile.setId(id);
        return userProfile;
    }
}
