package com.app.backpackdaddy.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.app.backpackdaddy.domain.AdmAuditTrail;

import com.app.backpackdaddy.repository.AdmAuditTrailRepository;
import com.app.backpackdaddy.repository.search.AdmAuditTrailSearchRepository;
import com.app.backpackdaddy.web.rest.errors.BadRequestAlertException;
import com.app.backpackdaddy.web.rest.util.HeaderUtil;
import com.app.backpackdaddy.service.dto.AdmAuditTrailDTO;
import com.app.backpackdaddy.service.mapper.AdmAuditTrailMapper;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing AdmAuditTrail.
 */
@RestController
@RequestMapping("/api")
public class AdmAuditTrailResource {

    private final Logger log = LoggerFactory.getLogger(AdmAuditTrailResource.class);

    private static final String ENTITY_NAME = "admAuditTrail";

    private final AdmAuditTrailRepository admAuditTrailRepository;

    private final AdmAuditTrailMapper admAuditTrailMapper;

    private final AdmAuditTrailSearchRepository admAuditTrailSearchRepository;

    public AdmAuditTrailResource(AdmAuditTrailRepository admAuditTrailRepository, AdmAuditTrailMapper admAuditTrailMapper, AdmAuditTrailSearchRepository admAuditTrailSearchRepository) {
        this.admAuditTrailRepository = admAuditTrailRepository;
        this.admAuditTrailMapper = admAuditTrailMapper;
        this.admAuditTrailSearchRepository = admAuditTrailSearchRepository;
    }

    /**
     * POST  /adm-audit-trails : Create a new admAuditTrail.
     *
     * @param admAuditTrailDTO the admAuditTrailDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new admAuditTrailDTO, or with status 400 (Bad Request) if the admAuditTrail has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/adm-audit-trails")
    @Timed
    public ResponseEntity<AdmAuditTrailDTO> createAdmAuditTrail(@Valid @RequestBody AdmAuditTrailDTO admAuditTrailDTO) throws URISyntaxException {
        log.debug("REST request to save AdmAuditTrail : {}", admAuditTrailDTO);
        if (admAuditTrailDTO.getId() != null) {
            throw new BadRequestAlertException("A new admAuditTrail cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AdmAuditTrail admAuditTrail = admAuditTrailMapper.toEntity(admAuditTrailDTO);
        admAuditTrail = admAuditTrailRepository.save(admAuditTrail);
        AdmAuditTrailDTO result = admAuditTrailMapper.toDto(admAuditTrail);
        admAuditTrailSearchRepository.save(admAuditTrail);
        return ResponseEntity.created(new URI("/api/adm-audit-trails/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /adm-audit-trails : Updates an existing admAuditTrail.
     *
     * @param admAuditTrailDTO the admAuditTrailDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated admAuditTrailDTO,
     * or with status 400 (Bad Request) if the admAuditTrailDTO is not valid,
     * or with status 500 (Internal Server Error) if the admAuditTrailDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/adm-audit-trails")
    @Timed
    public ResponseEntity<AdmAuditTrailDTO> updateAdmAuditTrail(@Valid @RequestBody AdmAuditTrailDTO admAuditTrailDTO) throws URISyntaxException {
        log.debug("REST request to update AdmAuditTrail : {}", admAuditTrailDTO);
        if (admAuditTrailDTO.getId() == null) {
            return createAdmAuditTrail(admAuditTrailDTO);
        }
        AdmAuditTrail admAuditTrail = admAuditTrailMapper.toEntity(admAuditTrailDTO);
        admAuditTrail = admAuditTrailRepository.save(admAuditTrail);
        AdmAuditTrailDTO result = admAuditTrailMapper.toDto(admAuditTrail);
        admAuditTrailSearchRepository.save(admAuditTrail);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, admAuditTrailDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /adm-audit-trails : get all the admAuditTrails.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of admAuditTrails in body
     */
    @GetMapping("/adm-audit-trails")
    @Timed
    public List<AdmAuditTrailDTO> getAllAdmAuditTrails() {
        log.debug("REST request to get all AdmAuditTrails");
        List<AdmAuditTrail> admAuditTrails = admAuditTrailRepository.findAll();
        return admAuditTrailMapper.toDto(admAuditTrails);
        }

    /**
     * GET  /adm-audit-trails/:id : get the "id" admAuditTrail.
     *
     * @param id the id of the admAuditTrailDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the admAuditTrailDTO, or with status 404 (Not Found)
     */
    @GetMapping("/adm-audit-trails/{id}")
    @Timed
    public ResponseEntity<AdmAuditTrailDTO> getAdmAuditTrail(@PathVariable Long id) {
        log.debug("REST request to get AdmAuditTrail : {}", id);
        AdmAuditTrail admAuditTrail = admAuditTrailRepository.findOne(id);
        AdmAuditTrailDTO admAuditTrailDTO = admAuditTrailMapper.toDto(admAuditTrail);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(admAuditTrailDTO));
    }

    /**
     * DELETE  /adm-audit-trails/:id : delete the "id" admAuditTrail.
     *
     * @param id the id of the admAuditTrailDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/adm-audit-trails/{id}")
    @Timed
    public ResponseEntity<Void> deleteAdmAuditTrail(@PathVariable Long id) {
        log.debug("REST request to delete AdmAuditTrail : {}", id);
        admAuditTrailRepository.delete(id);
        admAuditTrailSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/adm-audit-trails?query=:query : search for the admAuditTrail corresponding
     * to the query.
     *
     * @param query the query of the admAuditTrail search
     * @return the result of the search
     */
    @GetMapping("/_search/adm-audit-trails")
    @Timed
    public List<AdmAuditTrailDTO> searchAdmAuditTrails(@RequestParam String query) {
        log.debug("REST request to search AdmAuditTrails for query {}", query);
        return StreamSupport
            .stream(admAuditTrailSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(admAuditTrailMapper::toDto)
            .collect(Collectors.toList());
    }

}
