package com.app.backpackdaddy.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.app.backpackdaddy.domain.AdmRefBranch;

import com.app.backpackdaddy.repository.AdmRefBranchRepository;
import com.app.backpackdaddy.repository.search.AdmRefBranchSearchRepository;
import com.app.backpackdaddy.web.rest.errors.BadRequestAlertException;
import com.app.backpackdaddy.web.rest.util.HeaderUtil;
import com.app.backpackdaddy.service.dto.AdmRefBranchDTO;
import com.app.backpackdaddy.service.mapper.AdmRefBranchMapper;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing AdmRefBranch.
 */
@RestController
@RequestMapping("/api")
public class AdmRefBranchResource {

    private final Logger log = LoggerFactory.getLogger(AdmRefBranchResource.class);

    private static final String ENTITY_NAME = "admRefBranch";

    private final AdmRefBranchRepository admRefBranchRepository;

    private final AdmRefBranchMapper admRefBranchMapper;

    private final AdmRefBranchSearchRepository admRefBranchSearchRepository;

    public AdmRefBranchResource(AdmRefBranchRepository admRefBranchRepository, AdmRefBranchMapper admRefBranchMapper, AdmRefBranchSearchRepository admRefBranchSearchRepository) {
        this.admRefBranchRepository = admRefBranchRepository;
        this.admRefBranchMapper = admRefBranchMapper;
        this.admRefBranchSearchRepository = admRefBranchSearchRepository;
    }

    /**
     * POST  /adm-ref-branches : Create a new admRefBranch.
     *
     * @param admRefBranchDTO the admRefBranchDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new admRefBranchDTO, or with status 400 (Bad Request) if the admRefBranch has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/adm-ref-branches")
    @Timed
    public ResponseEntity<AdmRefBranchDTO> createAdmRefBranch(@Valid @RequestBody AdmRefBranchDTO admRefBranchDTO) throws URISyntaxException {
        log.debug("REST request to save AdmRefBranch : {}", admRefBranchDTO);
        if (admRefBranchDTO.getId() != null) {
            throw new BadRequestAlertException("A new admRefBranch cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AdmRefBranch admRefBranch = admRefBranchMapper.toEntity(admRefBranchDTO);
        admRefBranch = admRefBranchRepository.save(admRefBranch);
        AdmRefBranchDTO result = admRefBranchMapper.toDto(admRefBranch);
        admRefBranchSearchRepository.save(admRefBranch);
        return ResponseEntity.created(new URI("/api/adm-ref-branches/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /adm-ref-branches : Updates an existing admRefBranch.
     *
     * @param admRefBranchDTO the admRefBranchDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated admRefBranchDTO,
     * or with status 400 (Bad Request) if the admRefBranchDTO is not valid,
     * or with status 500 (Internal Server Error) if the admRefBranchDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/adm-ref-branches")
    @Timed
    public ResponseEntity<AdmRefBranchDTO> updateAdmRefBranch(@Valid @RequestBody AdmRefBranchDTO admRefBranchDTO) throws URISyntaxException {
        log.debug("REST request to update AdmRefBranch : {}", admRefBranchDTO);
        if (admRefBranchDTO.getId() == null) {
            return createAdmRefBranch(admRefBranchDTO);
        }
        AdmRefBranch admRefBranch = admRefBranchMapper.toEntity(admRefBranchDTO);
        admRefBranch = admRefBranchRepository.save(admRefBranch);
        AdmRefBranchDTO result = admRefBranchMapper.toDto(admRefBranch);
        admRefBranchSearchRepository.save(admRefBranch);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, admRefBranchDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /adm-ref-branches : get all the admRefBranches.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of admRefBranches in body
     */
    @GetMapping("/adm-ref-branches")
    @Timed
    public List<AdmRefBranchDTO> getAllAdmRefBranches() {
        log.debug("REST request to get all AdmRefBranches");
        List<AdmRefBranch> admRefBranches = admRefBranchRepository.findAll();
        return admRefBranchMapper.toDto(admRefBranches);
        }

    /**
     * GET  /adm-ref-branches/:id : get the "id" admRefBranch.
     *
     * @param id the id of the admRefBranchDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the admRefBranchDTO, or with status 404 (Not Found)
     */
    @GetMapping("/adm-ref-branches/{id}")
    @Timed
    public ResponseEntity<AdmRefBranchDTO> getAdmRefBranch(@PathVariable Long id) {
        log.debug("REST request to get AdmRefBranch : {}", id);
        AdmRefBranch admRefBranch = admRefBranchRepository.findOne(id);
        AdmRefBranchDTO admRefBranchDTO = admRefBranchMapper.toDto(admRefBranch);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(admRefBranchDTO));
    }

    /**
     * DELETE  /adm-ref-branches/:id : delete the "id" admRefBranch.
     *
     * @param id the id of the admRefBranchDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/adm-ref-branches/{id}")
    @Timed
    public ResponseEntity<Void> deleteAdmRefBranch(@PathVariable Long id) {
        log.debug("REST request to delete AdmRefBranch : {}", id);
        admRefBranchRepository.delete(id);
        admRefBranchSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/adm-ref-branches?query=:query : search for the admRefBranch corresponding
     * to the query.
     *
     * @param query the query of the admRefBranch search
     * @return the result of the search
     */
    @GetMapping("/_search/adm-ref-branches")
    @Timed
    public List<AdmRefBranchDTO> searchAdmRefBranches(@RequestParam String query) {
        log.debug("REST request to search AdmRefBranches for query {}", query);
        return StreamSupport
            .stream(admRefBranchSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(admRefBranchMapper::toDto)
            .collect(Collectors.toList());
    }

}
