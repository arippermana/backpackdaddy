package com.app.backpackdaddy.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.app.backpackdaddy.domain.AdmRefDepartment;

import com.app.backpackdaddy.repository.AdmRefDepartmentRepository;
import com.app.backpackdaddy.repository.search.AdmRefDepartmentSearchRepository;
import com.app.backpackdaddy.web.rest.errors.BadRequestAlertException;
import com.app.backpackdaddy.web.rest.util.HeaderUtil;
import com.app.backpackdaddy.service.dto.AdmRefDepartmentDTO;
import com.app.backpackdaddy.service.mapper.AdmRefDepartmentMapper;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing AdmRefDepartment.
 */
@RestController
@RequestMapping("/api")
public class AdmRefDepartmentResource {

    private final Logger log = LoggerFactory.getLogger(AdmRefDepartmentResource.class);

    private static final String ENTITY_NAME = "admRefDepartment";

    private final AdmRefDepartmentRepository admRefDepartmentRepository;

    private final AdmRefDepartmentMapper admRefDepartmentMapper;

    private final AdmRefDepartmentSearchRepository admRefDepartmentSearchRepository;

    public AdmRefDepartmentResource(AdmRefDepartmentRepository admRefDepartmentRepository, AdmRefDepartmentMapper admRefDepartmentMapper, AdmRefDepartmentSearchRepository admRefDepartmentSearchRepository) {
        this.admRefDepartmentRepository = admRefDepartmentRepository;
        this.admRefDepartmentMapper = admRefDepartmentMapper;
        this.admRefDepartmentSearchRepository = admRefDepartmentSearchRepository;
    }

    /**
     * POST  /adm-ref-departments : Create a new admRefDepartment.
     *
     * @param admRefDepartmentDTO the admRefDepartmentDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new admRefDepartmentDTO, or with status 400 (Bad Request) if the admRefDepartment has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/adm-ref-departments")
    @Timed
    public ResponseEntity<AdmRefDepartmentDTO> createAdmRefDepartment(@Valid @RequestBody AdmRefDepartmentDTO admRefDepartmentDTO) throws URISyntaxException {
        log.debug("REST request to save AdmRefDepartment : {}", admRefDepartmentDTO);
        if (admRefDepartmentDTO.getId() != null) {
            throw new BadRequestAlertException("A new admRefDepartment cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AdmRefDepartment admRefDepartment = admRefDepartmentMapper.toEntity(admRefDepartmentDTO);
        admRefDepartment = admRefDepartmentRepository.save(admRefDepartment);
        AdmRefDepartmentDTO result = admRefDepartmentMapper.toDto(admRefDepartment);
        admRefDepartmentSearchRepository.save(admRefDepartment);
        return ResponseEntity.created(new URI("/api/adm-ref-departments/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /adm-ref-departments : Updates an existing admRefDepartment.
     *
     * @param admRefDepartmentDTO the admRefDepartmentDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated admRefDepartmentDTO,
     * or with status 400 (Bad Request) if the admRefDepartmentDTO is not valid,
     * or with status 500 (Internal Server Error) if the admRefDepartmentDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/adm-ref-departments")
    @Timed
    public ResponseEntity<AdmRefDepartmentDTO> updateAdmRefDepartment(@Valid @RequestBody AdmRefDepartmentDTO admRefDepartmentDTO) throws URISyntaxException {
        log.debug("REST request to update AdmRefDepartment : {}", admRefDepartmentDTO);
        if (admRefDepartmentDTO.getId() == null) {
            return createAdmRefDepartment(admRefDepartmentDTO);
        }
        AdmRefDepartment admRefDepartment = admRefDepartmentMapper.toEntity(admRefDepartmentDTO);
        admRefDepartment = admRefDepartmentRepository.save(admRefDepartment);
        AdmRefDepartmentDTO result = admRefDepartmentMapper.toDto(admRefDepartment);
        admRefDepartmentSearchRepository.save(admRefDepartment);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, admRefDepartmentDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /adm-ref-departments : get all the admRefDepartments.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of admRefDepartments in body
     */
    @GetMapping("/adm-ref-departments")
    @Timed
    public List<AdmRefDepartmentDTO> getAllAdmRefDepartments() {
        log.debug("REST request to get all AdmRefDepartments");
        List<AdmRefDepartment> admRefDepartments = admRefDepartmentRepository.findAll();
        return admRefDepartmentMapper.toDto(admRefDepartments);
        }

    /**
     * GET  /adm-ref-departments/:id : get the "id" admRefDepartment.
     *
     * @param id the id of the admRefDepartmentDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the admRefDepartmentDTO, or with status 404 (Not Found)
     */
    @GetMapping("/adm-ref-departments/{id}")
    @Timed
    public ResponseEntity<AdmRefDepartmentDTO> getAdmRefDepartment(@PathVariable Long id) {
        log.debug("REST request to get AdmRefDepartment : {}", id);
        AdmRefDepartment admRefDepartment = admRefDepartmentRepository.findOne(id);
        AdmRefDepartmentDTO admRefDepartmentDTO = admRefDepartmentMapper.toDto(admRefDepartment);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(admRefDepartmentDTO));
    }

    /**
     * DELETE  /adm-ref-departments/:id : delete the "id" admRefDepartment.
     *
     * @param id the id of the admRefDepartmentDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/adm-ref-departments/{id}")
    @Timed
    public ResponseEntity<Void> deleteAdmRefDepartment(@PathVariable Long id) {
        log.debug("REST request to delete AdmRefDepartment : {}", id);
        admRefDepartmentRepository.delete(id);
        admRefDepartmentSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/adm-ref-departments?query=:query : search for the admRefDepartment corresponding
     * to the query.
     *
     * @param query the query of the admRefDepartment search
     * @return the result of the search
     */
    @GetMapping("/_search/adm-ref-departments")
    @Timed
    public List<AdmRefDepartmentDTO> searchAdmRefDepartments(@RequestParam String query) {
        log.debug("REST request to search AdmRefDepartments for query {}", query);
        return StreamSupport
            .stream(admRefDepartmentSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(admRefDepartmentMapper::toDto)
            .collect(Collectors.toList());
    }

}
