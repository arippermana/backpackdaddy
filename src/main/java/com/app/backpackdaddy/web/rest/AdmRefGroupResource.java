package com.app.backpackdaddy.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.app.backpackdaddy.domain.AdmRefGroup;

import com.app.backpackdaddy.repository.AdmRefGroupRepository;
import com.app.backpackdaddy.repository.search.AdmRefGroupSearchRepository;
import com.app.backpackdaddy.web.rest.errors.BadRequestAlertException;
import com.app.backpackdaddy.web.rest.util.HeaderUtil;
import com.app.backpackdaddy.service.dto.AdmRefGroupDTO;
import com.app.backpackdaddy.service.mapper.AdmRefGroupMapper;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing AdmRefGroup.
 */
@RestController
@RequestMapping("/api")
public class AdmRefGroupResource {

    private final Logger log = LoggerFactory.getLogger(AdmRefGroupResource.class);

    private static final String ENTITY_NAME = "admRefGroup";

    private final AdmRefGroupRepository admRefGroupRepository;

    private final AdmRefGroupMapper admRefGroupMapper;

    private final AdmRefGroupSearchRepository admRefGroupSearchRepository;

    public AdmRefGroupResource(AdmRefGroupRepository admRefGroupRepository, AdmRefGroupMapper admRefGroupMapper, AdmRefGroupSearchRepository admRefGroupSearchRepository) {
        this.admRefGroupRepository = admRefGroupRepository;
        this.admRefGroupMapper = admRefGroupMapper;
        this.admRefGroupSearchRepository = admRefGroupSearchRepository;
    }

    /**
     * POST  /adm-ref-groups : Create a new admRefGroup.
     *
     * @param admRefGroupDTO the admRefGroupDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new admRefGroupDTO, or with status 400 (Bad Request) if the admRefGroup has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/adm-ref-groups")
    @Timed
    public ResponseEntity<AdmRefGroupDTO> createAdmRefGroup(@Valid @RequestBody AdmRefGroupDTO admRefGroupDTO) throws URISyntaxException {
        log.debug("REST request to save AdmRefGroup : {}", admRefGroupDTO);
        if (admRefGroupDTO.getId() != null) {
            throw new BadRequestAlertException("A new admRefGroup cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AdmRefGroup admRefGroup = admRefGroupMapper.toEntity(admRefGroupDTO);
        admRefGroup = admRefGroupRepository.save(admRefGroup);
        AdmRefGroupDTO result = admRefGroupMapper.toDto(admRefGroup);
        admRefGroupSearchRepository.save(admRefGroup);
        return ResponseEntity.created(new URI("/api/adm-ref-groups/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /adm-ref-groups : Updates an existing admRefGroup.
     *
     * @param admRefGroupDTO the admRefGroupDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated admRefGroupDTO,
     * or with status 400 (Bad Request) if the admRefGroupDTO is not valid,
     * or with status 500 (Internal Server Error) if the admRefGroupDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/adm-ref-groups")
    @Timed
    public ResponseEntity<AdmRefGroupDTO> updateAdmRefGroup(@Valid @RequestBody AdmRefGroupDTO admRefGroupDTO) throws URISyntaxException {
        log.debug("REST request to update AdmRefGroup : {}", admRefGroupDTO);
        if (admRefGroupDTO.getId() == null) {
            return createAdmRefGroup(admRefGroupDTO);
        }
        AdmRefGroup admRefGroup = admRefGroupMapper.toEntity(admRefGroupDTO);
        admRefGroup = admRefGroupRepository.save(admRefGroup);
        AdmRefGroupDTO result = admRefGroupMapper.toDto(admRefGroup);
        admRefGroupSearchRepository.save(admRefGroup);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, admRefGroupDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /adm-ref-groups : get all the admRefGroups.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of admRefGroups in body
     */
    @GetMapping("/adm-ref-groups")
    @Timed
    public List<AdmRefGroupDTO> getAllAdmRefGroups() {
        log.debug("REST request to get all AdmRefGroups");
        List<AdmRefGroup> admRefGroups = admRefGroupRepository.findAll();
        return admRefGroupMapper.toDto(admRefGroups);
        }

    /**
     * GET  /adm-ref-groups/:id : get the "id" admRefGroup.
     *
     * @param id the id of the admRefGroupDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the admRefGroupDTO, or with status 404 (Not Found)
     */
    @GetMapping("/adm-ref-groups/{id}")
    @Timed
    public ResponseEntity<AdmRefGroupDTO> getAdmRefGroup(@PathVariable Long id) {
        log.debug("REST request to get AdmRefGroup : {}", id);
        AdmRefGroup admRefGroup = admRefGroupRepository.findOne(id);
        AdmRefGroupDTO admRefGroupDTO = admRefGroupMapper.toDto(admRefGroup);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(admRefGroupDTO));
    }

    /**
     * DELETE  /adm-ref-groups/:id : delete the "id" admRefGroup.
     *
     * @param id the id of the admRefGroupDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/adm-ref-groups/{id}")
    @Timed
    public ResponseEntity<Void> deleteAdmRefGroup(@PathVariable Long id) {
        log.debug("REST request to delete AdmRefGroup : {}", id);
        admRefGroupRepository.delete(id);
        admRefGroupSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/adm-ref-groups?query=:query : search for the admRefGroup corresponding
     * to the query.
     *
     * @param query the query of the admRefGroup search
     * @return the result of the search
     */
    @GetMapping("/_search/adm-ref-groups")
    @Timed
    public List<AdmRefGroupDTO> searchAdmRefGroups(@RequestParam String query) {
        log.debug("REST request to search AdmRefGroups for query {}", query);
        return StreamSupport
            .stream(admRefGroupSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(admRefGroupMapper::toDto)
            .collect(Collectors.toList());
    }

}
