package com.app.backpackdaddy.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.app.backpackdaddy.domain.AdmRefPosition;

import com.app.backpackdaddy.repository.AdmRefPositionRepository;
import com.app.backpackdaddy.repository.search.AdmRefPositionSearchRepository;
import com.app.backpackdaddy.web.rest.errors.BadRequestAlertException;
import com.app.backpackdaddy.web.rest.util.HeaderUtil;
import com.app.backpackdaddy.service.dto.AdmRefPositionDTO;
import com.app.backpackdaddy.service.mapper.AdmRefPositionMapper;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing AdmRefPosition.
 */
@RestController
@RequestMapping("/api")
public class AdmRefPositionResource {

    private final Logger log = LoggerFactory.getLogger(AdmRefPositionResource.class);

    private static final String ENTITY_NAME = "admRefPosition";

    private final AdmRefPositionRepository admRefPositionRepository;

    private final AdmRefPositionMapper admRefPositionMapper;

    private final AdmRefPositionSearchRepository admRefPositionSearchRepository;

    public AdmRefPositionResource(AdmRefPositionRepository admRefPositionRepository, AdmRefPositionMapper admRefPositionMapper, AdmRefPositionSearchRepository admRefPositionSearchRepository) {
        this.admRefPositionRepository = admRefPositionRepository;
        this.admRefPositionMapper = admRefPositionMapper;
        this.admRefPositionSearchRepository = admRefPositionSearchRepository;
    }

    /**
     * POST  /adm-ref-positions : Create a new admRefPosition.
     *
     * @param admRefPositionDTO the admRefPositionDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new admRefPositionDTO, or with status 400 (Bad Request) if the admRefPosition has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/adm-ref-positions")
    @Timed
    public ResponseEntity<AdmRefPositionDTO> createAdmRefPosition(@Valid @RequestBody AdmRefPositionDTO admRefPositionDTO) throws URISyntaxException {
        log.debug("REST request to save AdmRefPosition : {}", admRefPositionDTO);
        if (admRefPositionDTO.getId() != null) {
            throw new BadRequestAlertException("A new admRefPosition cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AdmRefPosition admRefPosition = admRefPositionMapper.toEntity(admRefPositionDTO);
        admRefPosition = admRefPositionRepository.save(admRefPosition);
        AdmRefPositionDTO result = admRefPositionMapper.toDto(admRefPosition);
        admRefPositionSearchRepository.save(admRefPosition);
        return ResponseEntity.created(new URI("/api/adm-ref-positions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /adm-ref-positions : Updates an existing admRefPosition.
     *
     * @param admRefPositionDTO the admRefPositionDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated admRefPositionDTO,
     * or with status 400 (Bad Request) if the admRefPositionDTO is not valid,
     * or with status 500 (Internal Server Error) if the admRefPositionDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/adm-ref-positions")
    @Timed
    public ResponseEntity<AdmRefPositionDTO> updateAdmRefPosition(@Valid @RequestBody AdmRefPositionDTO admRefPositionDTO) throws URISyntaxException {
        log.debug("REST request to update AdmRefPosition : {}", admRefPositionDTO);
        if (admRefPositionDTO.getId() == null) {
            return createAdmRefPosition(admRefPositionDTO);
        }
        AdmRefPosition admRefPosition = admRefPositionMapper.toEntity(admRefPositionDTO);
        admRefPosition = admRefPositionRepository.save(admRefPosition);
        AdmRefPositionDTO result = admRefPositionMapper.toDto(admRefPosition);
        admRefPositionSearchRepository.save(admRefPosition);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, admRefPositionDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /adm-ref-positions : get all the admRefPositions.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of admRefPositions in body
     */
    @GetMapping("/adm-ref-positions")
    @Timed
    public List<AdmRefPositionDTO> getAllAdmRefPositions() {
        log.debug("REST request to get all AdmRefPositions");
        List<AdmRefPosition> admRefPositions = admRefPositionRepository.findAll();
        return admRefPositionMapper.toDto(admRefPositions);
        }

    /**
     * GET  /adm-ref-positions/:id : get the "id" admRefPosition.
     *
     * @param id the id of the admRefPositionDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the admRefPositionDTO, or with status 404 (Not Found)
     */
    @GetMapping("/adm-ref-positions/{id}")
    @Timed
    public ResponseEntity<AdmRefPositionDTO> getAdmRefPosition(@PathVariable Long id) {
        log.debug("REST request to get AdmRefPosition : {}", id);
        AdmRefPosition admRefPosition = admRefPositionRepository.findOne(id);
        AdmRefPositionDTO admRefPositionDTO = admRefPositionMapper.toDto(admRefPosition);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(admRefPositionDTO));
    }

    /**
     * DELETE  /adm-ref-positions/:id : delete the "id" admRefPosition.
     *
     * @param id the id of the admRefPositionDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/adm-ref-positions/{id}")
    @Timed
    public ResponseEntity<Void> deleteAdmRefPosition(@PathVariable Long id) {
        log.debug("REST request to delete AdmRefPosition : {}", id);
        admRefPositionRepository.delete(id);
        admRefPositionSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/adm-ref-positions?query=:query : search for the admRefPosition corresponding
     * to the query.
     *
     * @param query the query of the admRefPosition search
     * @return the result of the search
     */
    @GetMapping("/_search/adm-ref-positions")
    @Timed
    public List<AdmRefPositionDTO> searchAdmRefPositions(@RequestParam String query) {
        log.debug("REST request to search AdmRefPositions for query {}", query);
        return StreamSupport
            .stream(admRefPositionSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(admRefPositionMapper::toDto)
            .collect(Collectors.toList());
    }

}
