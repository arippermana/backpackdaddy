package com.app.backpackdaddy.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.app.backpackdaddy.domain.AdmStaff;

import com.app.backpackdaddy.repository.AdmStaffRepository;
import com.app.backpackdaddy.repository.search.AdmStaffSearchRepository;
import com.app.backpackdaddy.web.rest.errors.BadRequestAlertException;
import com.app.backpackdaddy.web.rest.util.HeaderUtil;
import com.app.backpackdaddy.service.dto.AdmStaffDTO;
import com.app.backpackdaddy.service.mapper.AdmStaffMapper;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing AdmStaff.
 */
@RestController
@RequestMapping("/api")
public class AdmStaffResource {

    private final Logger log = LoggerFactory.getLogger(AdmStaffResource.class);

    private static final String ENTITY_NAME = "admStaff";

    private final AdmStaffRepository admStaffRepository;

    private final AdmStaffMapper admStaffMapper;

    private final AdmStaffSearchRepository admStaffSearchRepository;

    public AdmStaffResource(AdmStaffRepository admStaffRepository, AdmStaffMapper admStaffMapper, AdmStaffSearchRepository admStaffSearchRepository) {
        this.admStaffRepository = admStaffRepository;
        this.admStaffMapper = admStaffMapper;
        this.admStaffSearchRepository = admStaffSearchRepository;
    }

    /**
     * POST  /adm-staffs : Create a new admStaff.
     *
     * @param admStaffDTO the admStaffDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new admStaffDTO, or with status 400 (Bad Request) if the admStaff has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/adm-staffs")
    @Timed
    public ResponseEntity<AdmStaffDTO> createAdmStaff(@Valid @RequestBody AdmStaffDTO admStaffDTO) throws URISyntaxException {
        log.debug("REST request to save AdmStaff : {}", admStaffDTO);
        if (admStaffDTO.getId() != null) {
            throw new BadRequestAlertException("A new admStaff cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AdmStaff admStaff = admStaffMapper.toEntity(admStaffDTO);
        admStaff = admStaffRepository.save(admStaff);
        AdmStaffDTO result = admStaffMapper.toDto(admStaff);
        admStaffSearchRepository.save(admStaff);
        return ResponseEntity.created(new URI("/api/adm-staffs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /adm-staffs : Updates an existing admStaff.
     *
     * @param admStaffDTO the admStaffDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated admStaffDTO,
     * or with status 400 (Bad Request) if the admStaffDTO is not valid,
     * or with status 500 (Internal Server Error) if the admStaffDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/adm-staffs")
    @Timed
    public ResponseEntity<AdmStaffDTO> updateAdmStaff(@Valid @RequestBody AdmStaffDTO admStaffDTO) throws URISyntaxException {
        log.debug("REST request to update AdmStaff : {}", admStaffDTO);
        if (admStaffDTO.getId() == null) {
            return createAdmStaff(admStaffDTO);
        }
        AdmStaff admStaff = admStaffMapper.toEntity(admStaffDTO);
        admStaff = admStaffRepository.save(admStaff);
        AdmStaffDTO result = admStaffMapper.toDto(admStaff);
        admStaffSearchRepository.save(admStaff);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, admStaffDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /adm-staffs : get all the admStaffs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of admStaffs in body
     */
    @GetMapping("/adm-staffs")
    @Timed
    public List<AdmStaffDTO> getAllAdmStaffs() {
        log.debug("REST request to get all AdmStaffs");
        List<AdmStaff> admStaffs = admStaffRepository.findAll();
        return admStaffMapper.toDto(admStaffs);
        }

    /**
     * GET  /adm-staffs/:id : get the "id" admStaff.
     *
     * @param id the id of the admStaffDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the admStaffDTO, or with status 404 (Not Found)
     */
    @GetMapping("/adm-staffs/{id}")
    @Timed
    public ResponseEntity<AdmStaffDTO> getAdmStaff(@PathVariable Long id) {
        log.debug("REST request to get AdmStaff : {}", id);
        AdmStaff admStaff = admStaffRepository.findOne(id);
        AdmStaffDTO admStaffDTO = admStaffMapper.toDto(admStaff);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(admStaffDTO));
    }

    /**
     * DELETE  /adm-staffs/:id : delete the "id" admStaff.
     *
     * @param id the id of the admStaffDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/adm-staffs/{id}")
    @Timed
    public ResponseEntity<Void> deleteAdmStaff(@PathVariable Long id) {
        log.debug("REST request to delete AdmStaff : {}", id);
        admStaffRepository.delete(id);
        admStaffSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/adm-staffs?query=:query : search for the admStaff corresponding
     * to the query.
     *
     * @param query the query of the admStaff search
     * @return the result of the search
     */
    @GetMapping("/_search/adm-staffs")
    @Timed
    public List<AdmStaffDTO> searchAdmStaffs(@RequestParam String query) {
        log.debug("REST request to search AdmStaffs for query {}", query);
        return StreamSupport
            .stream(admStaffSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(admStaffMapper::toDto)
            .collect(Collectors.toList());
    }

}
