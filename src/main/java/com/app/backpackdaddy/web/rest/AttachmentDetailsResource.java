package com.app.backpackdaddy.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.app.backpackdaddy.service.AttachmentDetailsService;
import com.app.backpackdaddy.web.rest.errors.BadRequestAlertException;
import com.app.backpackdaddy.web.rest.util.HeaderUtil;
import com.app.backpackdaddy.service.dto.AttachmentDetailsDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing AttachmentDetails.
 */
@RestController
@RequestMapping("/api")
public class AttachmentDetailsResource {

    private final Logger log = LoggerFactory.getLogger(AttachmentDetailsResource.class);

    private static final String ENTITY_NAME = "attachmentDetails";

    private final AttachmentDetailsService attachmentDetailsService;

    public AttachmentDetailsResource(AttachmentDetailsService attachmentDetailsService) {
        this.attachmentDetailsService = attachmentDetailsService;
    }

    /**
     * POST  /attachment-details : Create a new attachmentDetails.
     *
     * @param attachmentDetailsDTO the attachmentDetailsDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new attachmentDetailsDTO, or with status 400 (Bad Request) if the attachmentDetails has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/attachment-details")
    @Timed
    public ResponseEntity<AttachmentDetailsDTO> createAttachmentDetails(@Valid @RequestBody AttachmentDetailsDTO attachmentDetailsDTO) throws URISyntaxException {
        log.debug("REST request to save AttachmentDetails : {}", attachmentDetailsDTO);
        if (attachmentDetailsDTO.getId() != null) {
            throw new BadRequestAlertException("A new attachmentDetails cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AttachmentDetailsDTO result = attachmentDetailsService.save(attachmentDetailsDTO);
        return ResponseEntity.created(new URI("/api/attachment-details/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /attachment-details : Updates an existing attachmentDetails.
     *
     * @param attachmentDetailsDTO the attachmentDetailsDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated attachmentDetailsDTO,
     * or with status 400 (Bad Request) if the attachmentDetailsDTO is not valid,
     * or with status 500 (Internal Server Error) if the attachmentDetailsDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/attachment-details")
    @Timed
    public ResponseEntity<AttachmentDetailsDTO> updateAttachmentDetails(@Valid @RequestBody AttachmentDetailsDTO attachmentDetailsDTO) throws URISyntaxException {
        log.debug("REST request to update AttachmentDetails : {}", attachmentDetailsDTO);
        if (attachmentDetailsDTO.getId() == null) {
            return createAttachmentDetails(attachmentDetailsDTO);
        }
        AttachmentDetailsDTO result = attachmentDetailsService.save(attachmentDetailsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, attachmentDetailsDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /attachment-details : get all the attachmentDetails.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of attachmentDetails in body
     */
    @GetMapping("/attachment-details")
    @Timed
    public List<AttachmentDetailsDTO> getAllAttachmentDetails() {
        log.debug("REST request to get all AttachmentDetails");
        return attachmentDetailsService.findAll();
        }

    /**
     * GET  /attachment-details/:id : get the "id" attachmentDetails.
     *
     * @param id the id of the attachmentDetailsDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the attachmentDetailsDTO, or with status 404 (Not Found)
     */
    @GetMapping("/attachment-details/{id}")
    @Timed
    public ResponseEntity<AttachmentDetailsDTO> getAttachmentDetails(@PathVariable Long id) {
        log.debug("REST request to get AttachmentDetails : {}", id);
        AttachmentDetailsDTO attachmentDetailsDTO = attachmentDetailsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(attachmentDetailsDTO));
    }

    /**
     * DELETE  /attachment-details/:id : delete the "id" attachmentDetails.
     *
     * @param id the id of the attachmentDetailsDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/attachment-details/{id}")
    @Timed
    public ResponseEntity<Void> deleteAttachmentDetails(@PathVariable Long id) {
        log.debug("REST request to delete AttachmentDetails : {}", id);
        attachmentDetailsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/attachment-details?query=:query : search for the attachmentDetails corresponding
     * to the query.
     *
     * @param query the query of the attachmentDetails search
     * @return the result of the search
     */
    @GetMapping("/_search/attachment-details")
    @Timed
    public List<AttachmentDetailsDTO> searchAttachmentDetails(@RequestParam String query) {
        log.debug("REST request to search AttachmentDetails for query {}", query);
        return attachmentDetailsService.search(query);
    }

}
