package com.app.backpackdaddy.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.app.backpackdaddy.domain.Occupation;

import com.app.backpackdaddy.repository.OccupationRepository;
import com.app.backpackdaddy.repository.search.OccupationSearchRepository;
import com.app.backpackdaddy.web.rest.errors.BadRequestAlertException;
import com.app.backpackdaddy.web.rest.util.HeaderUtil;
import com.app.backpackdaddy.service.dto.OccupationDTO;
import com.app.backpackdaddy.service.mapper.OccupationMapper;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Occupation.
 */
@RestController
@RequestMapping("/api")
public class OccupationResource {

    private final Logger log = LoggerFactory.getLogger(OccupationResource.class);

    private static final String ENTITY_NAME = "occupation";

    private final OccupationRepository occupationRepository;

    private final OccupationMapper occupationMapper;

    private final OccupationSearchRepository occupationSearchRepository;

    public OccupationResource(OccupationRepository occupationRepository, OccupationMapper occupationMapper, OccupationSearchRepository occupationSearchRepository) {
        this.occupationRepository = occupationRepository;
        this.occupationMapper = occupationMapper;
        this.occupationSearchRepository = occupationSearchRepository;
    }

    /**
     * POST  /occupations : Create a new occupation.
     *
     * @param occupationDTO the occupationDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new occupationDTO, or with status 400 (Bad Request) if the occupation has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/occupations")
    @Timed
    public ResponseEntity<OccupationDTO> createOccupation(@Valid @RequestBody OccupationDTO occupationDTO) throws URISyntaxException {
        log.debug("REST request to save Occupation : {}", occupationDTO);
        if (occupationDTO.getId() != null) {
            throw new BadRequestAlertException("A new occupation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Occupation occupation = occupationMapper.toEntity(occupationDTO);
        occupation = occupationRepository.save(occupation);
        OccupationDTO result = occupationMapper.toDto(occupation);
        occupationSearchRepository.save(occupation);
        return ResponseEntity.created(new URI("/api/occupations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /occupations : Updates an existing occupation.
     *
     * @param occupationDTO the occupationDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated occupationDTO,
     * or with status 400 (Bad Request) if the occupationDTO is not valid,
     * or with status 500 (Internal Server Error) if the occupationDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/occupations")
    @Timed
    public ResponseEntity<OccupationDTO> updateOccupation(@Valid @RequestBody OccupationDTO occupationDTO) throws URISyntaxException {
        log.debug("REST request to update Occupation : {}", occupationDTO);
        if (occupationDTO.getId() == null) {
            return createOccupation(occupationDTO);
        }
        Occupation occupation = occupationMapper.toEntity(occupationDTO);
        occupation = occupationRepository.save(occupation);
        OccupationDTO result = occupationMapper.toDto(occupation);
        occupationSearchRepository.save(occupation);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, occupationDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /occupations : get all the occupations.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of occupations in body
     */
    @GetMapping("/occupations")
    @Timed
    public List<OccupationDTO> getAllOccupations() {
        log.debug("REST request to get all Occupations");
        List<Occupation> occupations = occupationRepository.findAll();
        return occupationMapper.toDto(occupations);
        }

    /**
     * GET  /occupations/:id : get the "id" occupation.
     *
     * @param id the id of the occupationDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the occupationDTO, or with status 404 (Not Found)
     */
    @GetMapping("/occupations/{id}")
    @Timed
    public ResponseEntity<OccupationDTO> getOccupation(@PathVariable Long id) {
        log.debug("REST request to get Occupation : {}", id);
        Occupation occupation = occupationRepository.findOne(id);
        OccupationDTO occupationDTO = occupationMapper.toDto(occupation);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(occupationDTO));
    }

    /**
     * DELETE  /occupations/:id : delete the "id" occupation.
     *
     * @param id the id of the occupationDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/occupations/{id}")
    @Timed
    public ResponseEntity<Void> deleteOccupation(@PathVariable Long id) {
        log.debug("REST request to delete Occupation : {}", id);
        occupationRepository.delete(id);
        occupationSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/occupations?query=:query : search for the occupation corresponding
     * to the query.
     *
     * @param query the query of the occupation search
     * @return the result of the search
     */
    @GetMapping("/_search/occupations")
    @Timed
    public List<OccupationDTO> searchOccupations(@RequestParam String query) {
        log.debug("REST request to search Occupations for query {}", query);
        return StreamSupport
            .stream(occupationSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(occupationMapper::toDto)
            .collect(Collectors.toList());
    }

}
