package com.app.backpackdaddy.web.rest.vm;

import com.app.backpackdaddy.service.dto.UserProfileDTO;

public class ManagedUserProfileVM extends UserProfileDTO {

    private String key;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
