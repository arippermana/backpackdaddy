/**
 * View Models used by Spring MVC REST controllers.
 */
package com.app.backpackdaddy.web.rest.vm;
