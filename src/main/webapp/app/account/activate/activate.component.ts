import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { UserProfile } from '../../entities/user-profile/user-profile.model';
import { ActivateService } from './activate.service';
import { LoginModalService } from '../../shared';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { UserProfilePopupService } from '../../entities/user-profile/user-profile-popup.service';
import { UserProfileService } from '../../entities/user-profile/user-profile.service';
import { Gender, GenderService } from '../../entities/gender';
import { Country, CountryService } from '../../entities/country';
import { Occupation, OccupationService } from '../../entities/occupation';
import { Language, LanguageService } from '../../entities/language';
import { Currency, CurrencyService } from '../../entities/currency';
import { ResponseWrapper } from '../../shared';
import { JhiDateUtils } from 'ng-jhipster';

@Component({
    selector: 'jhi-activate',
    templateUrl: './activate.component.html'
})
export class ActivateComponent implements OnInit {
    error: string;
    success: string;
    isActive: boolean;
    modalRef: NgbModalRef;
    userProfile: any;
    isSaving: boolean;
    genders: Gender[];

    countries: Country[];

    occupations: Occupation[];

    languages: Language[];

    currencies: Currency[];
    birthdateDp: any;
    key: any;

    constructor(
        private activateService: ActivateService,
        private loginModalService: LoginModalService,
        private route: ActivatedRoute,
        private jhiAlertService: JhiAlertService,
        private userProfileService: UserProfileService,
        private genderService: GenderService,
        private countryService: CountryService,
        private occupationService: OccupationService,
        private languageService: LanguageService,
        private currencyService: CurrencyService,
        private eventManager: JhiEventManager,
        private dateUtils: JhiDateUtils
    ) {
    }

    ngOnInit() {
        this.userProfile = {};
        this.isSaving = false;
        this.genderService.query()
            .subscribe((res: ResponseWrapper) => { this.genders = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.countryService.query()
            .subscribe((res: ResponseWrapper) => { this.countries = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.occupationService.query()
            .subscribe((res: ResponseWrapper) => { this.occupations = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.languageService.query()
            .subscribe((res: ResponseWrapper) => { this.languages = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.currencyService.query()
            .subscribe((res: ResponseWrapper) => { this.currencies = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.route.queryParams.subscribe((params) => {
            this.key = params['key'];
            this.activateService.check(params['key']).subscribe(() => {
                this.error = null;
                this.success = 'OK';
                this.isActive = false;
            }, () => {
                this.success = null;
                this.error = 'ERROR';
                this.isActive = false;
            });
        });
    }

    login() {
        this.modalRef = this.loginModalService.open();
    }

    clear() {
    }

    save() {
        this.userProfile.key = this.key;
        this.userProfile.birthdate = this.dateUtils
            .convertLocalDateToServer(this.userProfile.birthdate);
        this.userProfileService.activateProfile(this.userProfile).subscribe(() => {
            this.route.queryParams.subscribe((params) => {
                this.activateService.get(params['key']).subscribe(() => {
                    this.error = null;
                    this.success = 'OK';
                    this.isActive = true;
                }, () => {
                    this.success = null;
                    this.error = 'ERROR';
                    this.isActive = false;
                });
            });
        }, () => {
            this.success = null;
            this.error = 'ERROR';
            this.isActive = false;
        });
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
