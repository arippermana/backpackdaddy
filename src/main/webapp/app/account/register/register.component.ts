import { Component, OnInit, AfterViewInit, Renderer, ElementRef, Input, ViewChild } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiLanguageService } from 'ng-jhipster';

import { Register } from './register.service';
import { LoginModalService, EMAIL_ALREADY_USED_TYPE, LOGIN_ALREADY_USED_TYPE } from '../../shared';

@Component({
    selector: 'jhi-register',
    templateUrl: './register.component.html'
})
export class RegisterComponent implements OnInit, AfterViewInit {

    confirmPassword: string;
    doNotMatch: string;
    error: string;
    errorEmailExists: string;
    errorUserExists: string;
    registerAccount: any;
    success: boolean;
    modalRef: NgbModalRef;

    @ViewChild('showHide') el: ElementRef;
    @ViewChild('toggler') toggler: ElementRef;
    @Input() behaviour = 'click';

    constructor(
        private languageService: JhiLanguageService,
        private loginModalService: LoginModalService,
        private registerService: Register,
        private elementRef: ElementRef,
        private renderer: Renderer
    ) {
    }

    ngOnInit() {
        this.success = false;
        this.registerAccount = {};
    }

    ngAfterViewInit() {
        const textbox = this.el.nativeElement;
        const toggler = this.toggler.nativeElement;
        const togglerIcon = toggler.childNodes[0];

        if (this.behaviour === 'press') {
            toggler.addEventListener('mousedown', (e) => {
                textbox.type = 'text';
                togglerIcon.classList.remove('fa-eye');
                togglerIcon.classList.add('fa-eye-slash');
            });
            toggler.addEventListener('mouseup', (e) => {
                textbox.type = 'password';
                togglerIcon.classList.remove('fa-eye-slash');
                togglerIcon.classList.add('fa-eye');
            });
        }

        if (this.behaviour === 'click') {
            toggler.addEventListener('click', (e) => {
                textbox.type = textbox.type === 'password' ? 'text' : 'password';
                togglerIcon.classList.toggle('fa-eye');
                togglerIcon.classList.toggle('fa-eye-slash');
            });
        }
    }

    register() {
        this.registerAccount.login = this.registerAccount.email
        this.doNotMatch = null;
        this.error = null;
        this.errorUserExists = null;
        this.errorEmailExists = null;
        this.languageService.getCurrent().then((key) => {
            this.registerAccount.langKey = key;
            this.registerService.save(this.registerAccount).subscribe(() => {
                this.success = true;
            }, (response) => this.processError(response));
        });
    }

    openLogin() {
        this.modalRef = this.loginModalService.open();
    }

    private processError(response) {
        this.success = null;
        if (response.status === 400 && response.json().type === LOGIN_ALREADY_USED_TYPE) {
            this.errorUserExists = 'ERROR';
        } else if (response.status === 400 && response.json().type === EMAIL_ALREADY_USED_TYPE) {
            this.errorEmailExists = 'ERROR';
        } else {
            this.error = 'ERROR';
        }
    }
}
