import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { AdmAuditTrail } from './adm-audit-trail.model';
import { AdmAuditTrailPopupService } from './adm-audit-trail-popup.service';
import { AdmAuditTrailService } from './adm-audit-trail.service';

@Component({
    selector: 'jhi-adm-audit-trail-delete-dialog',
    templateUrl: './adm-audit-trail-delete-dialog.component.html'
})
export class AdmAuditTrailDeleteDialogComponent {

    admAuditTrail: AdmAuditTrail;

    constructor(
        private admAuditTrailService: AdmAuditTrailService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.admAuditTrailService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'admAuditTrailListModification',
                content: 'Deleted an admAuditTrail'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-adm-audit-trail-delete-popup',
    template: ''
})
export class AdmAuditTrailDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private admAuditTrailPopupService: AdmAuditTrailPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.admAuditTrailPopupService
                .open(AdmAuditTrailDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
