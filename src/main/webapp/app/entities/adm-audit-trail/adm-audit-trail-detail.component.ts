import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { AdmAuditTrail } from './adm-audit-trail.model';
import { AdmAuditTrailService } from './adm-audit-trail.service';

@Component({
    selector: 'jhi-adm-audit-trail-detail',
    templateUrl: './adm-audit-trail-detail.component.html'
})
export class AdmAuditTrailDetailComponent implements OnInit, OnDestroy {

    admAuditTrail: AdmAuditTrail;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private admAuditTrailService: AdmAuditTrailService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInAdmAuditTrails();
    }

    load(id) {
        this.admAuditTrailService.find(id).subscribe((admAuditTrail) => {
            this.admAuditTrail = admAuditTrail;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInAdmAuditTrails() {
        this.eventSubscriber = this.eventManager.subscribe(
            'admAuditTrailListModification',
            (response) => this.load(this.admAuditTrail.id)
        );
    }
}
