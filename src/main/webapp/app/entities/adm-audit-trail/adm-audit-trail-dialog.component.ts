import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { AdmAuditTrail } from './adm-audit-trail.model';
import { AdmAuditTrailPopupService } from './adm-audit-trail-popup.service';
import { AdmAuditTrailService } from './adm-audit-trail.service';

@Component({
    selector: 'jhi-adm-audit-trail-dialog',
    templateUrl: './adm-audit-trail-dialog.component.html'
})
export class AdmAuditTrailDialogComponent implements OnInit {

    admAuditTrail: AdmAuditTrail;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private admAuditTrailService: AdmAuditTrailService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.admAuditTrail.id !== undefined) {
            this.subscribeToSaveResponse(
                this.admAuditTrailService.update(this.admAuditTrail));
        } else {
            this.subscribeToSaveResponse(
                this.admAuditTrailService.create(this.admAuditTrail));
        }
    }

    private subscribeToSaveResponse(result: Observable<AdmAuditTrail>) {
        result.subscribe((res: AdmAuditTrail) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: AdmAuditTrail) {
        this.eventManager.broadcast({ name: 'admAuditTrailListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-adm-audit-trail-popup',
    template: ''
})
export class AdmAuditTrailPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private admAuditTrailPopupService: AdmAuditTrailPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.admAuditTrailPopupService
                    .open(AdmAuditTrailDialogComponent as Component, params['id']);
            } else {
                this.admAuditTrailPopupService
                    .open(AdmAuditTrailDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
