import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AdmAuditTrail } from './adm-audit-trail.model';
import { AdmAuditTrailService } from './adm-audit-trail.service';

@Injectable()
export class AdmAuditTrailPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private admAuditTrailService: AdmAuditTrailService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.admAuditTrailService.find(id).subscribe((admAuditTrail) => {
                    this.ngbModalRef = this.admAuditTrailModalRef(component, admAuditTrail);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.admAuditTrailModalRef(component, new AdmAuditTrail());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    admAuditTrailModalRef(component: Component, admAuditTrail: AdmAuditTrail): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.admAuditTrail = admAuditTrail;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
