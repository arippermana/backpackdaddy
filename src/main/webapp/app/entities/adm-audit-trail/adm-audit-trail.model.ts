import { BaseEntity } from './../../shared';

export class AdmAuditTrail implements BaseEntity {
    constructor(
        public id?: number,
        public tableName?: string,
        public auditStatus?: string,
        public oldValues?: string,
        public newValues?: string,
    ) {
    }
}
