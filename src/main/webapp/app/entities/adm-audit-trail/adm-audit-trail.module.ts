import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BackpackdaddySharedModule } from '../../shared';
import {
    AdmAuditTrailService,
    AdmAuditTrailPopupService,
    AdmAuditTrailComponent,
    AdmAuditTrailDetailComponent,
    AdmAuditTrailDialogComponent,
    AdmAuditTrailPopupComponent,
    AdmAuditTrailDeletePopupComponent,
    AdmAuditTrailDeleteDialogComponent,
    admAuditTrailRoute,
    admAuditTrailPopupRoute,
} from './';

const ENTITY_STATES = [
    ...admAuditTrailRoute,
    ...admAuditTrailPopupRoute,
];

@NgModule({
    imports: [
        BackpackdaddySharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        AdmAuditTrailComponent,
        AdmAuditTrailDetailComponent,
        AdmAuditTrailDialogComponent,
        AdmAuditTrailDeleteDialogComponent,
        AdmAuditTrailPopupComponent,
        AdmAuditTrailDeletePopupComponent,
    ],
    entryComponents: [
        AdmAuditTrailComponent,
        AdmAuditTrailDialogComponent,
        AdmAuditTrailPopupComponent,
        AdmAuditTrailDeleteDialogComponent,
        AdmAuditTrailDeletePopupComponent,
    ],
    providers: [
        AdmAuditTrailService,
        AdmAuditTrailPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BackpackdaddyAdmAuditTrailModule {}
