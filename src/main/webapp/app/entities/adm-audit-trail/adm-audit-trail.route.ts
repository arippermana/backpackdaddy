import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { AdmAuditTrailComponent } from './adm-audit-trail.component';
import { AdmAuditTrailDetailComponent } from './adm-audit-trail-detail.component';
import { AdmAuditTrailPopupComponent } from './adm-audit-trail-dialog.component';
import { AdmAuditTrailDeletePopupComponent } from './adm-audit-trail-delete-dialog.component';

export const admAuditTrailRoute: Routes = [
    {
        path: 'adm-audit-trail',
        component: AdmAuditTrailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backpackdaddyApp.admAuditTrail.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'adm-audit-trail/:id',
        component: AdmAuditTrailDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backpackdaddyApp.admAuditTrail.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const admAuditTrailPopupRoute: Routes = [
    {
        path: 'adm-audit-trail-new',
        component: AdmAuditTrailPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backpackdaddyApp.admAuditTrail.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'adm-audit-trail/:id/edit',
        component: AdmAuditTrailPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backpackdaddyApp.admAuditTrail.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'adm-audit-trail/:id/delete',
        component: AdmAuditTrailDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backpackdaddyApp.admAuditTrail.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
