import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { AdmRefBranch } from './adm-ref-branch.model';
import { AdmRefBranchPopupService } from './adm-ref-branch-popup.service';
import { AdmRefBranchService } from './adm-ref-branch.service';

@Component({
    selector: 'jhi-adm-ref-branch-delete-dialog',
    templateUrl: './adm-ref-branch-delete-dialog.component.html'
})
export class AdmRefBranchDeleteDialogComponent {

    admRefBranch: AdmRefBranch;

    constructor(
        private admRefBranchService: AdmRefBranchService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.admRefBranchService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'admRefBranchListModification',
                content: 'Deleted an admRefBranch'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-adm-ref-branch-delete-popup',
    template: ''
})
export class AdmRefBranchDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private admRefBranchPopupService: AdmRefBranchPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.admRefBranchPopupService
                .open(AdmRefBranchDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
