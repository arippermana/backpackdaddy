import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { AdmRefBranch } from './adm-ref-branch.model';
import { AdmRefBranchService } from './adm-ref-branch.service';

@Component({
    selector: 'jhi-adm-ref-branch-detail',
    templateUrl: './adm-ref-branch-detail.component.html'
})
export class AdmRefBranchDetailComponent implements OnInit, OnDestroy {

    admRefBranch: AdmRefBranch;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private admRefBranchService: AdmRefBranchService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInAdmRefBranches();
    }

    load(id) {
        this.admRefBranchService.find(id).subscribe((admRefBranch) => {
            this.admRefBranch = admRefBranch;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInAdmRefBranches() {
        this.eventSubscriber = this.eventManager.subscribe(
            'admRefBranchListModification',
            (response) => this.load(this.admRefBranch.id)
        );
    }
}
