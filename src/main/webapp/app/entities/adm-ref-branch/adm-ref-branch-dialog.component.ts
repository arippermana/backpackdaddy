import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { AdmRefBranch } from './adm-ref-branch.model';
import { AdmRefBranchPopupService } from './adm-ref-branch-popup.service';
import { AdmRefBranchService } from './adm-ref-branch.service';

@Component({
    selector: 'jhi-adm-ref-branch-dialog',
    templateUrl: './adm-ref-branch-dialog.component.html'
})
export class AdmRefBranchDialogComponent implements OnInit {

    admRefBranch: AdmRefBranch;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private admRefBranchService: AdmRefBranchService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.admRefBranch.id !== undefined) {
            this.subscribeToSaveResponse(
                this.admRefBranchService.update(this.admRefBranch));
        } else {
            this.subscribeToSaveResponse(
                this.admRefBranchService.create(this.admRefBranch));
        }
    }

    private subscribeToSaveResponse(result: Observable<AdmRefBranch>) {
        result.subscribe((res: AdmRefBranch) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: AdmRefBranch) {
        this.eventManager.broadcast({ name: 'admRefBranchListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-adm-ref-branch-popup',
    template: ''
})
export class AdmRefBranchPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private admRefBranchPopupService: AdmRefBranchPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.admRefBranchPopupService
                    .open(AdmRefBranchDialogComponent as Component, params['id']);
            } else {
                this.admRefBranchPopupService
                    .open(AdmRefBranchDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
