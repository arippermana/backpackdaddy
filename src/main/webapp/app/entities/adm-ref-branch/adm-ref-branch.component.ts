import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { AdmRefBranch } from './adm-ref-branch.model';
import { AdmRefBranchService } from './adm-ref-branch.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-adm-ref-branch',
    templateUrl: './adm-ref-branch.component.html'
})
export class AdmRefBranchComponent implements OnInit, OnDestroy {
admRefBranches: AdmRefBranch[];
    currentAccount: any;
    eventSubscriber: Subscription;
    currentSearch: string;

    constructor(
        private admRefBranchService: AdmRefBranchService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private activatedRoute: ActivatedRoute,
        private principal: Principal
    ) {
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        if (this.currentSearch) {
            this.admRefBranchService.search({
                query: this.currentSearch,
                }).subscribe(
                    (res: ResponseWrapper) => this.admRefBranches = res.json,
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
       }
        this.admRefBranchService.query().subscribe(
            (res: ResponseWrapper) => {
                this.admRefBranches = res.json;
                this.currentSearch = '';
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.currentSearch = query;
        this.loadAll();
    }

    clear() {
        this.currentSearch = '';
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInAdmRefBranches();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: AdmRefBranch) {
        return item.id;
    }
    registerChangeInAdmRefBranches() {
        this.eventSubscriber = this.eventManager.subscribe('admRefBranchListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
