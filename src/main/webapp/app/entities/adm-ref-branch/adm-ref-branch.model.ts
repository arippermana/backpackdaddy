import { BaseEntity } from './../../shared';

export class AdmRefBranch implements BaseEntity {
    constructor(
        public id?: number,
        public branchName?: string,
    ) {
    }
}
