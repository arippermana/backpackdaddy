import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BackpackdaddySharedModule } from '../../shared';
import {
    AdmRefBranchService,
    AdmRefBranchPopupService,
    AdmRefBranchComponent,
    AdmRefBranchDetailComponent,
    AdmRefBranchDialogComponent,
    AdmRefBranchPopupComponent,
    AdmRefBranchDeletePopupComponent,
    AdmRefBranchDeleteDialogComponent,
    admRefBranchRoute,
    admRefBranchPopupRoute,
} from './';

const ENTITY_STATES = [
    ...admRefBranchRoute,
    ...admRefBranchPopupRoute,
];

@NgModule({
    imports: [
        BackpackdaddySharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        AdmRefBranchComponent,
        AdmRefBranchDetailComponent,
        AdmRefBranchDialogComponent,
        AdmRefBranchDeleteDialogComponent,
        AdmRefBranchPopupComponent,
        AdmRefBranchDeletePopupComponent,
    ],
    entryComponents: [
        AdmRefBranchComponent,
        AdmRefBranchDialogComponent,
        AdmRefBranchPopupComponent,
        AdmRefBranchDeleteDialogComponent,
        AdmRefBranchDeletePopupComponent,
    ],
    providers: [
        AdmRefBranchService,
        AdmRefBranchPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BackpackdaddyAdmRefBranchModule {}
