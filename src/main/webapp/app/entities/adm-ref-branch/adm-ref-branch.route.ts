import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { AdmRefBranchComponent } from './adm-ref-branch.component';
import { AdmRefBranchDetailComponent } from './adm-ref-branch-detail.component';
import { AdmRefBranchPopupComponent } from './adm-ref-branch-dialog.component';
import { AdmRefBranchDeletePopupComponent } from './adm-ref-branch-delete-dialog.component';

export const admRefBranchRoute: Routes = [
    {
        path: 'adm-ref-branch',
        component: AdmRefBranchComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backpackdaddyApp.admRefBranch.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'adm-ref-branch/:id',
        component: AdmRefBranchDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backpackdaddyApp.admRefBranch.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const admRefBranchPopupRoute: Routes = [
    {
        path: 'adm-ref-branch-new',
        component: AdmRefBranchPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backpackdaddyApp.admRefBranch.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'adm-ref-branch/:id/edit',
        component: AdmRefBranchPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backpackdaddyApp.admRefBranch.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'adm-ref-branch/:id/delete',
        component: AdmRefBranchDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backpackdaddyApp.admRefBranch.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
