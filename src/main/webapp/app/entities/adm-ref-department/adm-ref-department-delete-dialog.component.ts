import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { AdmRefDepartment } from './adm-ref-department.model';
import { AdmRefDepartmentPopupService } from './adm-ref-department-popup.service';
import { AdmRefDepartmentService } from './adm-ref-department.service';

@Component({
    selector: 'jhi-adm-ref-department-delete-dialog',
    templateUrl: './adm-ref-department-delete-dialog.component.html'
})
export class AdmRefDepartmentDeleteDialogComponent {

    admRefDepartment: AdmRefDepartment;

    constructor(
        private admRefDepartmentService: AdmRefDepartmentService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.admRefDepartmentService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'admRefDepartmentListModification',
                content: 'Deleted an admRefDepartment'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-adm-ref-department-delete-popup',
    template: ''
})
export class AdmRefDepartmentDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private admRefDepartmentPopupService: AdmRefDepartmentPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.admRefDepartmentPopupService
                .open(AdmRefDepartmentDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
