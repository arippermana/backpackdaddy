import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { AdmRefDepartment } from './adm-ref-department.model';
import { AdmRefDepartmentService } from './adm-ref-department.service';

@Component({
    selector: 'jhi-adm-ref-department-detail',
    templateUrl: './adm-ref-department-detail.component.html'
})
export class AdmRefDepartmentDetailComponent implements OnInit, OnDestroy {

    admRefDepartment: AdmRefDepartment;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private admRefDepartmentService: AdmRefDepartmentService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInAdmRefDepartments();
    }

    load(id) {
        this.admRefDepartmentService.find(id).subscribe((admRefDepartment) => {
            this.admRefDepartment = admRefDepartment;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInAdmRefDepartments() {
        this.eventSubscriber = this.eventManager.subscribe(
            'admRefDepartmentListModification',
            (response) => this.load(this.admRefDepartment.id)
        );
    }
}
