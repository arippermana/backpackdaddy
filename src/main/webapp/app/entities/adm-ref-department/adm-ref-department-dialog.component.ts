import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { AdmRefDepartment } from './adm-ref-department.model';
import { AdmRefDepartmentPopupService } from './adm-ref-department-popup.service';
import { AdmRefDepartmentService } from './adm-ref-department.service';

@Component({
    selector: 'jhi-adm-ref-department-dialog',
    templateUrl: './adm-ref-department-dialog.component.html'
})
export class AdmRefDepartmentDialogComponent implements OnInit {

    admRefDepartment: AdmRefDepartment;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private admRefDepartmentService: AdmRefDepartmentService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.admRefDepartment.id !== undefined) {
            this.subscribeToSaveResponse(
                this.admRefDepartmentService.update(this.admRefDepartment));
        } else {
            this.subscribeToSaveResponse(
                this.admRefDepartmentService.create(this.admRefDepartment));
        }
    }

    private subscribeToSaveResponse(result: Observable<AdmRefDepartment>) {
        result.subscribe((res: AdmRefDepartment) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: AdmRefDepartment) {
        this.eventManager.broadcast({ name: 'admRefDepartmentListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-adm-ref-department-popup',
    template: ''
})
export class AdmRefDepartmentPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private admRefDepartmentPopupService: AdmRefDepartmentPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.admRefDepartmentPopupService
                    .open(AdmRefDepartmentDialogComponent as Component, params['id']);
            } else {
                this.admRefDepartmentPopupService
                    .open(AdmRefDepartmentDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
