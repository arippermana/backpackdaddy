import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AdmRefDepartment } from './adm-ref-department.model';
import { AdmRefDepartmentService } from './adm-ref-department.service';

@Injectable()
export class AdmRefDepartmentPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private admRefDepartmentService: AdmRefDepartmentService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.admRefDepartmentService.find(id).subscribe((admRefDepartment) => {
                    this.ngbModalRef = this.admRefDepartmentModalRef(component, admRefDepartment);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.admRefDepartmentModalRef(component, new AdmRefDepartment());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    admRefDepartmentModalRef(component: Component, admRefDepartment: AdmRefDepartment): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.admRefDepartment = admRefDepartment;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
