import { BaseEntity } from './../../shared';

export class AdmRefDepartment implements BaseEntity {
    constructor(
        public id?: number,
        public departmentName?: string,
    ) {
    }
}
