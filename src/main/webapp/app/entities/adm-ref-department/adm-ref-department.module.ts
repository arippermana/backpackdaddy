import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BackpackdaddySharedModule } from '../../shared';
import {
    AdmRefDepartmentService,
    AdmRefDepartmentPopupService,
    AdmRefDepartmentComponent,
    AdmRefDepartmentDetailComponent,
    AdmRefDepartmentDialogComponent,
    AdmRefDepartmentPopupComponent,
    AdmRefDepartmentDeletePopupComponent,
    AdmRefDepartmentDeleteDialogComponent,
    admRefDepartmentRoute,
    admRefDepartmentPopupRoute,
} from './';

const ENTITY_STATES = [
    ...admRefDepartmentRoute,
    ...admRefDepartmentPopupRoute,
];

@NgModule({
    imports: [
        BackpackdaddySharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        AdmRefDepartmentComponent,
        AdmRefDepartmentDetailComponent,
        AdmRefDepartmentDialogComponent,
        AdmRefDepartmentDeleteDialogComponent,
        AdmRefDepartmentPopupComponent,
        AdmRefDepartmentDeletePopupComponent,
    ],
    entryComponents: [
        AdmRefDepartmentComponent,
        AdmRefDepartmentDialogComponent,
        AdmRefDepartmentPopupComponent,
        AdmRefDepartmentDeleteDialogComponent,
        AdmRefDepartmentDeletePopupComponent,
    ],
    providers: [
        AdmRefDepartmentService,
        AdmRefDepartmentPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BackpackdaddyAdmRefDepartmentModule {}
