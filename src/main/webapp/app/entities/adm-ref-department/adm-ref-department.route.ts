import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { AdmRefDepartmentComponent } from './adm-ref-department.component';
import { AdmRefDepartmentDetailComponent } from './adm-ref-department-detail.component';
import { AdmRefDepartmentPopupComponent } from './adm-ref-department-dialog.component';
import { AdmRefDepartmentDeletePopupComponent } from './adm-ref-department-delete-dialog.component';

export const admRefDepartmentRoute: Routes = [
    {
        path: 'adm-ref-department',
        component: AdmRefDepartmentComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backpackdaddyApp.admRefDepartment.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'adm-ref-department/:id',
        component: AdmRefDepartmentDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backpackdaddyApp.admRefDepartment.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const admRefDepartmentPopupRoute: Routes = [
    {
        path: 'adm-ref-department-new',
        component: AdmRefDepartmentPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backpackdaddyApp.admRefDepartment.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'adm-ref-department/:id/edit',
        component: AdmRefDepartmentPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backpackdaddyApp.admRefDepartment.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'adm-ref-department/:id/delete',
        component: AdmRefDepartmentDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backpackdaddyApp.admRefDepartment.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
