import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { AdmRefDepartment } from './adm-ref-department.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class AdmRefDepartmentService {

    private resourceUrl = SERVER_API_URL + 'api/adm-ref-departments';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/adm-ref-departments';

    constructor(private http: Http) { }

    create(admRefDepartment: AdmRefDepartment): Observable<AdmRefDepartment> {
        const copy = this.convert(admRefDepartment);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(admRefDepartment: AdmRefDepartment): Observable<AdmRefDepartment> {
        const copy = this.convert(admRefDepartment);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<AdmRefDepartment> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to AdmRefDepartment.
     */
    private convertItemFromServer(json: any): AdmRefDepartment {
        const entity: AdmRefDepartment = Object.assign(new AdmRefDepartment(), json);
        return entity;
    }

    /**
     * Convert a AdmRefDepartment to a JSON which can be sent to the server.
     */
    private convert(admRefDepartment: AdmRefDepartment): AdmRefDepartment {
        const copy: AdmRefDepartment = Object.assign({}, admRefDepartment);
        return copy;
    }
}
