import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { AdmRefGroup } from './adm-ref-group.model';
import { AdmRefGroupPopupService } from './adm-ref-group-popup.service';
import { AdmRefGroupService } from './adm-ref-group.service';

@Component({
    selector: 'jhi-adm-ref-group-delete-dialog',
    templateUrl: './adm-ref-group-delete-dialog.component.html'
})
export class AdmRefGroupDeleteDialogComponent {

    admRefGroup: AdmRefGroup;

    constructor(
        private admRefGroupService: AdmRefGroupService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.admRefGroupService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'admRefGroupListModification',
                content: 'Deleted an admRefGroup'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-adm-ref-group-delete-popup',
    template: ''
})
export class AdmRefGroupDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private admRefGroupPopupService: AdmRefGroupPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.admRefGroupPopupService
                .open(AdmRefGroupDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
