import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { AdmRefGroup } from './adm-ref-group.model';
import { AdmRefGroupService } from './adm-ref-group.service';

@Component({
    selector: 'jhi-adm-ref-group-detail',
    templateUrl: './adm-ref-group-detail.component.html'
})
export class AdmRefGroupDetailComponent implements OnInit, OnDestroy {

    admRefGroup: AdmRefGroup;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private admRefGroupService: AdmRefGroupService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInAdmRefGroups();
    }

    load(id) {
        this.admRefGroupService.find(id).subscribe((admRefGroup) => {
            this.admRefGroup = admRefGroup;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInAdmRefGroups() {
        this.eventSubscriber = this.eventManager.subscribe(
            'admRefGroupListModification',
            (response) => this.load(this.admRefGroup.id)
        );
    }
}
