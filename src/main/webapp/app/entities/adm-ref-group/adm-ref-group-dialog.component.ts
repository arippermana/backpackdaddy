import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { AdmRefGroup } from './adm-ref-group.model';
import { AdmRefGroupPopupService } from './adm-ref-group-popup.service';
import { AdmRefGroupService } from './adm-ref-group.service';

@Component({
    selector: 'jhi-adm-ref-group-dialog',
    templateUrl: './adm-ref-group-dialog.component.html'
})
export class AdmRefGroupDialogComponent implements OnInit {

    admRefGroup: AdmRefGroup;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private admRefGroupService: AdmRefGroupService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.admRefGroup.id !== undefined) {
            this.subscribeToSaveResponse(
                this.admRefGroupService.update(this.admRefGroup));
        } else {
            this.subscribeToSaveResponse(
                this.admRefGroupService.create(this.admRefGroup));
        }
    }

    private subscribeToSaveResponse(result: Observable<AdmRefGroup>) {
        result.subscribe((res: AdmRefGroup) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: AdmRefGroup) {
        this.eventManager.broadcast({ name: 'admRefGroupListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-adm-ref-group-popup',
    template: ''
})
export class AdmRefGroupPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private admRefGroupPopupService: AdmRefGroupPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.admRefGroupPopupService
                    .open(AdmRefGroupDialogComponent as Component, params['id']);
            } else {
                this.admRefGroupPopupService
                    .open(AdmRefGroupDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
