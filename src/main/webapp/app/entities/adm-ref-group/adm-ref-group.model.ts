import { BaseEntity } from './../../shared';

export class AdmRefGroup implements BaseEntity {
    constructor(
        public id?: number,
        public groupName?: string,
    ) {
    }
}
