import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BackpackdaddySharedModule } from '../../shared';
import {
    AdmRefGroupService,
    AdmRefGroupPopupService,
    AdmRefGroupComponent,
    AdmRefGroupDetailComponent,
    AdmRefGroupDialogComponent,
    AdmRefGroupPopupComponent,
    AdmRefGroupDeletePopupComponent,
    AdmRefGroupDeleteDialogComponent,
    admRefGroupRoute,
    admRefGroupPopupRoute,
} from './';

const ENTITY_STATES = [
    ...admRefGroupRoute,
    ...admRefGroupPopupRoute,
];

@NgModule({
    imports: [
        BackpackdaddySharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        AdmRefGroupComponent,
        AdmRefGroupDetailComponent,
        AdmRefGroupDialogComponent,
        AdmRefGroupDeleteDialogComponent,
        AdmRefGroupPopupComponent,
        AdmRefGroupDeletePopupComponent,
    ],
    entryComponents: [
        AdmRefGroupComponent,
        AdmRefGroupDialogComponent,
        AdmRefGroupPopupComponent,
        AdmRefGroupDeleteDialogComponent,
        AdmRefGroupDeletePopupComponent,
    ],
    providers: [
        AdmRefGroupService,
        AdmRefGroupPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BackpackdaddyAdmRefGroupModule {}
