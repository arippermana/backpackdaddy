import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { AdmRefGroupComponent } from './adm-ref-group.component';
import { AdmRefGroupDetailComponent } from './adm-ref-group-detail.component';
import { AdmRefGroupPopupComponent } from './adm-ref-group-dialog.component';
import { AdmRefGroupDeletePopupComponent } from './adm-ref-group-delete-dialog.component';

export const admRefGroupRoute: Routes = [
    {
        path: 'adm-ref-group',
        component: AdmRefGroupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backpackdaddyApp.admRefGroup.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'adm-ref-group/:id',
        component: AdmRefGroupDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backpackdaddyApp.admRefGroup.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const admRefGroupPopupRoute: Routes = [
    {
        path: 'adm-ref-group-new',
        component: AdmRefGroupPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backpackdaddyApp.admRefGroup.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'adm-ref-group/:id/edit',
        component: AdmRefGroupPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backpackdaddyApp.admRefGroup.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'adm-ref-group/:id/delete',
        component: AdmRefGroupDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backpackdaddyApp.admRefGroup.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
