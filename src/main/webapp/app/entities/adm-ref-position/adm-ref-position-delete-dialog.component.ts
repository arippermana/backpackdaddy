import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { AdmRefPosition } from './adm-ref-position.model';
import { AdmRefPositionPopupService } from './adm-ref-position-popup.service';
import { AdmRefPositionService } from './adm-ref-position.service';

@Component({
    selector: 'jhi-adm-ref-position-delete-dialog',
    templateUrl: './adm-ref-position-delete-dialog.component.html'
})
export class AdmRefPositionDeleteDialogComponent {

    admRefPosition: AdmRefPosition;

    constructor(
        private admRefPositionService: AdmRefPositionService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.admRefPositionService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'admRefPositionListModification',
                content: 'Deleted an admRefPosition'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-adm-ref-position-delete-popup',
    template: ''
})
export class AdmRefPositionDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private admRefPositionPopupService: AdmRefPositionPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.admRefPositionPopupService
                .open(AdmRefPositionDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
