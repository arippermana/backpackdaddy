import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { AdmRefPosition } from './adm-ref-position.model';
import { AdmRefPositionService } from './adm-ref-position.service';

@Component({
    selector: 'jhi-adm-ref-position-detail',
    templateUrl: './adm-ref-position-detail.component.html'
})
export class AdmRefPositionDetailComponent implements OnInit, OnDestroy {

    admRefPosition: AdmRefPosition;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private admRefPositionService: AdmRefPositionService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInAdmRefPositions();
    }

    load(id) {
        this.admRefPositionService.find(id).subscribe((admRefPosition) => {
            this.admRefPosition = admRefPosition;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInAdmRefPositions() {
        this.eventSubscriber = this.eventManager.subscribe(
            'admRefPositionListModification',
            (response) => this.load(this.admRefPosition.id)
        );
    }
}
