import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { AdmRefPosition } from './adm-ref-position.model';
import { AdmRefPositionPopupService } from './adm-ref-position-popup.service';
import { AdmRefPositionService } from './adm-ref-position.service';

@Component({
    selector: 'jhi-adm-ref-position-dialog',
    templateUrl: './adm-ref-position-dialog.component.html'
})
export class AdmRefPositionDialogComponent implements OnInit {

    admRefPosition: AdmRefPosition;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private admRefPositionService: AdmRefPositionService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.admRefPosition.id !== undefined) {
            this.subscribeToSaveResponse(
                this.admRefPositionService.update(this.admRefPosition));
        } else {
            this.subscribeToSaveResponse(
                this.admRefPositionService.create(this.admRefPosition));
        }
    }

    private subscribeToSaveResponse(result: Observable<AdmRefPosition>) {
        result.subscribe((res: AdmRefPosition) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: AdmRefPosition) {
        this.eventManager.broadcast({ name: 'admRefPositionListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-adm-ref-position-popup',
    template: ''
})
export class AdmRefPositionPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private admRefPositionPopupService: AdmRefPositionPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.admRefPositionPopupService
                    .open(AdmRefPositionDialogComponent as Component, params['id']);
            } else {
                this.admRefPositionPopupService
                    .open(AdmRefPositionDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
