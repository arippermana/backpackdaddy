import { BaseEntity } from './../../shared';

export class AdmRefPosition implements BaseEntity {
    constructor(
        public id?: number,
        public positionName?: string,
    ) {
    }
}
