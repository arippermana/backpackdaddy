import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BackpackdaddySharedModule } from '../../shared';
import {
    AdmRefPositionService,
    AdmRefPositionPopupService,
    AdmRefPositionComponent,
    AdmRefPositionDetailComponent,
    AdmRefPositionDialogComponent,
    AdmRefPositionPopupComponent,
    AdmRefPositionDeletePopupComponent,
    AdmRefPositionDeleteDialogComponent,
    admRefPositionRoute,
    admRefPositionPopupRoute,
} from './';

const ENTITY_STATES = [
    ...admRefPositionRoute,
    ...admRefPositionPopupRoute,
];

@NgModule({
    imports: [
        BackpackdaddySharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        AdmRefPositionComponent,
        AdmRefPositionDetailComponent,
        AdmRefPositionDialogComponent,
        AdmRefPositionDeleteDialogComponent,
        AdmRefPositionPopupComponent,
        AdmRefPositionDeletePopupComponent,
    ],
    entryComponents: [
        AdmRefPositionComponent,
        AdmRefPositionDialogComponent,
        AdmRefPositionPopupComponent,
        AdmRefPositionDeleteDialogComponent,
        AdmRefPositionDeletePopupComponent,
    ],
    providers: [
        AdmRefPositionService,
        AdmRefPositionPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BackpackdaddyAdmRefPositionModule {}
