import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { AdmRefPositionComponent } from './adm-ref-position.component';
import { AdmRefPositionDetailComponent } from './adm-ref-position-detail.component';
import { AdmRefPositionPopupComponent } from './adm-ref-position-dialog.component';
import { AdmRefPositionDeletePopupComponent } from './adm-ref-position-delete-dialog.component';

export const admRefPositionRoute: Routes = [
    {
        path: 'adm-ref-position',
        component: AdmRefPositionComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backpackdaddyApp.admRefPosition.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'adm-ref-position/:id',
        component: AdmRefPositionDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backpackdaddyApp.admRefPosition.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const admRefPositionPopupRoute: Routes = [
    {
        path: 'adm-ref-position-new',
        component: AdmRefPositionPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backpackdaddyApp.admRefPosition.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'adm-ref-position/:id/edit',
        component: AdmRefPositionPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backpackdaddyApp.admRefPosition.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'adm-ref-position/:id/delete',
        component: AdmRefPositionDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backpackdaddyApp.admRefPosition.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
