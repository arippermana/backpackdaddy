import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { AdmStaff } from './adm-staff.model';
import { AdmStaffPopupService } from './adm-staff-popup.service';
import { AdmStaffService } from './adm-staff.service';

@Component({
    selector: 'jhi-adm-staff-delete-dialog',
    templateUrl: './adm-staff-delete-dialog.component.html'
})
export class AdmStaffDeleteDialogComponent {

    admStaff: AdmStaff;

    constructor(
        private admStaffService: AdmStaffService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.admStaffService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'admStaffListModification',
                content: 'Deleted an admStaff'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-adm-staff-delete-popup',
    template: ''
})
export class AdmStaffDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private admStaffPopupService: AdmStaffPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.admStaffPopupService
                .open(AdmStaffDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
