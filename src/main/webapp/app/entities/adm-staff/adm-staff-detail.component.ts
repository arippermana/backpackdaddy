import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { AdmStaff } from './adm-staff.model';
import { AdmStaffService } from './adm-staff.service';

@Component({
    selector: 'jhi-adm-staff-detail',
    templateUrl: './adm-staff-detail.component.html'
})
export class AdmStaffDetailComponent implements OnInit, OnDestroy {

    admStaff: AdmStaff;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private admStaffService: AdmStaffService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInAdmStaffs();
    }

    load(id) {
        this.admStaffService.find(id).subscribe((admStaff) => {
            this.admStaff = admStaff;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInAdmStaffs() {
        this.eventSubscriber = this.eventManager.subscribe(
            'admStaffListModification',
            (response) => this.load(this.admStaff.id)
        );
    }
}
