import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { AdmStaff } from './adm-staff.model';
import { AdmStaffPopupService } from './adm-staff-popup.service';
import { AdmStaffService } from './adm-staff.service';
import { AdmRefBranch, AdmRefBranchService } from '../adm-ref-branch';
import { AdmRefDepartment, AdmRefDepartmentService } from '../adm-ref-department';
import { AdmRefGroup, AdmRefGroupService } from '../adm-ref-group';
import { AdmRefPosition, AdmRefPositionService } from '../adm-ref-position';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-adm-staff-dialog',
    templateUrl: './adm-staff-dialog.component.html'
})
export class AdmStaffDialogComponent implements OnInit {

    admStaff: AdmStaff;
    isSaving: boolean;

    admrefbranches: AdmRefBranch[];

    admrefdepartments: AdmRefDepartment[];

    admrefgroups: AdmRefGroup[];

    admrefpositions: AdmRefPosition[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private admStaffService: AdmStaffService,
        private admRefBranchService: AdmRefBranchService,
        private admRefDepartmentService: AdmRefDepartmentService,
        private admRefGroupService: AdmRefGroupService,
        private admRefPositionService: AdmRefPositionService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.admRefBranchService.query()
            .subscribe((res: ResponseWrapper) => { this.admrefbranches = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.admRefDepartmentService.query()
            .subscribe((res: ResponseWrapper) => { this.admrefdepartments = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.admRefGroupService.query()
            .subscribe((res: ResponseWrapper) => { this.admrefgroups = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.admRefPositionService.query()
            .subscribe((res: ResponseWrapper) => { this.admrefpositions = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.admStaff.id !== undefined) {
            this.subscribeToSaveResponse(
                this.admStaffService.update(this.admStaff));
        } else {
            this.subscribeToSaveResponse(
                this.admStaffService.create(this.admStaff));
        }
    }

    private subscribeToSaveResponse(result: Observable<AdmStaff>) {
        result.subscribe((res: AdmStaff) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: AdmStaff) {
        this.eventManager.broadcast({ name: 'admStaffListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackAdmRefBranchById(index: number, item: AdmRefBranch) {
        return item.id;
    }

    trackAdmRefDepartmentById(index: number, item: AdmRefDepartment) {
        return item.id;
    }

    trackAdmRefGroupById(index: number, item: AdmRefGroup) {
        return item.id;
    }

    trackAdmRefPositionById(index: number, item: AdmRefPosition) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-adm-staff-popup',
    template: ''
})
export class AdmStaffPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private admStaffPopupService: AdmStaffPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.admStaffPopupService
                    .open(AdmStaffDialogComponent as Component, params['id']);
            } else {
                this.admStaffPopupService
                    .open(AdmStaffDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
