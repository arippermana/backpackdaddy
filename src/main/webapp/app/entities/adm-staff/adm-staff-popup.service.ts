import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { AdmStaff } from './adm-staff.model';
import { AdmStaffService } from './adm-staff.service';

@Injectable()
export class AdmStaffPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private admStaffService: AdmStaffService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.admStaffService.find(id).subscribe((admStaff) => {
                    admStaff.hireDate = this.datePipe
                        .transform(admStaff.hireDate, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.admStaffModalRef(component, admStaff);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.admStaffModalRef(component, new AdmStaff());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    admStaffModalRef(component: Component, admStaff: AdmStaff): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.admStaff = admStaff;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
