import { BaseEntity } from './../../shared';

export class AdmStaff implements BaseEntity {
    constructor(
        public id?: number,
        public email?: string,
        public staffName?: string,
        public password?: string,
        public hireDate?: any,
        public isActivated?: boolean,
        public isActive?: boolean,
        public branchId?: number,
        public departmentId?: number,
        public groupId?: number,
        public positionId?: number,
    ) {
        this.isActivated = false;
        this.isActive = false;
    }
}
