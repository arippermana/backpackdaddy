import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BackpackdaddySharedModule } from '../../shared';
import {
    AdmStaffService,
    AdmStaffPopupService,
    AdmStaffComponent,
    AdmStaffDetailComponent,
    AdmStaffDialogComponent,
    AdmStaffPopupComponent,
    AdmStaffDeletePopupComponent,
    AdmStaffDeleteDialogComponent,
    admStaffRoute,
    admStaffPopupRoute,
} from './';

const ENTITY_STATES = [
    ...admStaffRoute,
    ...admStaffPopupRoute,
];

@NgModule({
    imports: [
        BackpackdaddySharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        AdmStaffComponent,
        AdmStaffDetailComponent,
        AdmStaffDialogComponent,
        AdmStaffDeleteDialogComponent,
        AdmStaffPopupComponent,
        AdmStaffDeletePopupComponent,
    ],
    entryComponents: [
        AdmStaffComponent,
        AdmStaffDialogComponent,
        AdmStaffPopupComponent,
        AdmStaffDeleteDialogComponent,
        AdmStaffDeletePopupComponent,
    ],
    providers: [
        AdmStaffService,
        AdmStaffPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BackpackdaddyAdmStaffModule {}
