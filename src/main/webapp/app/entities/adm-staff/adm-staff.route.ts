import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { AdmStaffComponent } from './adm-staff.component';
import { AdmStaffDetailComponent } from './adm-staff-detail.component';
import { AdmStaffPopupComponent } from './adm-staff-dialog.component';
import { AdmStaffDeletePopupComponent } from './adm-staff-delete-dialog.component';

export const admStaffRoute: Routes = [
    {
        path: 'adm-staff',
        component: AdmStaffComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backpackdaddyApp.admStaff.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'adm-staff/:id',
        component: AdmStaffDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backpackdaddyApp.admStaff.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const admStaffPopupRoute: Routes = [
    {
        path: 'adm-staff-new',
        component: AdmStaffPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backpackdaddyApp.admStaff.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'adm-staff/:id/edit',
        component: AdmStaffPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backpackdaddyApp.admStaff.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'adm-staff/:id/delete',
        component: AdmStaffDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backpackdaddyApp.admStaff.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
