import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { AttachmentDetails } from './attachment-details.model';
import { AttachmentDetailsPopupService } from './attachment-details-popup.service';
import { AttachmentDetailsService } from './attachment-details.service';

@Component({
    selector: 'jhi-attachment-details-delete-dialog',
    templateUrl: './attachment-details-delete-dialog.component.html'
})
export class AttachmentDetailsDeleteDialogComponent {

    attachmentDetails: AttachmentDetails;

    constructor(
        private attachmentDetailsService: AttachmentDetailsService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.attachmentDetailsService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'attachmentDetailsListModification',
                content: 'Deleted an attachmentDetails'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-attachment-details-delete-popup',
    template: ''
})
export class AttachmentDetailsDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private attachmentDetailsPopupService: AttachmentDetailsPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.attachmentDetailsPopupService
                .open(AttachmentDetailsDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
