import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { AttachmentDetails } from './attachment-details.model';
import { AttachmentDetailsService } from './attachment-details.service';

@Component({
    selector: 'jhi-attachment-details-detail',
    templateUrl: './attachment-details-detail.component.html'
})
export class AttachmentDetailsDetailComponent implements OnInit, OnDestroy {

    attachmentDetails: AttachmentDetails;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private attachmentDetailsService: AttachmentDetailsService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInAttachmentDetails();
    }

    load(id) {
        this.attachmentDetailsService.find(id).subscribe((attachmentDetails) => {
            this.attachmentDetails = attachmentDetails;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInAttachmentDetails() {
        this.eventSubscriber = this.eventManager.subscribe(
            'attachmentDetailsListModification',
            (response) => this.load(this.attachmentDetails.id)
        );
    }
}
