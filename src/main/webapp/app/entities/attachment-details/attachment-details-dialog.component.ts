import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { AttachmentDetails } from './attachment-details.model';
import { AttachmentDetailsPopupService } from './attachment-details-popup.service';
import { AttachmentDetailsService } from './attachment-details.service';
import { Track, TrackService } from '../track';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-attachment-details-dialog',
    templateUrl: './attachment-details-dialog.component.html'
})
export class AttachmentDetailsDialogComponent implements OnInit {

    attachmentDetails: AttachmentDetails;
    isSaving: boolean;

    tracks: Track[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private attachmentDetailsService: AttachmentDetailsService,
        private trackService: TrackService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.trackService.query()
            .subscribe((res: ResponseWrapper) => { this.tracks = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.attachmentDetails.id !== undefined) {
            this.subscribeToSaveResponse(
                this.attachmentDetailsService.update(this.attachmentDetails));
        } else {
            this.subscribeToSaveResponse(
                this.attachmentDetailsService.create(this.attachmentDetails));
        }
    }

    private subscribeToSaveResponse(result: Observable<AttachmentDetails>) {
        result.subscribe((res: AttachmentDetails) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: AttachmentDetails) {
        this.eventManager.broadcast({ name: 'attachmentDetailsListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackTrackById(index: number, item: Track) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-attachment-details-popup',
    template: ''
})
export class AttachmentDetailsPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private attachmentDetailsPopupService: AttachmentDetailsPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.attachmentDetailsPopupService
                    .open(AttachmentDetailsDialogComponent as Component, params['id']);
            } else {
                this.attachmentDetailsPopupService
                    .open(AttachmentDetailsDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
