import { BaseEntity } from './../../shared';

export class AttachmentDetails implements BaseEntity {
    constructor(
        public id?: number,
        public attachmentDescription?: string,
        public attachmentPath?: string,
        public trackId?: number,
    ) {
    }
}
