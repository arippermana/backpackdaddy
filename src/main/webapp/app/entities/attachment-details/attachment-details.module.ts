import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BackpackdaddySharedModule } from '../../shared';
import {
    AttachmentDetailsService,
    AttachmentDetailsPopupService,
    AttachmentDetailsComponent,
    AttachmentDetailsDetailComponent,
    AttachmentDetailsDialogComponent,
    AttachmentDetailsPopupComponent,
    AttachmentDetailsDeletePopupComponent,
    AttachmentDetailsDeleteDialogComponent,
    attachmentDetailsRoute,
    attachmentDetailsPopupRoute,
} from './';

const ENTITY_STATES = [
    ...attachmentDetailsRoute,
    ...attachmentDetailsPopupRoute,
];

@NgModule({
    imports: [
        BackpackdaddySharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        AttachmentDetailsComponent,
        AttachmentDetailsDetailComponent,
        AttachmentDetailsDialogComponent,
        AttachmentDetailsDeleteDialogComponent,
        AttachmentDetailsPopupComponent,
        AttachmentDetailsDeletePopupComponent,
    ],
    entryComponents: [
        AttachmentDetailsComponent,
        AttachmentDetailsDialogComponent,
        AttachmentDetailsPopupComponent,
        AttachmentDetailsDeleteDialogComponent,
        AttachmentDetailsDeletePopupComponent,
    ],
    providers: [
        AttachmentDetailsService,
        AttachmentDetailsPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BackpackdaddyAttachmentDetailsModule {}
