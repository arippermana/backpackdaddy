import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { AttachmentDetailsComponent } from './attachment-details.component';
import { AttachmentDetailsDetailComponent } from './attachment-details-detail.component';
import { AttachmentDetailsPopupComponent } from './attachment-details-dialog.component';
import { AttachmentDetailsDeletePopupComponent } from './attachment-details-delete-dialog.component';

export const attachmentDetailsRoute: Routes = [
    {
        path: 'attachment-details',
        component: AttachmentDetailsComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backpackdaddyApp.attachmentDetails.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'attachment-details/:id',
        component: AttachmentDetailsDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backpackdaddyApp.attachmentDetails.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const attachmentDetailsPopupRoute: Routes = [
    {
        path: 'attachment-details-new',
        component: AttachmentDetailsPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backpackdaddyApp.attachmentDetails.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'attachment-details/:id/edit',
        component: AttachmentDetailsPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backpackdaddyApp.attachmentDetails.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'attachment-details/:id/delete',
        component: AttachmentDetailsDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backpackdaddyApp.attachmentDetails.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
