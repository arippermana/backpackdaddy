export * from './attachment-details.model';
export * from './attachment-details-popup.service';
export * from './attachment-details.service';
export * from './attachment-details-dialog.component';
export * from './attachment-details-delete-dialog.component';
export * from './attachment-details-detail.component';
export * from './attachment-details.component';
export * from './attachment-details.route';
