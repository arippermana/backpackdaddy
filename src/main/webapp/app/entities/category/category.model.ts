import { BaseEntity } from './../../shared';

export class Category implements BaseEntity {
    constructor(
        public id?: number,
        public categoryDescription?: string,
        public level?: number,
        public parentId?: number,
        public order?: number,
    ) {
    }
}
