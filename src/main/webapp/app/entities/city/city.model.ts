import { BaseEntity } from './../../shared';

export class City implements BaseEntity {
    constructor(
        public id?: number,
        public latitude?: string,
        public longitude?: string,
        public name?: string,
        public regionId?: number,
    ) {
    }
}
