import { BaseEntity } from './../../shared';

export class Country implements BaseEntity {
    constructor(
        public id?: number,
        public code?: string,
        public name?: string,
        public imageFlagPath?: string,
        public regions?: BaseEntity[],
    ) {
    }
}
