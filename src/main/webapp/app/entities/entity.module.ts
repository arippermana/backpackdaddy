import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { BackpackdaddyTransactionModule } from './transaction/transaction.module';
import { BackpackdaddyTrackModule } from './track/track.module';
import { BackpackdaddyAttachmentDetailsModule } from './attachment-details/attachment-details.module';
import { BackpackdaddyTransactionStatusModule } from './transaction-status/transaction-status.module';
import { BackpackdaddyTripModule } from './trip/trip.module';
import { BackpackdaddyRequestModule } from './request/request.module';
import { BackpackdaddyProductModule } from './product/product.module';
import { BackpackdaddyStoreModule } from './store/store.module';
import { BackpackdaddyCountryModule } from './country/country.module';
import { BackpackdaddyRegionModule } from './region/region.module';
import { BackpackdaddyCityModule } from './city/city.module';
import { BackpackdaddyCurrencyModule } from './currency/currency.module';
import { BackpackdaddyCategoryModule } from './category/category.module';
import { BackpackdaddyGenderModule } from './gender/gender.module';
import { BackpackdaddyLanguageModule } from './language/language.module';
import { BackpackdaddyOccupationModule } from './occupation/occupation.module';
import { BackpackdaddyUserProfileModule } from './user-profile/user-profile.module';
import { BackpackdaddyAdmAuditTrailModule } from './adm-audit-trail/adm-audit-trail.module';
import { BackpackdaddyAdmRefBranchModule } from './adm-ref-branch/adm-ref-branch.module';
import { BackpackdaddyAdmRefDepartmentModule } from './adm-ref-department/adm-ref-department.module';
import { BackpackdaddyAdmRefGroupModule } from './adm-ref-group/adm-ref-group.module';
import { BackpackdaddyAdmRefPositionModule } from './adm-ref-position/adm-ref-position.module';
import { BackpackdaddyAdmStaffModule } from './adm-staff/adm-staff.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        BackpackdaddyTransactionModule,
        BackpackdaddyTrackModule,
        BackpackdaddyAttachmentDetailsModule,
        BackpackdaddyTransactionStatusModule,
        BackpackdaddyTripModule,
        BackpackdaddyRequestModule,
        BackpackdaddyProductModule,
        BackpackdaddyStoreModule,
        BackpackdaddyCountryModule,
        BackpackdaddyRegionModule,
        BackpackdaddyCityModule,
        BackpackdaddyCurrencyModule,
        BackpackdaddyCategoryModule,
        BackpackdaddyGenderModule,
        BackpackdaddyLanguageModule,
        BackpackdaddyOccupationModule,
        BackpackdaddyUserProfileModule,
        BackpackdaddyAdmAuditTrailModule,
        BackpackdaddyAdmRefBranchModule,
        BackpackdaddyAdmRefDepartmentModule,
        BackpackdaddyAdmRefGroupModule,
        BackpackdaddyAdmRefPositionModule,
        BackpackdaddyAdmStaffModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BackpackdaddyEntityModule {}
