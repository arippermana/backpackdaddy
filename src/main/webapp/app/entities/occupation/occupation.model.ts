import { BaseEntity } from './../../shared';

export class Occupation implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
    ) {
    }
}
