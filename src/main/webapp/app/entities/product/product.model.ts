import { BaseEntity } from './../../shared';

export class Product implements BaseEntity {
    constructor(
        public id?: number,
        public productName?: string,
        public image1?: string,
        public image2?: string,
        public image3?: string,
        public image4?: string,
        public image5?: string,
        public specification?: string,
        public stores?: BaseEntity[],
        public categoryId?: number,
        public madeInId?: number,
    ) {
    }
}
