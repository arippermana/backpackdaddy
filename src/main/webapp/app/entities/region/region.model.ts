import { BaseEntity } from './../../shared';

export class Region implements BaseEntity {
    constructor(
        public id?: number,
        public code?: string,
        public name?: string,
        public cities?: BaseEntity[],
        public countryId?: number,
    ) {
    }
}
