import { BaseEntity } from './../../shared';

export const enum ShippingMethod {
    'DIRECT_TRAVELLER',
    'COMPANY_SERVICE'
}

export const enum PreferencePayment {
    'NEGOTIABLE',
    'RANGE',
    'FIXED',
    'PERCENTAGE'
}

export class Request implements BaseEntity {
    constructor(
        public id?: number,
        public shippingMethod?: ShippingMethod,
        public requestDate?: any,
        public name?: string,
        public address?: string,
        public productPrice?: number,
        public productQuantity?: number,
        public preferencePayment?: PreferencePayment,
        public tipAmount?: number,
        public commisionFee?: number,
        public baggageCharge?: number,
        public shippingCharge?: number,
        public remarks?: string,
        public isDraft?: string,
        public productId?: number,
        public userId?: number,
        public cityId?: number,
        public tipCurrencyId?: number,
    ) {
    }
}
