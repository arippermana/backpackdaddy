import { BaseEntity } from './../../shared';

export class Store implements BaseEntity {
    constructor(
        public id?: number,
        public offlineShop?: string,
        public onlineShop?: string,
        public price?: number,
        public countryId?: number,
        public currencyId?: number,
        public productId?: number,
    ) {
    }
}
