import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { TransactionStatus } from './transaction-status.model';
import { TransactionStatusService } from './transaction-status.service';

@Component({
    selector: 'jhi-transaction-status-detail',
    templateUrl: './transaction-status-detail.component.html'
})
export class TransactionStatusDetailComponent implements OnInit, OnDestroy {

    transactionStatus: TransactionStatus;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private transactionStatusService: TransactionStatusService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInTransactionStatuses();
    }

    load(id) {
        this.transactionStatusService.find(id).subscribe((transactionStatus) => {
            this.transactionStatus = transactionStatus;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTransactionStatuses() {
        this.eventSubscriber = this.eventManager.subscribe(
            'transactionStatusListModification',
            (response) => this.load(this.transactionStatus.id)
        );
    }
}
