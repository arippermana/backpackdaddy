import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { TransactionStatus } from './transaction-status.model';
import { TransactionStatusPopupService } from './transaction-status-popup.service';
import { TransactionStatusService } from './transaction-status.service';

@Component({
    selector: 'jhi-transaction-status-dialog',
    templateUrl: './transaction-status-dialog.component.html'
})
export class TransactionStatusDialogComponent implements OnInit {

    transactionStatus: TransactionStatus;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private transactionStatusService: TransactionStatusService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.transactionStatus.id !== undefined) {
            this.subscribeToSaveResponse(
                this.transactionStatusService.update(this.transactionStatus));
        } else {
            this.subscribeToSaveResponse(
                this.transactionStatusService.create(this.transactionStatus));
        }
    }

    private subscribeToSaveResponse(result: Observable<TransactionStatus>) {
        result.subscribe((res: TransactionStatus) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: TransactionStatus) {
        this.eventManager.broadcast({ name: 'transactionStatusListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-transaction-status-popup',
    template: ''
})
export class TransactionStatusPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private transactionStatusPopupService: TransactionStatusPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.transactionStatusPopupService
                    .open(TransactionStatusDialogComponent as Component, params['id']);
            } else {
                this.transactionStatusPopupService
                    .open(TransactionStatusDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
