import { BaseEntity } from './../../shared';

export class TransactionStatus implements BaseEntity {
    constructor(
        public id?: number,
        public description?: string,
        public flagId?: string,
    ) {
    }
}
