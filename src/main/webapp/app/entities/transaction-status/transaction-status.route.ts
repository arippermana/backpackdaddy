import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { TransactionStatusComponent } from './transaction-status.component';
import { TransactionStatusDetailComponent } from './transaction-status-detail.component';
import { TransactionStatusPopupComponent } from './transaction-status-dialog.component';
import { TransactionStatusDeletePopupComponent } from './transaction-status-delete-dialog.component';

export const transactionStatusRoute: Routes = [
    {
        path: 'transaction-status',
        component: TransactionStatusComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backpackdaddyApp.transactionStatus.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'transaction-status/:id',
        component: TransactionStatusDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backpackdaddyApp.transactionStatus.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const transactionStatusPopupRoute: Routes = [
    {
        path: 'transaction-status-new',
        component: TransactionStatusPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backpackdaddyApp.transactionStatus.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'transaction-status/:id/edit',
        component: TransactionStatusPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backpackdaddyApp.transactionStatus.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'transaction-status/:id/delete',
        component: TransactionStatusDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backpackdaddyApp.transactionStatus.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
