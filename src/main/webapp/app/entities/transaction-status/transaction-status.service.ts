import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { TransactionStatus } from './transaction-status.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class TransactionStatusService {

    private resourceUrl = SERVER_API_URL + 'api/transaction-statuses';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/transaction-statuses';

    constructor(private http: Http) { }

    create(transactionStatus: TransactionStatus): Observable<TransactionStatus> {
        const copy = this.convert(transactionStatus);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(transactionStatus: TransactionStatus): Observable<TransactionStatus> {
        const copy = this.convert(transactionStatus);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<TransactionStatus> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to TransactionStatus.
     */
    private convertItemFromServer(json: any): TransactionStatus {
        const entity: TransactionStatus = Object.assign(new TransactionStatus(), json);
        return entity;
    }

    /**
     * Convert a TransactionStatus to a JSON which can be sent to the server.
     */
    private convert(transactionStatus: TransactionStatus): TransactionStatus {
        const copy: TransactionStatus = Object.assign({}, transactionStatus);
        return copy;
    }
}
