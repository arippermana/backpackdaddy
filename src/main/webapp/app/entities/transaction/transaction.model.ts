import { BaseEntity } from './../../shared';

export class Transaction implements BaseEntity {
    constructor(
        public id?: number,
        public trackId?: number,
        public requestId?: number,
        public tripId?: number,
    ) {
    }
}
