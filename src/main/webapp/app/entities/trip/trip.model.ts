import { BaseEntity } from './../../shared';

export const enum PreferencePayment {
    'NEGOTIABLE',
    'RANGE',
    'FIXED',
    'PERCENTAGE'
}

export class Trip implements BaseEntity {
    constructor(
        public id?: number,
        public weight?: number,
        public notice?: string,
        public preferencePayment?: PreferencePayment,
        public rangeFrom?: number,
        public rangeTo?: number,
        public fixedAmount?: number,
        public percentageAmount?: number,
        public baggageInformation?: string,
        public shipDate?: any,
        public remarks?: string,
        public isDraft?: string,
        public userId?: number,
        public fromCountryId?: number,
        public toCountryId?: number,
        public categoryId?: number,
        public paymentCurrencyId?: number,
        public shippingFromId?: number,
    ) {
    }
}
