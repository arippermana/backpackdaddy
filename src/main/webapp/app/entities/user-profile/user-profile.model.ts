import { BaseEntity } from './../../shared';

export class UserProfile implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public birthdate?: any,
        public isAgeShow?: boolean,
        public isWorkingOversea?: boolean,
        public profileImage?: string,
        public genderId?: number,
        public nationalityId?: number,
        public currentLocationId?: number,
        public occupationId?: number,
        public languageId?: number,
        public currencyId?: number,
        public userId?: number,
    ) {
        this.isAgeShow = false;
        this.isWorkingOversea = false;
    }
}
