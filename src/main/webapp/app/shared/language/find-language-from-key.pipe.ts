import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'findLanguageFromKey'})
export class FindLanguageFromKeyPipe implements PipeTransform {
    private languages: any = {
        'zh-cn': { name: '中文（简体）' },
        'zh-tw': { name: '繁體中文' },
        'en': { name: 'English' },
        'hi': { name: 'हिंदी' },
        'id': { name: 'Bahasa Indonesia' },
        'ta': { name: 'தமிழ்' },
        'th': { name: 'ไทย' },
        'vi': { name: 'Tiếng Việt' }
        // jhipster-needle-i18n-language-key-pipe - JHipster will add/remove languages in this object
    };
    transform(lang: string): string {
        return this.languages[lang].name;
    }
}
