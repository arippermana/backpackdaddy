package com.app.backpackdaddy.web.rest;

import com.app.backpackdaddy.BackpackdaddyApp;

import com.app.backpackdaddy.domain.AdmAuditTrail;
import com.app.backpackdaddy.repository.AdmAuditTrailRepository;
import com.app.backpackdaddy.repository.search.AdmAuditTrailSearchRepository;
import com.app.backpackdaddy.service.dto.AdmAuditTrailDTO;
import com.app.backpackdaddy.service.mapper.AdmAuditTrailMapper;
import com.app.backpackdaddy.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.app.backpackdaddy.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AdmAuditTrailResource REST controller.
 *
 * @see AdmAuditTrailResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BackpackdaddyApp.class)
public class AdmAuditTrailResourceIntTest {

    private static final String DEFAULT_TABLE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_TABLE_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_AUDIT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_AUDIT_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_OLD_VALUES = "AAAAAAAAAA";
    private static final String UPDATED_OLD_VALUES = "BBBBBBBBBB";

    private static final String DEFAULT_NEW_VALUES = "AAAAAAAAAA";
    private static final String UPDATED_NEW_VALUES = "BBBBBBBBBB";

    @Autowired
    private AdmAuditTrailRepository admAuditTrailRepository;

    @Autowired
    private AdmAuditTrailMapper admAuditTrailMapper;

    @Autowired
    private AdmAuditTrailSearchRepository admAuditTrailSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAdmAuditTrailMockMvc;

    private AdmAuditTrail admAuditTrail;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AdmAuditTrailResource admAuditTrailResource = new AdmAuditTrailResource(admAuditTrailRepository, admAuditTrailMapper, admAuditTrailSearchRepository);
        this.restAdmAuditTrailMockMvc = MockMvcBuilders.standaloneSetup(admAuditTrailResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AdmAuditTrail createEntity(EntityManager em) {
        AdmAuditTrail admAuditTrail = new AdmAuditTrail()
            .tableName(DEFAULT_TABLE_NAME)
            .auditStatus(DEFAULT_AUDIT_STATUS)
            .oldValues(DEFAULT_OLD_VALUES)
            .newValues(DEFAULT_NEW_VALUES);
        return admAuditTrail;
    }

    @Before
    public void initTest() {
        admAuditTrailSearchRepository.deleteAll();
        admAuditTrail = createEntity(em);
    }

    @Test
    @Transactional
    public void createAdmAuditTrail() throws Exception {
        int databaseSizeBeforeCreate = admAuditTrailRepository.findAll().size();

        // Create the AdmAuditTrail
        AdmAuditTrailDTO admAuditTrailDTO = admAuditTrailMapper.toDto(admAuditTrail);
        restAdmAuditTrailMockMvc.perform(post("/api/adm-audit-trails")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(admAuditTrailDTO)))
            .andExpect(status().isCreated());

        // Validate the AdmAuditTrail in the database
        List<AdmAuditTrail> admAuditTrailList = admAuditTrailRepository.findAll();
        assertThat(admAuditTrailList).hasSize(databaseSizeBeforeCreate + 1);
        AdmAuditTrail testAdmAuditTrail = admAuditTrailList.get(admAuditTrailList.size() - 1);
        assertThat(testAdmAuditTrail.getTableName()).isEqualTo(DEFAULT_TABLE_NAME);
        assertThat(testAdmAuditTrail.getAuditStatus()).isEqualTo(DEFAULT_AUDIT_STATUS);
        assertThat(testAdmAuditTrail.getOldValues()).isEqualTo(DEFAULT_OLD_VALUES);
        assertThat(testAdmAuditTrail.getNewValues()).isEqualTo(DEFAULT_NEW_VALUES);

        // Validate the AdmAuditTrail in Elasticsearch
        AdmAuditTrail admAuditTrailEs = admAuditTrailSearchRepository.findOne(testAdmAuditTrail.getId());
        assertThat(admAuditTrailEs).isEqualToComparingFieldByField(testAdmAuditTrail);
    }

    @Test
    @Transactional
    public void createAdmAuditTrailWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = admAuditTrailRepository.findAll().size();

        // Create the AdmAuditTrail with an existing ID
        admAuditTrail.setId(1L);
        AdmAuditTrailDTO admAuditTrailDTO = admAuditTrailMapper.toDto(admAuditTrail);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAdmAuditTrailMockMvc.perform(post("/api/adm-audit-trails")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(admAuditTrailDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AdmAuditTrail in the database
        List<AdmAuditTrail> admAuditTrailList = admAuditTrailRepository.findAll();
        assertThat(admAuditTrailList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkTableNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = admAuditTrailRepository.findAll().size();
        // set the field null
        admAuditTrail.setTableName(null);

        // Create the AdmAuditTrail, which fails.
        AdmAuditTrailDTO admAuditTrailDTO = admAuditTrailMapper.toDto(admAuditTrail);

        restAdmAuditTrailMockMvc.perform(post("/api/adm-audit-trails")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(admAuditTrailDTO)))
            .andExpect(status().isBadRequest());

        List<AdmAuditTrail> admAuditTrailList = admAuditTrailRepository.findAll();
        assertThat(admAuditTrailList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAuditStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = admAuditTrailRepository.findAll().size();
        // set the field null
        admAuditTrail.setAuditStatus(null);

        // Create the AdmAuditTrail, which fails.
        AdmAuditTrailDTO admAuditTrailDTO = admAuditTrailMapper.toDto(admAuditTrail);

        restAdmAuditTrailMockMvc.perform(post("/api/adm-audit-trails")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(admAuditTrailDTO)))
            .andExpect(status().isBadRequest());

        List<AdmAuditTrail> admAuditTrailList = admAuditTrailRepository.findAll();
        assertThat(admAuditTrailList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkOldValuesIsRequired() throws Exception {
        int databaseSizeBeforeTest = admAuditTrailRepository.findAll().size();
        // set the field null
        admAuditTrail.setOldValues(null);

        // Create the AdmAuditTrail, which fails.
        AdmAuditTrailDTO admAuditTrailDTO = admAuditTrailMapper.toDto(admAuditTrail);

        restAdmAuditTrailMockMvc.perform(post("/api/adm-audit-trails")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(admAuditTrailDTO)))
            .andExpect(status().isBadRequest());

        List<AdmAuditTrail> admAuditTrailList = admAuditTrailRepository.findAll();
        assertThat(admAuditTrailList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNewValuesIsRequired() throws Exception {
        int databaseSizeBeforeTest = admAuditTrailRepository.findAll().size();
        // set the field null
        admAuditTrail.setNewValues(null);

        // Create the AdmAuditTrail, which fails.
        AdmAuditTrailDTO admAuditTrailDTO = admAuditTrailMapper.toDto(admAuditTrail);

        restAdmAuditTrailMockMvc.perform(post("/api/adm-audit-trails")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(admAuditTrailDTO)))
            .andExpect(status().isBadRequest());

        List<AdmAuditTrail> admAuditTrailList = admAuditTrailRepository.findAll();
        assertThat(admAuditTrailList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAdmAuditTrails() throws Exception {
        // Initialize the database
        admAuditTrailRepository.saveAndFlush(admAuditTrail);

        // Get all the admAuditTrailList
        restAdmAuditTrailMockMvc.perform(get("/api/adm-audit-trails?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(admAuditTrail.getId().intValue())))
            .andExpect(jsonPath("$.[*].tableName").value(hasItem(DEFAULT_TABLE_NAME.toString())))
            .andExpect(jsonPath("$.[*].auditStatus").value(hasItem(DEFAULT_AUDIT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].oldValues").value(hasItem(DEFAULT_OLD_VALUES.toString())))
            .andExpect(jsonPath("$.[*].newValues").value(hasItem(DEFAULT_NEW_VALUES.toString())));
    }

    @Test
    @Transactional
    public void getAdmAuditTrail() throws Exception {
        // Initialize the database
        admAuditTrailRepository.saveAndFlush(admAuditTrail);

        // Get the admAuditTrail
        restAdmAuditTrailMockMvc.perform(get("/api/adm-audit-trails/{id}", admAuditTrail.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(admAuditTrail.getId().intValue()))
            .andExpect(jsonPath("$.tableName").value(DEFAULT_TABLE_NAME.toString()))
            .andExpect(jsonPath("$.auditStatus").value(DEFAULT_AUDIT_STATUS.toString()))
            .andExpect(jsonPath("$.oldValues").value(DEFAULT_OLD_VALUES.toString()))
            .andExpect(jsonPath("$.newValues").value(DEFAULT_NEW_VALUES.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAdmAuditTrail() throws Exception {
        // Get the admAuditTrail
        restAdmAuditTrailMockMvc.perform(get("/api/adm-audit-trails/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAdmAuditTrail() throws Exception {
        // Initialize the database
        admAuditTrailRepository.saveAndFlush(admAuditTrail);
        admAuditTrailSearchRepository.save(admAuditTrail);
        int databaseSizeBeforeUpdate = admAuditTrailRepository.findAll().size();

        // Update the admAuditTrail
        AdmAuditTrail updatedAdmAuditTrail = admAuditTrailRepository.findOne(admAuditTrail.getId());
        updatedAdmAuditTrail
            .tableName(UPDATED_TABLE_NAME)
            .auditStatus(UPDATED_AUDIT_STATUS)
            .oldValues(UPDATED_OLD_VALUES)
            .newValues(UPDATED_NEW_VALUES);
        AdmAuditTrailDTO admAuditTrailDTO = admAuditTrailMapper.toDto(updatedAdmAuditTrail);

        restAdmAuditTrailMockMvc.perform(put("/api/adm-audit-trails")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(admAuditTrailDTO)))
            .andExpect(status().isOk());

        // Validate the AdmAuditTrail in the database
        List<AdmAuditTrail> admAuditTrailList = admAuditTrailRepository.findAll();
        assertThat(admAuditTrailList).hasSize(databaseSizeBeforeUpdate);
        AdmAuditTrail testAdmAuditTrail = admAuditTrailList.get(admAuditTrailList.size() - 1);
        assertThat(testAdmAuditTrail.getTableName()).isEqualTo(UPDATED_TABLE_NAME);
        assertThat(testAdmAuditTrail.getAuditStatus()).isEqualTo(UPDATED_AUDIT_STATUS);
        assertThat(testAdmAuditTrail.getOldValues()).isEqualTo(UPDATED_OLD_VALUES);
        assertThat(testAdmAuditTrail.getNewValues()).isEqualTo(UPDATED_NEW_VALUES);

        // Validate the AdmAuditTrail in Elasticsearch
        AdmAuditTrail admAuditTrailEs = admAuditTrailSearchRepository.findOne(testAdmAuditTrail.getId());
        assertThat(admAuditTrailEs).isEqualToComparingFieldByField(testAdmAuditTrail);
    }

    @Test
    @Transactional
    public void updateNonExistingAdmAuditTrail() throws Exception {
        int databaseSizeBeforeUpdate = admAuditTrailRepository.findAll().size();

        // Create the AdmAuditTrail
        AdmAuditTrailDTO admAuditTrailDTO = admAuditTrailMapper.toDto(admAuditTrail);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAdmAuditTrailMockMvc.perform(put("/api/adm-audit-trails")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(admAuditTrailDTO)))
            .andExpect(status().isCreated());

        // Validate the AdmAuditTrail in the database
        List<AdmAuditTrail> admAuditTrailList = admAuditTrailRepository.findAll();
        assertThat(admAuditTrailList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAdmAuditTrail() throws Exception {
        // Initialize the database
        admAuditTrailRepository.saveAndFlush(admAuditTrail);
        admAuditTrailSearchRepository.save(admAuditTrail);
        int databaseSizeBeforeDelete = admAuditTrailRepository.findAll().size();

        // Get the admAuditTrail
        restAdmAuditTrailMockMvc.perform(delete("/api/adm-audit-trails/{id}", admAuditTrail.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean admAuditTrailExistsInEs = admAuditTrailSearchRepository.exists(admAuditTrail.getId());
        assertThat(admAuditTrailExistsInEs).isFalse();

        // Validate the database is empty
        List<AdmAuditTrail> admAuditTrailList = admAuditTrailRepository.findAll();
        assertThat(admAuditTrailList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchAdmAuditTrail() throws Exception {
        // Initialize the database
        admAuditTrailRepository.saveAndFlush(admAuditTrail);
        admAuditTrailSearchRepository.save(admAuditTrail);

        // Search the admAuditTrail
        restAdmAuditTrailMockMvc.perform(get("/api/_search/adm-audit-trails?query=id:" + admAuditTrail.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(admAuditTrail.getId().intValue())))
            .andExpect(jsonPath("$.[*].tableName").value(hasItem(DEFAULT_TABLE_NAME.toString())))
            .andExpect(jsonPath("$.[*].auditStatus").value(hasItem(DEFAULT_AUDIT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].oldValues").value(hasItem(DEFAULT_OLD_VALUES.toString())))
            .andExpect(jsonPath("$.[*].newValues").value(hasItem(DEFAULT_NEW_VALUES.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AdmAuditTrail.class);
        AdmAuditTrail admAuditTrail1 = new AdmAuditTrail();
        admAuditTrail1.setId(1L);
        AdmAuditTrail admAuditTrail2 = new AdmAuditTrail();
        admAuditTrail2.setId(admAuditTrail1.getId());
        assertThat(admAuditTrail1).isEqualTo(admAuditTrail2);
        admAuditTrail2.setId(2L);
        assertThat(admAuditTrail1).isNotEqualTo(admAuditTrail2);
        admAuditTrail1.setId(null);
        assertThat(admAuditTrail1).isNotEqualTo(admAuditTrail2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AdmAuditTrailDTO.class);
        AdmAuditTrailDTO admAuditTrailDTO1 = new AdmAuditTrailDTO();
        admAuditTrailDTO1.setId(1L);
        AdmAuditTrailDTO admAuditTrailDTO2 = new AdmAuditTrailDTO();
        assertThat(admAuditTrailDTO1).isNotEqualTo(admAuditTrailDTO2);
        admAuditTrailDTO2.setId(admAuditTrailDTO1.getId());
        assertThat(admAuditTrailDTO1).isEqualTo(admAuditTrailDTO2);
        admAuditTrailDTO2.setId(2L);
        assertThat(admAuditTrailDTO1).isNotEqualTo(admAuditTrailDTO2);
        admAuditTrailDTO1.setId(null);
        assertThat(admAuditTrailDTO1).isNotEqualTo(admAuditTrailDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(admAuditTrailMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(admAuditTrailMapper.fromId(null)).isNull();
    }
}
