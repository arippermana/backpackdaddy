package com.app.backpackdaddy.web.rest;

import com.app.backpackdaddy.BackpackdaddyApp;

import com.app.backpackdaddy.domain.AdmRefBranch;
import com.app.backpackdaddy.repository.AdmRefBranchRepository;
import com.app.backpackdaddy.repository.search.AdmRefBranchSearchRepository;
import com.app.backpackdaddy.service.dto.AdmRefBranchDTO;
import com.app.backpackdaddy.service.mapper.AdmRefBranchMapper;
import com.app.backpackdaddy.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.app.backpackdaddy.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AdmRefBranchResource REST controller.
 *
 * @see AdmRefBranchResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BackpackdaddyApp.class)
public class AdmRefBranchResourceIntTest {

    private static final String DEFAULT_BRANCH_NAME = "AAAAAAAAAA";
    private static final String UPDATED_BRANCH_NAME = "BBBBBBBBBB";

    @Autowired
    private AdmRefBranchRepository admRefBranchRepository;

    @Autowired
    private AdmRefBranchMapper admRefBranchMapper;

    @Autowired
    private AdmRefBranchSearchRepository admRefBranchSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAdmRefBranchMockMvc;

    private AdmRefBranch admRefBranch;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AdmRefBranchResource admRefBranchResource = new AdmRefBranchResource(admRefBranchRepository, admRefBranchMapper, admRefBranchSearchRepository);
        this.restAdmRefBranchMockMvc = MockMvcBuilders.standaloneSetup(admRefBranchResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AdmRefBranch createEntity(EntityManager em) {
        AdmRefBranch admRefBranch = new AdmRefBranch()
            .branchName(DEFAULT_BRANCH_NAME);
        return admRefBranch;
    }

    @Before
    public void initTest() {
        admRefBranchSearchRepository.deleteAll();
        admRefBranch = createEntity(em);
    }

    @Test
    @Transactional
    public void createAdmRefBranch() throws Exception {
        int databaseSizeBeforeCreate = admRefBranchRepository.findAll().size();

        // Create the AdmRefBranch
        AdmRefBranchDTO admRefBranchDTO = admRefBranchMapper.toDto(admRefBranch);
        restAdmRefBranchMockMvc.perform(post("/api/adm-ref-branches")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(admRefBranchDTO)))
            .andExpect(status().isCreated());

        // Validate the AdmRefBranch in the database
        List<AdmRefBranch> admRefBranchList = admRefBranchRepository.findAll();
        assertThat(admRefBranchList).hasSize(databaseSizeBeforeCreate + 1);
        AdmRefBranch testAdmRefBranch = admRefBranchList.get(admRefBranchList.size() - 1);
        assertThat(testAdmRefBranch.getBranchName()).isEqualTo(DEFAULT_BRANCH_NAME);

        // Validate the AdmRefBranch in Elasticsearch
        AdmRefBranch admRefBranchEs = admRefBranchSearchRepository.findOne(testAdmRefBranch.getId());
        assertThat(admRefBranchEs).isEqualToComparingFieldByField(testAdmRefBranch);
    }

    @Test
    @Transactional
    public void createAdmRefBranchWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = admRefBranchRepository.findAll().size();

        // Create the AdmRefBranch with an existing ID
        admRefBranch.setId(1L);
        AdmRefBranchDTO admRefBranchDTO = admRefBranchMapper.toDto(admRefBranch);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAdmRefBranchMockMvc.perform(post("/api/adm-ref-branches")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(admRefBranchDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AdmRefBranch in the database
        List<AdmRefBranch> admRefBranchList = admRefBranchRepository.findAll();
        assertThat(admRefBranchList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkBranchNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = admRefBranchRepository.findAll().size();
        // set the field null
        admRefBranch.setBranchName(null);

        // Create the AdmRefBranch, which fails.
        AdmRefBranchDTO admRefBranchDTO = admRefBranchMapper.toDto(admRefBranch);

        restAdmRefBranchMockMvc.perform(post("/api/adm-ref-branches")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(admRefBranchDTO)))
            .andExpect(status().isBadRequest());

        List<AdmRefBranch> admRefBranchList = admRefBranchRepository.findAll();
        assertThat(admRefBranchList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAdmRefBranches() throws Exception {
        // Initialize the database
        admRefBranchRepository.saveAndFlush(admRefBranch);

        // Get all the admRefBranchList
        restAdmRefBranchMockMvc.perform(get("/api/adm-ref-branches?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(admRefBranch.getId().intValue())))
            .andExpect(jsonPath("$.[*].branchName").value(hasItem(DEFAULT_BRANCH_NAME.toString())));
    }

    @Test
    @Transactional
    public void getAdmRefBranch() throws Exception {
        // Initialize the database
        admRefBranchRepository.saveAndFlush(admRefBranch);

        // Get the admRefBranch
        restAdmRefBranchMockMvc.perform(get("/api/adm-ref-branches/{id}", admRefBranch.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(admRefBranch.getId().intValue()))
            .andExpect(jsonPath("$.branchName").value(DEFAULT_BRANCH_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAdmRefBranch() throws Exception {
        // Get the admRefBranch
        restAdmRefBranchMockMvc.perform(get("/api/adm-ref-branches/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAdmRefBranch() throws Exception {
        // Initialize the database
        admRefBranchRepository.saveAndFlush(admRefBranch);
        admRefBranchSearchRepository.save(admRefBranch);
        int databaseSizeBeforeUpdate = admRefBranchRepository.findAll().size();

        // Update the admRefBranch
        AdmRefBranch updatedAdmRefBranch = admRefBranchRepository.findOne(admRefBranch.getId());
        updatedAdmRefBranch
            .branchName(UPDATED_BRANCH_NAME);
        AdmRefBranchDTO admRefBranchDTO = admRefBranchMapper.toDto(updatedAdmRefBranch);

        restAdmRefBranchMockMvc.perform(put("/api/adm-ref-branches")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(admRefBranchDTO)))
            .andExpect(status().isOk());

        // Validate the AdmRefBranch in the database
        List<AdmRefBranch> admRefBranchList = admRefBranchRepository.findAll();
        assertThat(admRefBranchList).hasSize(databaseSizeBeforeUpdate);
        AdmRefBranch testAdmRefBranch = admRefBranchList.get(admRefBranchList.size() - 1);
        assertThat(testAdmRefBranch.getBranchName()).isEqualTo(UPDATED_BRANCH_NAME);

        // Validate the AdmRefBranch in Elasticsearch
        AdmRefBranch admRefBranchEs = admRefBranchSearchRepository.findOne(testAdmRefBranch.getId());
        assertThat(admRefBranchEs).isEqualToComparingFieldByField(testAdmRefBranch);
    }

    @Test
    @Transactional
    public void updateNonExistingAdmRefBranch() throws Exception {
        int databaseSizeBeforeUpdate = admRefBranchRepository.findAll().size();

        // Create the AdmRefBranch
        AdmRefBranchDTO admRefBranchDTO = admRefBranchMapper.toDto(admRefBranch);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAdmRefBranchMockMvc.perform(put("/api/adm-ref-branches")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(admRefBranchDTO)))
            .andExpect(status().isCreated());

        // Validate the AdmRefBranch in the database
        List<AdmRefBranch> admRefBranchList = admRefBranchRepository.findAll();
        assertThat(admRefBranchList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAdmRefBranch() throws Exception {
        // Initialize the database
        admRefBranchRepository.saveAndFlush(admRefBranch);
        admRefBranchSearchRepository.save(admRefBranch);
        int databaseSizeBeforeDelete = admRefBranchRepository.findAll().size();

        // Get the admRefBranch
        restAdmRefBranchMockMvc.perform(delete("/api/adm-ref-branches/{id}", admRefBranch.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean admRefBranchExistsInEs = admRefBranchSearchRepository.exists(admRefBranch.getId());
        assertThat(admRefBranchExistsInEs).isFalse();

        // Validate the database is empty
        List<AdmRefBranch> admRefBranchList = admRefBranchRepository.findAll();
        assertThat(admRefBranchList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchAdmRefBranch() throws Exception {
        // Initialize the database
        admRefBranchRepository.saveAndFlush(admRefBranch);
        admRefBranchSearchRepository.save(admRefBranch);

        // Search the admRefBranch
        restAdmRefBranchMockMvc.perform(get("/api/_search/adm-ref-branches?query=id:" + admRefBranch.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(admRefBranch.getId().intValue())))
            .andExpect(jsonPath("$.[*].branchName").value(hasItem(DEFAULT_BRANCH_NAME.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AdmRefBranch.class);
        AdmRefBranch admRefBranch1 = new AdmRefBranch();
        admRefBranch1.setId(1L);
        AdmRefBranch admRefBranch2 = new AdmRefBranch();
        admRefBranch2.setId(admRefBranch1.getId());
        assertThat(admRefBranch1).isEqualTo(admRefBranch2);
        admRefBranch2.setId(2L);
        assertThat(admRefBranch1).isNotEqualTo(admRefBranch2);
        admRefBranch1.setId(null);
        assertThat(admRefBranch1).isNotEqualTo(admRefBranch2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AdmRefBranchDTO.class);
        AdmRefBranchDTO admRefBranchDTO1 = new AdmRefBranchDTO();
        admRefBranchDTO1.setId(1L);
        AdmRefBranchDTO admRefBranchDTO2 = new AdmRefBranchDTO();
        assertThat(admRefBranchDTO1).isNotEqualTo(admRefBranchDTO2);
        admRefBranchDTO2.setId(admRefBranchDTO1.getId());
        assertThat(admRefBranchDTO1).isEqualTo(admRefBranchDTO2);
        admRefBranchDTO2.setId(2L);
        assertThat(admRefBranchDTO1).isNotEqualTo(admRefBranchDTO2);
        admRefBranchDTO1.setId(null);
        assertThat(admRefBranchDTO1).isNotEqualTo(admRefBranchDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(admRefBranchMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(admRefBranchMapper.fromId(null)).isNull();
    }
}
