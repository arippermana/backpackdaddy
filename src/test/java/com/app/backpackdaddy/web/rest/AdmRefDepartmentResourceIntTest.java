package com.app.backpackdaddy.web.rest;

import com.app.backpackdaddy.BackpackdaddyApp;

import com.app.backpackdaddy.domain.AdmRefDepartment;
import com.app.backpackdaddy.repository.AdmRefDepartmentRepository;
import com.app.backpackdaddy.repository.search.AdmRefDepartmentSearchRepository;
import com.app.backpackdaddy.service.dto.AdmRefDepartmentDTO;
import com.app.backpackdaddy.service.mapper.AdmRefDepartmentMapper;
import com.app.backpackdaddy.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.app.backpackdaddy.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AdmRefDepartmentResource REST controller.
 *
 * @see AdmRefDepartmentResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BackpackdaddyApp.class)
public class AdmRefDepartmentResourceIntTest {

    private static final String DEFAULT_DEPARTMENT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_DEPARTMENT_NAME = "BBBBBBBBBB";

    @Autowired
    private AdmRefDepartmentRepository admRefDepartmentRepository;

    @Autowired
    private AdmRefDepartmentMapper admRefDepartmentMapper;

    @Autowired
    private AdmRefDepartmentSearchRepository admRefDepartmentSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAdmRefDepartmentMockMvc;

    private AdmRefDepartment admRefDepartment;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AdmRefDepartmentResource admRefDepartmentResource = new AdmRefDepartmentResource(admRefDepartmentRepository, admRefDepartmentMapper, admRefDepartmentSearchRepository);
        this.restAdmRefDepartmentMockMvc = MockMvcBuilders.standaloneSetup(admRefDepartmentResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AdmRefDepartment createEntity(EntityManager em) {
        AdmRefDepartment admRefDepartment = new AdmRefDepartment()
            .departmentName(DEFAULT_DEPARTMENT_NAME);
        return admRefDepartment;
    }

    @Before
    public void initTest() {
        admRefDepartmentSearchRepository.deleteAll();
        admRefDepartment = createEntity(em);
    }

    @Test
    @Transactional
    public void createAdmRefDepartment() throws Exception {
        int databaseSizeBeforeCreate = admRefDepartmentRepository.findAll().size();

        // Create the AdmRefDepartment
        AdmRefDepartmentDTO admRefDepartmentDTO = admRefDepartmentMapper.toDto(admRefDepartment);
        restAdmRefDepartmentMockMvc.perform(post("/api/adm-ref-departments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(admRefDepartmentDTO)))
            .andExpect(status().isCreated());

        // Validate the AdmRefDepartment in the database
        List<AdmRefDepartment> admRefDepartmentList = admRefDepartmentRepository.findAll();
        assertThat(admRefDepartmentList).hasSize(databaseSizeBeforeCreate + 1);
        AdmRefDepartment testAdmRefDepartment = admRefDepartmentList.get(admRefDepartmentList.size() - 1);
        assertThat(testAdmRefDepartment.getDepartmentName()).isEqualTo(DEFAULT_DEPARTMENT_NAME);

        // Validate the AdmRefDepartment in Elasticsearch
        AdmRefDepartment admRefDepartmentEs = admRefDepartmentSearchRepository.findOne(testAdmRefDepartment.getId());
        assertThat(admRefDepartmentEs).isEqualToComparingFieldByField(testAdmRefDepartment);
    }

    @Test
    @Transactional
    public void createAdmRefDepartmentWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = admRefDepartmentRepository.findAll().size();

        // Create the AdmRefDepartment with an existing ID
        admRefDepartment.setId(1L);
        AdmRefDepartmentDTO admRefDepartmentDTO = admRefDepartmentMapper.toDto(admRefDepartment);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAdmRefDepartmentMockMvc.perform(post("/api/adm-ref-departments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(admRefDepartmentDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AdmRefDepartment in the database
        List<AdmRefDepartment> admRefDepartmentList = admRefDepartmentRepository.findAll();
        assertThat(admRefDepartmentList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDepartmentNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = admRefDepartmentRepository.findAll().size();
        // set the field null
        admRefDepartment.setDepartmentName(null);

        // Create the AdmRefDepartment, which fails.
        AdmRefDepartmentDTO admRefDepartmentDTO = admRefDepartmentMapper.toDto(admRefDepartment);

        restAdmRefDepartmentMockMvc.perform(post("/api/adm-ref-departments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(admRefDepartmentDTO)))
            .andExpect(status().isBadRequest());

        List<AdmRefDepartment> admRefDepartmentList = admRefDepartmentRepository.findAll();
        assertThat(admRefDepartmentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAdmRefDepartments() throws Exception {
        // Initialize the database
        admRefDepartmentRepository.saveAndFlush(admRefDepartment);

        // Get all the admRefDepartmentList
        restAdmRefDepartmentMockMvc.perform(get("/api/adm-ref-departments?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(admRefDepartment.getId().intValue())))
            .andExpect(jsonPath("$.[*].departmentName").value(hasItem(DEFAULT_DEPARTMENT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getAdmRefDepartment() throws Exception {
        // Initialize the database
        admRefDepartmentRepository.saveAndFlush(admRefDepartment);

        // Get the admRefDepartment
        restAdmRefDepartmentMockMvc.perform(get("/api/adm-ref-departments/{id}", admRefDepartment.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(admRefDepartment.getId().intValue()))
            .andExpect(jsonPath("$.departmentName").value(DEFAULT_DEPARTMENT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAdmRefDepartment() throws Exception {
        // Get the admRefDepartment
        restAdmRefDepartmentMockMvc.perform(get("/api/adm-ref-departments/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAdmRefDepartment() throws Exception {
        // Initialize the database
        admRefDepartmentRepository.saveAndFlush(admRefDepartment);
        admRefDepartmentSearchRepository.save(admRefDepartment);
        int databaseSizeBeforeUpdate = admRefDepartmentRepository.findAll().size();

        // Update the admRefDepartment
        AdmRefDepartment updatedAdmRefDepartment = admRefDepartmentRepository.findOne(admRefDepartment.getId());
        updatedAdmRefDepartment
            .departmentName(UPDATED_DEPARTMENT_NAME);
        AdmRefDepartmentDTO admRefDepartmentDTO = admRefDepartmentMapper.toDto(updatedAdmRefDepartment);

        restAdmRefDepartmentMockMvc.perform(put("/api/adm-ref-departments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(admRefDepartmentDTO)))
            .andExpect(status().isOk());

        // Validate the AdmRefDepartment in the database
        List<AdmRefDepartment> admRefDepartmentList = admRefDepartmentRepository.findAll();
        assertThat(admRefDepartmentList).hasSize(databaseSizeBeforeUpdate);
        AdmRefDepartment testAdmRefDepartment = admRefDepartmentList.get(admRefDepartmentList.size() - 1);
        assertThat(testAdmRefDepartment.getDepartmentName()).isEqualTo(UPDATED_DEPARTMENT_NAME);

        // Validate the AdmRefDepartment in Elasticsearch
        AdmRefDepartment admRefDepartmentEs = admRefDepartmentSearchRepository.findOne(testAdmRefDepartment.getId());
        assertThat(admRefDepartmentEs).isEqualToComparingFieldByField(testAdmRefDepartment);
    }

    @Test
    @Transactional
    public void updateNonExistingAdmRefDepartment() throws Exception {
        int databaseSizeBeforeUpdate = admRefDepartmentRepository.findAll().size();

        // Create the AdmRefDepartment
        AdmRefDepartmentDTO admRefDepartmentDTO = admRefDepartmentMapper.toDto(admRefDepartment);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAdmRefDepartmentMockMvc.perform(put("/api/adm-ref-departments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(admRefDepartmentDTO)))
            .andExpect(status().isCreated());

        // Validate the AdmRefDepartment in the database
        List<AdmRefDepartment> admRefDepartmentList = admRefDepartmentRepository.findAll();
        assertThat(admRefDepartmentList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAdmRefDepartment() throws Exception {
        // Initialize the database
        admRefDepartmentRepository.saveAndFlush(admRefDepartment);
        admRefDepartmentSearchRepository.save(admRefDepartment);
        int databaseSizeBeforeDelete = admRefDepartmentRepository.findAll().size();

        // Get the admRefDepartment
        restAdmRefDepartmentMockMvc.perform(delete("/api/adm-ref-departments/{id}", admRefDepartment.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean admRefDepartmentExistsInEs = admRefDepartmentSearchRepository.exists(admRefDepartment.getId());
        assertThat(admRefDepartmentExistsInEs).isFalse();

        // Validate the database is empty
        List<AdmRefDepartment> admRefDepartmentList = admRefDepartmentRepository.findAll();
        assertThat(admRefDepartmentList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchAdmRefDepartment() throws Exception {
        // Initialize the database
        admRefDepartmentRepository.saveAndFlush(admRefDepartment);
        admRefDepartmentSearchRepository.save(admRefDepartment);

        // Search the admRefDepartment
        restAdmRefDepartmentMockMvc.perform(get("/api/_search/adm-ref-departments?query=id:" + admRefDepartment.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(admRefDepartment.getId().intValue())))
            .andExpect(jsonPath("$.[*].departmentName").value(hasItem(DEFAULT_DEPARTMENT_NAME.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AdmRefDepartment.class);
        AdmRefDepartment admRefDepartment1 = new AdmRefDepartment();
        admRefDepartment1.setId(1L);
        AdmRefDepartment admRefDepartment2 = new AdmRefDepartment();
        admRefDepartment2.setId(admRefDepartment1.getId());
        assertThat(admRefDepartment1).isEqualTo(admRefDepartment2);
        admRefDepartment2.setId(2L);
        assertThat(admRefDepartment1).isNotEqualTo(admRefDepartment2);
        admRefDepartment1.setId(null);
        assertThat(admRefDepartment1).isNotEqualTo(admRefDepartment2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AdmRefDepartmentDTO.class);
        AdmRefDepartmentDTO admRefDepartmentDTO1 = new AdmRefDepartmentDTO();
        admRefDepartmentDTO1.setId(1L);
        AdmRefDepartmentDTO admRefDepartmentDTO2 = new AdmRefDepartmentDTO();
        assertThat(admRefDepartmentDTO1).isNotEqualTo(admRefDepartmentDTO2);
        admRefDepartmentDTO2.setId(admRefDepartmentDTO1.getId());
        assertThat(admRefDepartmentDTO1).isEqualTo(admRefDepartmentDTO2);
        admRefDepartmentDTO2.setId(2L);
        assertThat(admRefDepartmentDTO1).isNotEqualTo(admRefDepartmentDTO2);
        admRefDepartmentDTO1.setId(null);
        assertThat(admRefDepartmentDTO1).isNotEqualTo(admRefDepartmentDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(admRefDepartmentMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(admRefDepartmentMapper.fromId(null)).isNull();
    }
}
