package com.app.backpackdaddy.web.rest;

import com.app.backpackdaddy.BackpackdaddyApp;

import com.app.backpackdaddy.domain.AdmRefGroup;
import com.app.backpackdaddy.repository.AdmRefGroupRepository;
import com.app.backpackdaddy.repository.search.AdmRefGroupSearchRepository;
import com.app.backpackdaddy.service.dto.AdmRefGroupDTO;
import com.app.backpackdaddy.service.mapper.AdmRefGroupMapper;
import com.app.backpackdaddy.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.app.backpackdaddy.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AdmRefGroupResource REST controller.
 *
 * @see AdmRefGroupResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BackpackdaddyApp.class)
public class AdmRefGroupResourceIntTest {

    private static final String DEFAULT_GROUP_NAME = "AAAAAAAAAA";
    private static final String UPDATED_GROUP_NAME = "BBBBBBBBBB";

    @Autowired
    private AdmRefGroupRepository admRefGroupRepository;

    @Autowired
    private AdmRefGroupMapper admRefGroupMapper;

    @Autowired
    private AdmRefGroupSearchRepository admRefGroupSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAdmRefGroupMockMvc;

    private AdmRefGroup admRefGroup;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AdmRefGroupResource admRefGroupResource = new AdmRefGroupResource(admRefGroupRepository, admRefGroupMapper, admRefGroupSearchRepository);
        this.restAdmRefGroupMockMvc = MockMvcBuilders.standaloneSetup(admRefGroupResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AdmRefGroup createEntity(EntityManager em) {
        AdmRefGroup admRefGroup = new AdmRefGroup()
            .groupName(DEFAULT_GROUP_NAME);
        return admRefGroup;
    }

    @Before
    public void initTest() {
        admRefGroupSearchRepository.deleteAll();
        admRefGroup = createEntity(em);
    }

    @Test
    @Transactional
    public void createAdmRefGroup() throws Exception {
        int databaseSizeBeforeCreate = admRefGroupRepository.findAll().size();

        // Create the AdmRefGroup
        AdmRefGroupDTO admRefGroupDTO = admRefGroupMapper.toDto(admRefGroup);
        restAdmRefGroupMockMvc.perform(post("/api/adm-ref-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(admRefGroupDTO)))
            .andExpect(status().isCreated());

        // Validate the AdmRefGroup in the database
        List<AdmRefGroup> admRefGroupList = admRefGroupRepository.findAll();
        assertThat(admRefGroupList).hasSize(databaseSizeBeforeCreate + 1);
        AdmRefGroup testAdmRefGroup = admRefGroupList.get(admRefGroupList.size() - 1);
        assertThat(testAdmRefGroup.getGroupName()).isEqualTo(DEFAULT_GROUP_NAME);

        // Validate the AdmRefGroup in Elasticsearch
        AdmRefGroup admRefGroupEs = admRefGroupSearchRepository.findOne(testAdmRefGroup.getId());
        assertThat(admRefGroupEs).isEqualToComparingFieldByField(testAdmRefGroup);
    }

    @Test
    @Transactional
    public void createAdmRefGroupWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = admRefGroupRepository.findAll().size();

        // Create the AdmRefGroup with an existing ID
        admRefGroup.setId(1L);
        AdmRefGroupDTO admRefGroupDTO = admRefGroupMapper.toDto(admRefGroup);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAdmRefGroupMockMvc.perform(post("/api/adm-ref-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(admRefGroupDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AdmRefGroup in the database
        List<AdmRefGroup> admRefGroupList = admRefGroupRepository.findAll();
        assertThat(admRefGroupList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkGroupNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = admRefGroupRepository.findAll().size();
        // set the field null
        admRefGroup.setGroupName(null);

        // Create the AdmRefGroup, which fails.
        AdmRefGroupDTO admRefGroupDTO = admRefGroupMapper.toDto(admRefGroup);

        restAdmRefGroupMockMvc.perform(post("/api/adm-ref-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(admRefGroupDTO)))
            .andExpect(status().isBadRequest());

        List<AdmRefGroup> admRefGroupList = admRefGroupRepository.findAll();
        assertThat(admRefGroupList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAdmRefGroups() throws Exception {
        // Initialize the database
        admRefGroupRepository.saveAndFlush(admRefGroup);

        // Get all the admRefGroupList
        restAdmRefGroupMockMvc.perform(get("/api/adm-ref-groups?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(admRefGroup.getId().intValue())))
            .andExpect(jsonPath("$.[*].groupName").value(hasItem(DEFAULT_GROUP_NAME.toString())));
    }

    @Test
    @Transactional
    public void getAdmRefGroup() throws Exception {
        // Initialize the database
        admRefGroupRepository.saveAndFlush(admRefGroup);

        // Get the admRefGroup
        restAdmRefGroupMockMvc.perform(get("/api/adm-ref-groups/{id}", admRefGroup.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(admRefGroup.getId().intValue()))
            .andExpect(jsonPath("$.groupName").value(DEFAULT_GROUP_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAdmRefGroup() throws Exception {
        // Get the admRefGroup
        restAdmRefGroupMockMvc.perform(get("/api/adm-ref-groups/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAdmRefGroup() throws Exception {
        // Initialize the database
        admRefGroupRepository.saveAndFlush(admRefGroup);
        admRefGroupSearchRepository.save(admRefGroup);
        int databaseSizeBeforeUpdate = admRefGroupRepository.findAll().size();

        // Update the admRefGroup
        AdmRefGroup updatedAdmRefGroup = admRefGroupRepository.findOne(admRefGroup.getId());
        updatedAdmRefGroup
            .groupName(UPDATED_GROUP_NAME);
        AdmRefGroupDTO admRefGroupDTO = admRefGroupMapper.toDto(updatedAdmRefGroup);

        restAdmRefGroupMockMvc.perform(put("/api/adm-ref-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(admRefGroupDTO)))
            .andExpect(status().isOk());

        // Validate the AdmRefGroup in the database
        List<AdmRefGroup> admRefGroupList = admRefGroupRepository.findAll();
        assertThat(admRefGroupList).hasSize(databaseSizeBeforeUpdate);
        AdmRefGroup testAdmRefGroup = admRefGroupList.get(admRefGroupList.size() - 1);
        assertThat(testAdmRefGroup.getGroupName()).isEqualTo(UPDATED_GROUP_NAME);

        // Validate the AdmRefGroup in Elasticsearch
        AdmRefGroup admRefGroupEs = admRefGroupSearchRepository.findOne(testAdmRefGroup.getId());
        assertThat(admRefGroupEs).isEqualToComparingFieldByField(testAdmRefGroup);
    }

    @Test
    @Transactional
    public void updateNonExistingAdmRefGroup() throws Exception {
        int databaseSizeBeforeUpdate = admRefGroupRepository.findAll().size();

        // Create the AdmRefGroup
        AdmRefGroupDTO admRefGroupDTO = admRefGroupMapper.toDto(admRefGroup);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAdmRefGroupMockMvc.perform(put("/api/adm-ref-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(admRefGroupDTO)))
            .andExpect(status().isCreated());

        // Validate the AdmRefGroup in the database
        List<AdmRefGroup> admRefGroupList = admRefGroupRepository.findAll();
        assertThat(admRefGroupList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAdmRefGroup() throws Exception {
        // Initialize the database
        admRefGroupRepository.saveAndFlush(admRefGroup);
        admRefGroupSearchRepository.save(admRefGroup);
        int databaseSizeBeforeDelete = admRefGroupRepository.findAll().size();

        // Get the admRefGroup
        restAdmRefGroupMockMvc.perform(delete("/api/adm-ref-groups/{id}", admRefGroup.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean admRefGroupExistsInEs = admRefGroupSearchRepository.exists(admRefGroup.getId());
        assertThat(admRefGroupExistsInEs).isFalse();

        // Validate the database is empty
        List<AdmRefGroup> admRefGroupList = admRefGroupRepository.findAll();
        assertThat(admRefGroupList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchAdmRefGroup() throws Exception {
        // Initialize the database
        admRefGroupRepository.saveAndFlush(admRefGroup);
        admRefGroupSearchRepository.save(admRefGroup);

        // Search the admRefGroup
        restAdmRefGroupMockMvc.perform(get("/api/_search/adm-ref-groups?query=id:" + admRefGroup.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(admRefGroup.getId().intValue())))
            .andExpect(jsonPath("$.[*].groupName").value(hasItem(DEFAULT_GROUP_NAME.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AdmRefGroup.class);
        AdmRefGroup admRefGroup1 = new AdmRefGroup();
        admRefGroup1.setId(1L);
        AdmRefGroup admRefGroup2 = new AdmRefGroup();
        admRefGroup2.setId(admRefGroup1.getId());
        assertThat(admRefGroup1).isEqualTo(admRefGroup2);
        admRefGroup2.setId(2L);
        assertThat(admRefGroup1).isNotEqualTo(admRefGroup2);
        admRefGroup1.setId(null);
        assertThat(admRefGroup1).isNotEqualTo(admRefGroup2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AdmRefGroupDTO.class);
        AdmRefGroupDTO admRefGroupDTO1 = new AdmRefGroupDTO();
        admRefGroupDTO1.setId(1L);
        AdmRefGroupDTO admRefGroupDTO2 = new AdmRefGroupDTO();
        assertThat(admRefGroupDTO1).isNotEqualTo(admRefGroupDTO2);
        admRefGroupDTO2.setId(admRefGroupDTO1.getId());
        assertThat(admRefGroupDTO1).isEqualTo(admRefGroupDTO2);
        admRefGroupDTO2.setId(2L);
        assertThat(admRefGroupDTO1).isNotEqualTo(admRefGroupDTO2);
        admRefGroupDTO1.setId(null);
        assertThat(admRefGroupDTO1).isNotEqualTo(admRefGroupDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(admRefGroupMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(admRefGroupMapper.fromId(null)).isNull();
    }
}
