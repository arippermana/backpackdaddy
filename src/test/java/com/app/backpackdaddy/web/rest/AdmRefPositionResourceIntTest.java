package com.app.backpackdaddy.web.rest;

import com.app.backpackdaddy.BackpackdaddyApp;

import com.app.backpackdaddy.domain.AdmRefPosition;
import com.app.backpackdaddy.repository.AdmRefPositionRepository;
import com.app.backpackdaddy.repository.search.AdmRefPositionSearchRepository;
import com.app.backpackdaddy.service.dto.AdmRefPositionDTO;
import com.app.backpackdaddy.service.mapper.AdmRefPositionMapper;
import com.app.backpackdaddy.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.app.backpackdaddy.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AdmRefPositionResource REST controller.
 *
 * @see AdmRefPositionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BackpackdaddyApp.class)
public class AdmRefPositionResourceIntTest {

    private static final String DEFAULT_POSITION_NAME = "AAAAAAAAAA";
    private static final String UPDATED_POSITION_NAME = "BBBBBBBBBB";

    @Autowired
    private AdmRefPositionRepository admRefPositionRepository;

    @Autowired
    private AdmRefPositionMapper admRefPositionMapper;

    @Autowired
    private AdmRefPositionSearchRepository admRefPositionSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAdmRefPositionMockMvc;

    private AdmRefPosition admRefPosition;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AdmRefPositionResource admRefPositionResource = new AdmRefPositionResource(admRefPositionRepository, admRefPositionMapper, admRefPositionSearchRepository);
        this.restAdmRefPositionMockMvc = MockMvcBuilders.standaloneSetup(admRefPositionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AdmRefPosition createEntity(EntityManager em) {
        AdmRefPosition admRefPosition = new AdmRefPosition()
            .positionName(DEFAULT_POSITION_NAME);
        return admRefPosition;
    }

    @Before
    public void initTest() {
        admRefPositionSearchRepository.deleteAll();
        admRefPosition = createEntity(em);
    }

    @Test
    @Transactional
    public void createAdmRefPosition() throws Exception {
        int databaseSizeBeforeCreate = admRefPositionRepository.findAll().size();

        // Create the AdmRefPosition
        AdmRefPositionDTO admRefPositionDTO = admRefPositionMapper.toDto(admRefPosition);
        restAdmRefPositionMockMvc.perform(post("/api/adm-ref-positions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(admRefPositionDTO)))
            .andExpect(status().isCreated());

        // Validate the AdmRefPosition in the database
        List<AdmRefPosition> admRefPositionList = admRefPositionRepository.findAll();
        assertThat(admRefPositionList).hasSize(databaseSizeBeforeCreate + 1);
        AdmRefPosition testAdmRefPosition = admRefPositionList.get(admRefPositionList.size() - 1);
        assertThat(testAdmRefPosition.getPositionName()).isEqualTo(DEFAULT_POSITION_NAME);

        // Validate the AdmRefPosition in Elasticsearch
        AdmRefPosition admRefPositionEs = admRefPositionSearchRepository.findOne(testAdmRefPosition.getId());
        assertThat(admRefPositionEs).isEqualToComparingFieldByField(testAdmRefPosition);
    }

    @Test
    @Transactional
    public void createAdmRefPositionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = admRefPositionRepository.findAll().size();

        // Create the AdmRefPosition with an existing ID
        admRefPosition.setId(1L);
        AdmRefPositionDTO admRefPositionDTO = admRefPositionMapper.toDto(admRefPosition);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAdmRefPositionMockMvc.perform(post("/api/adm-ref-positions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(admRefPositionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AdmRefPosition in the database
        List<AdmRefPosition> admRefPositionList = admRefPositionRepository.findAll();
        assertThat(admRefPositionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkPositionNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = admRefPositionRepository.findAll().size();
        // set the field null
        admRefPosition.setPositionName(null);

        // Create the AdmRefPosition, which fails.
        AdmRefPositionDTO admRefPositionDTO = admRefPositionMapper.toDto(admRefPosition);

        restAdmRefPositionMockMvc.perform(post("/api/adm-ref-positions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(admRefPositionDTO)))
            .andExpect(status().isBadRequest());

        List<AdmRefPosition> admRefPositionList = admRefPositionRepository.findAll();
        assertThat(admRefPositionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAdmRefPositions() throws Exception {
        // Initialize the database
        admRefPositionRepository.saveAndFlush(admRefPosition);

        // Get all the admRefPositionList
        restAdmRefPositionMockMvc.perform(get("/api/adm-ref-positions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(admRefPosition.getId().intValue())))
            .andExpect(jsonPath("$.[*].positionName").value(hasItem(DEFAULT_POSITION_NAME.toString())));
    }

    @Test
    @Transactional
    public void getAdmRefPosition() throws Exception {
        // Initialize the database
        admRefPositionRepository.saveAndFlush(admRefPosition);

        // Get the admRefPosition
        restAdmRefPositionMockMvc.perform(get("/api/adm-ref-positions/{id}", admRefPosition.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(admRefPosition.getId().intValue()))
            .andExpect(jsonPath("$.positionName").value(DEFAULT_POSITION_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAdmRefPosition() throws Exception {
        // Get the admRefPosition
        restAdmRefPositionMockMvc.perform(get("/api/adm-ref-positions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAdmRefPosition() throws Exception {
        // Initialize the database
        admRefPositionRepository.saveAndFlush(admRefPosition);
        admRefPositionSearchRepository.save(admRefPosition);
        int databaseSizeBeforeUpdate = admRefPositionRepository.findAll().size();

        // Update the admRefPosition
        AdmRefPosition updatedAdmRefPosition = admRefPositionRepository.findOne(admRefPosition.getId());
        updatedAdmRefPosition
            .positionName(UPDATED_POSITION_NAME);
        AdmRefPositionDTO admRefPositionDTO = admRefPositionMapper.toDto(updatedAdmRefPosition);

        restAdmRefPositionMockMvc.perform(put("/api/adm-ref-positions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(admRefPositionDTO)))
            .andExpect(status().isOk());

        // Validate the AdmRefPosition in the database
        List<AdmRefPosition> admRefPositionList = admRefPositionRepository.findAll();
        assertThat(admRefPositionList).hasSize(databaseSizeBeforeUpdate);
        AdmRefPosition testAdmRefPosition = admRefPositionList.get(admRefPositionList.size() - 1);
        assertThat(testAdmRefPosition.getPositionName()).isEqualTo(UPDATED_POSITION_NAME);

        // Validate the AdmRefPosition in Elasticsearch
        AdmRefPosition admRefPositionEs = admRefPositionSearchRepository.findOne(testAdmRefPosition.getId());
        assertThat(admRefPositionEs).isEqualToComparingFieldByField(testAdmRefPosition);
    }

    @Test
    @Transactional
    public void updateNonExistingAdmRefPosition() throws Exception {
        int databaseSizeBeforeUpdate = admRefPositionRepository.findAll().size();

        // Create the AdmRefPosition
        AdmRefPositionDTO admRefPositionDTO = admRefPositionMapper.toDto(admRefPosition);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAdmRefPositionMockMvc.perform(put("/api/adm-ref-positions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(admRefPositionDTO)))
            .andExpect(status().isCreated());

        // Validate the AdmRefPosition in the database
        List<AdmRefPosition> admRefPositionList = admRefPositionRepository.findAll();
        assertThat(admRefPositionList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAdmRefPosition() throws Exception {
        // Initialize the database
        admRefPositionRepository.saveAndFlush(admRefPosition);
        admRefPositionSearchRepository.save(admRefPosition);
        int databaseSizeBeforeDelete = admRefPositionRepository.findAll().size();

        // Get the admRefPosition
        restAdmRefPositionMockMvc.perform(delete("/api/adm-ref-positions/{id}", admRefPosition.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean admRefPositionExistsInEs = admRefPositionSearchRepository.exists(admRefPosition.getId());
        assertThat(admRefPositionExistsInEs).isFalse();

        // Validate the database is empty
        List<AdmRefPosition> admRefPositionList = admRefPositionRepository.findAll();
        assertThat(admRefPositionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchAdmRefPosition() throws Exception {
        // Initialize the database
        admRefPositionRepository.saveAndFlush(admRefPosition);
        admRefPositionSearchRepository.save(admRefPosition);

        // Search the admRefPosition
        restAdmRefPositionMockMvc.perform(get("/api/_search/adm-ref-positions?query=id:" + admRefPosition.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(admRefPosition.getId().intValue())))
            .andExpect(jsonPath("$.[*].positionName").value(hasItem(DEFAULT_POSITION_NAME.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AdmRefPosition.class);
        AdmRefPosition admRefPosition1 = new AdmRefPosition();
        admRefPosition1.setId(1L);
        AdmRefPosition admRefPosition2 = new AdmRefPosition();
        admRefPosition2.setId(admRefPosition1.getId());
        assertThat(admRefPosition1).isEqualTo(admRefPosition2);
        admRefPosition2.setId(2L);
        assertThat(admRefPosition1).isNotEqualTo(admRefPosition2);
        admRefPosition1.setId(null);
        assertThat(admRefPosition1).isNotEqualTo(admRefPosition2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AdmRefPositionDTO.class);
        AdmRefPositionDTO admRefPositionDTO1 = new AdmRefPositionDTO();
        admRefPositionDTO1.setId(1L);
        AdmRefPositionDTO admRefPositionDTO2 = new AdmRefPositionDTO();
        assertThat(admRefPositionDTO1).isNotEqualTo(admRefPositionDTO2);
        admRefPositionDTO2.setId(admRefPositionDTO1.getId());
        assertThat(admRefPositionDTO1).isEqualTo(admRefPositionDTO2);
        admRefPositionDTO2.setId(2L);
        assertThat(admRefPositionDTO1).isNotEqualTo(admRefPositionDTO2);
        admRefPositionDTO1.setId(null);
        assertThat(admRefPositionDTO1).isNotEqualTo(admRefPositionDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(admRefPositionMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(admRefPositionMapper.fromId(null)).isNull();
    }
}
