package com.app.backpackdaddy.web.rest;

import com.app.backpackdaddy.BackpackdaddyApp;

import com.app.backpackdaddy.domain.AdmStaff;
import com.app.backpackdaddy.domain.AdmRefBranch;
import com.app.backpackdaddy.domain.AdmRefDepartment;
import com.app.backpackdaddy.domain.AdmRefGroup;
import com.app.backpackdaddy.domain.AdmRefPosition;
import com.app.backpackdaddy.repository.AdmStaffRepository;
import com.app.backpackdaddy.repository.search.AdmStaffSearchRepository;
import com.app.backpackdaddy.service.dto.AdmStaffDTO;
import com.app.backpackdaddy.service.mapper.AdmStaffMapper;
import com.app.backpackdaddy.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.app.backpackdaddy.web.rest.TestUtil.sameInstant;
import static com.app.backpackdaddy.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AdmStaffResource REST controller.
 *
 * @see AdmStaffResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BackpackdaddyApp.class)
public class AdmStaffResourceIntTest {

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_STAFF_NAME = "AAAAAAAAAA";
    private static final String UPDATED_STAFF_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_PASSWORD = "AAAAAAAAAA";
    private static final String UPDATED_PASSWORD = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_HIRE_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_HIRE_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Boolean DEFAULT_IS_ACTIVATED = false;
    private static final Boolean UPDATED_IS_ACTIVATED = true;

    private static final Boolean DEFAULT_IS_ACTIVE = false;
    private static final Boolean UPDATED_IS_ACTIVE = true;

    @Autowired
    private AdmStaffRepository admStaffRepository;

    @Autowired
    private AdmStaffMapper admStaffMapper;

    @Autowired
    private AdmStaffSearchRepository admStaffSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAdmStaffMockMvc;

    private AdmStaff admStaff;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AdmStaffResource admStaffResource = new AdmStaffResource(admStaffRepository, admStaffMapper, admStaffSearchRepository);
        this.restAdmStaffMockMvc = MockMvcBuilders.standaloneSetup(admStaffResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AdmStaff createEntity(EntityManager em) {
        AdmStaff admStaff = new AdmStaff()
            .email(DEFAULT_EMAIL)
            .staffName(DEFAULT_STAFF_NAME)
            .password(DEFAULT_PASSWORD)
            .hireDate(DEFAULT_HIRE_DATE)
            .isActivated(DEFAULT_IS_ACTIVATED)
            .isActive(DEFAULT_IS_ACTIVE);
        // Add required entity
        AdmRefBranch branch = AdmRefBranchResourceIntTest.createEntity(em);
        em.persist(branch);
        em.flush();
        admStaff.setBranch(branch);
        // Add required entity
        AdmRefDepartment department = AdmRefDepartmentResourceIntTest.createEntity(em);
        em.persist(department);
        em.flush();
        admStaff.setDepartment(department);
        // Add required entity
        AdmRefGroup group = AdmRefGroupResourceIntTest.createEntity(em);
        em.persist(group);
        em.flush();
        admStaff.setGroup(group);
        // Add required entity
        AdmRefPosition position = AdmRefPositionResourceIntTest.createEntity(em);
        em.persist(position);
        em.flush();
        admStaff.setPosition(position);
        return admStaff;
    }

    @Before
    public void initTest() {
        admStaffSearchRepository.deleteAll();
        admStaff = createEntity(em);
    }

    @Test
    @Transactional
    public void createAdmStaff() throws Exception {
        int databaseSizeBeforeCreate = admStaffRepository.findAll().size();

        // Create the AdmStaff
        AdmStaffDTO admStaffDTO = admStaffMapper.toDto(admStaff);
        restAdmStaffMockMvc.perform(post("/api/adm-staffs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(admStaffDTO)))
            .andExpect(status().isCreated());

        // Validate the AdmStaff in the database
        List<AdmStaff> admStaffList = admStaffRepository.findAll();
        assertThat(admStaffList).hasSize(databaseSizeBeforeCreate + 1);
        AdmStaff testAdmStaff = admStaffList.get(admStaffList.size() - 1);
        assertThat(testAdmStaff.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testAdmStaff.getStaffName()).isEqualTo(DEFAULT_STAFF_NAME);
        assertThat(testAdmStaff.getPassword()).isEqualTo(DEFAULT_PASSWORD);
        assertThat(testAdmStaff.getHireDate()).isEqualTo(DEFAULT_HIRE_DATE);
        assertThat(testAdmStaff.isIsActivated()).isEqualTo(DEFAULT_IS_ACTIVATED);
        assertThat(testAdmStaff.isIsActive()).isEqualTo(DEFAULT_IS_ACTIVE);

        // Validate the AdmStaff in Elasticsearch
        AdmStaff admStaffEs = admStaffSearchRepository.findOne(testAdmStaff.getId());
        assertThat(admStaffEs).isEqualToComparingFieldByField(testAdmStaff);
    }

    @Test
    @Transactional
    public void createAdmStaffWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = admStaffRepository.findAll().size();

        // Create the AdmStaff with an existing ID
        admStaff.setId(1L);
        AdmStaffDTO admStaffDTO = admStaffMapper.toDto(admStaff);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAdmStaffMockMvc.perform(post("/api/adm-staffs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(admStaffDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AdmStaff in the database
        List<AdmStaff> admStaffList = admStaffRepository.findAll();
        assertThat(admStaffList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkEmailIsRequired() throws Exception {
        int databaseSizeBeforeTest = admStaffRepository.findAll().size();
        // set the field null
        admStaff.setEmail(null);

        // Create the AdmStaff, which fails.
        AdmStaffDTO admStaffDTO = admStaffMapper.toDto(admStaff);

        restAdmStaffMockMvc.perform(post("/api/adm-staffs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(admStaffDTO)))
            .andExpect(status().isBadRequest());

        List<AdmStaff> admStaffList = admStaffRepository.findAll();
        assertThat(admStaffList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkStaffNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = admStaffRepository.findAll().size();
        // set the field null
        admStaff.setStaffName(null);

        // Create the AdmStaff, which fails.
        AdmStaffDTO admStaffDTO = admStaffMapper.toDto(admStaff);

        restAdmStaffMockMvc.perform(post("/api/adm-staffs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(admStaffDTO)))
            .andExpect(status().isBadRequest());

        List<AdmStaff> admStaffList = admStaffRepository.findAll();
        assertThat(admStaffList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPasswordIsRequired() throws Exception {
        int databaseSizeBeforeTest = admStaffRepository.findAll().size();
        // set the field null
        admStaff.setPassword(null);

        // Create the AdmStaff, which fails.
        AdmStaffDTO admStaffDTO = admStaffMapper.toDto(admStaff);

        restAdmStaffMockMvc.perform(post("/api/adm-staffs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(admStaffDTO)))
            .andExpect(status().isBadRequest());

        List<AdmStaff> admStaffList = admStaffRepository.findAll();
        assertThat(admStaffList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkHireDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = admStaffRepository.findAll().size();
        // set the field null
        admStaff.setHireDate(null);

        // Create the AdmStaff, which fails.
        AdmStaffDTO admStaffDTO = admStaffMapper.toDto(admStaff);

        restAdmStaffMockMvc.perform(post("/api/adm-staffs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(admStaffDTO)))
            .andExpect(status().isBadRequest());

        List<AdmStaff> admStaffList = admStaffRepository.findAll();
        assertThat(admStaffList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkIsActivatedIsRequired() throws Exception {
        int databaseSizeBeforeTest = admStaffRepository.findAll().size();
        // set the field null
        admStaff.setIsActivated(null);

        // Create the AdmStaff, which fails.
        AdmStaffDTO admStaffDTO = admStaffMapper.toDto(admStaff);

        restAdmStaffMockMvc.perform(post("/api/adm-staffs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(admStaffDTO)))
            .andExpect(status().isBadRequest());

        List<AdmStaff> admStaffList = admStaffRepository.findAll();
        assertThat(admStaffList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkIsActiveIsRequired() throws Exception {
        int databaseSizeBeforeTest = admStaffRepository.findAll().size();
        // set the field null
        admStaff.setIsActive(null);

        // Create the AdmStaff, which fails.
        AdmStaffDTO admStaffDTO = admStaffMapper.toDto(admStaff);

        restAdmStaffMockMvc.perform(post("/api/adm-staffs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(admStaffDTO)))
            .andExpect(status().isBadRequest());

        List<AdmStaff> admStaffList = admStaffRepository.findAll();
        assertThat(admStaffList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAdmStaffs() throws Exception {
        // Initialize the database
        admStaffRepository.saveAndFlush(admStaff);

        // Get all the admStaffList
        restAdmStaffMockMvc.perform(get("/api/adm-staffs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(admStaff.getId().intValue())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].staffName").value(hasItem(DEFAULT_STAFF_NAME.toString())))
            .andExpect(jsonPath("$.[*].password").value(hasItem(DEFAULT_PASSWORD.toString())))
            .andExpect(jsonPath("$.[*].hireDate").value(hasItem(sameInstant(DEFAULT_HIRE_DATE))))
            .andExpect(jsonPath("$.[*].isActivated").value(hasItem(DEFAULT_IS_ACTIVATED.booleanValue())))
            .andExpect(jsonPath("$.[*].isActive").value(hasItem(DEFAULT_IS_ACTIVE.booleanValue())));
    }

    @Test
    @Transactional
    public void getAdmStaff() throws Exception {
        // Initialize the database
        admStaffRepository.saveAndFlush(admStaff);

        // Get the admStaff
        restAdmStaffMockMvc.perform(get("/api/adm-staffs/{id}", admStaff.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(admStaff.getId().intValue()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.staffName").value(DEFAULT_STAFF_NAME.toString()))
            .andExpect(jsonPath("$.password").value(DEFAULT_PASSWORD.toString()))
            .andExpect(jsonPath("$.hireDate").value(sameInstant(DEFAULT_HIRE_DATE)))
            .andExpect(jsonPath("$.isActivated").value(DEFAULT_IS_ACTIVATED.booleanValue()))
            .andExpect(jsonPath("$.isActive").value(DEFAULT_IS_ACTIVE.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingAdmStaff() throws Exception {
        // Get the admStaff
        restAdmStaffMockMvc.perform(get("/api/adm-staffs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAdmStaff() throws Exception {
        // Initialize the database
        admStaffRepository.saveAndFlush(admStaff);
        admStaffSearchRepository.save(admStaff);
        int databaseSizeBeforeUpdate = admStaffRepository.findAll().size();

        // Update the admStaff
        AdmStaff updatedAdmStaff = admStaffRepository.findOne(admStaff.getId());
        updatedAdmStaff
            .email(UPDATED_EMAIL)
            .staffName(UPDATED_STAFF_NAME)
            .password(UPDATED_PASSWORD)
            .hireDate(UPDATED_HIRE_DATE)
            .isActivated(UPDATED_IS_ACTIVATED)
            .isActive(UPDATED_IS_ACTIVE);
        AdmStaffDTO admStaffDTO = admStaffMapper.toDto(updatedAdmStaff);

        restAdmStaffMockMvc.perform(put("/api/adm-staffs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(admStaffDTO)))
            .andExpect(status().isOk());

        // Validate the AdmStaff in the database
        List<AdmStaff> admStaffList = admStaffRepository.findAll();
        assertThat(admStaffList).hasSize(databaseSizeBeforeUpdate);
        AdmStaff testAdmStaff = admStaffList.get(admStaffList.size() - 1);
        assertThat(testAdmStaff.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testAdmStaff.getStaffName()).isEqualTo(UPDATED_STAFF_NAME);
        assertThat(testAdmStaff.getPassword()).isEqualTo(UPDATED_PASSWORD);
        assertThat(testAdmStaff.getHireDate()).isEqualTo(UPDATED_HIRE_DATE);
        assertThat(testAdmStaff.isIsActivated()).isEqualTo(UPDATED_IS_ACTIVATED);
        assertThat(testAdmStaff.isIsActive()).isEqualTo(UPDATED_IS_ACTIVE);

        // Validate the AdmStaff in Elasticsearch
        AdmStaff admStaffEs = admStaffSearchRepository.findOne(testAdmStaff.getId());
        assertThat(admStaffEs).isEqualToComparingFieldByField(testAdmStaff);
    }

    @Test
    @Transactional
    public void updateNonExistingAdmStaff() throws Exception {
        int databaseSizeBeforeUpdate = admStaffRepository.findAll().size();

        // Create the AdmStaff
        AdmStaffDTO admStaffDTO = admStaffMapper.toDto(admStaff);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAdmStaffMockMvc.perform(put("/api/adm-staffs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(admStaffDTO)))
            .andExpect(status().isCreated());

        // Validate the AdmStaff in the database
        List<AdmStaff> admStaffList = admStaffRepository.findAll();
        assertThat(admStaffList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAdmStaff() throws Exception {
        // Initialize the database
        admStaffRepository.saveAndFlush(admStaff);
        admStaffSearchRepository.save(admStaff);
        int databaseSizeBeforeDelete = admStaffRepository.findAll().size();

        // Get the admStaff
        restAdmStaffMockMvc.perform(delete("/api/adm-staffs/{id}", admStaff.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean admStaffExistsInEs = admStaffSearchRepository.exists(admStaff.getId());
        assertThat(admStaffExistsInEs).isFalse();

        // Validate the database is empty
        List<AdmStaff> admStaffList = admStaffRepository.findAll();
        assertThat(admStaffList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchAdmStaff() throws Exception {
        // Initialize the database
        admStaffRepository.saveAndFlush(admStaff);
        admStaffSearchRepository.save(admStaff);

        // Search the admStaff
        restAdmStaffMockMvc.perform(get("/api/_search/adm-staffs?query=id:" + admStaff.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(admStaff.getId().intValue())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].staffName").value(hasItem(DEFAULT_STAFF_NAME.toString())))
            .andExpect(jsonPath("$.[*].password").value(hasItem(DEFAULT_PASSWORD.toString())))
            .andExpect(jsonPath("$.[*].hireDate").value(hasItem(sameInstant(DEFAULT_HIRE_DATE))))
            .andExpect(jsonPath("$.[*].isActivated").value(hasItem(DEFAULT_IS_ACTIVATED.booleanValue())))
            .andExpect(jsonPath("$.[*].isActive").value(hasItem(DEFAULT_IS_ACTIVE.booleanValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AdmStaff.class);
        AdmStaff admStaff1 = new AdmStaff();
        admStaff1.setId(1L);
        AdmStaff admStaff2 = new AdmStaff();
        admStaff2.setId(admStaff1.getId());
        assertThat(admStaff1).isEqualTo(admStaff2);
        admStaff2.setId(2L);
        assertThat(admStaff1).isNotEqualTo(admStaff2);
        admStaff1.setId(null);
        assertThat(admStaff1).isNotEqualTo(admStaff2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AdmStaffDTO.class);
        AdmStaffDTO admStaffDTO1 = new AdmStaffDTO();
        admStaffDTO1.setId(1L);
        AdmStaffDTO admStaffDTO2 = new AdmStaffDTO();
        assertThat(admStaffDTO1).isNotEqualTo(admStaffDTO2);
        admStaffDTO2.setId(admStaffDTO1.getId());
        assertThat(admStaffDTO1).isEqualTo(admStaffDTO2);
        admStaffDTO2.setId(2L);
        assertThat(admStaffDTO1).isNotEqualTo(admStaffDTO2);
        admStaffDTO1.setId(null);
        assertThat(admStaffDTO1).isNotEqualTo(admStaffDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(admStaffMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(admStaffMapper.fromId(null)).isNull();
    }
}
