package com.app.backpackdaddy.web.rest;

import com.app.backpackdaddy.BackpackdaddyApp;

import com.app.backpackdaddy.domain.AttachmentDetails;
import com.app.backpackdaddy.repository.AttachmentDetailsRepository;
import com.app.backpackdaddy.service.AttachmentDetailsService;
import com.app.backpackdaddy.repository.search.AttachmentDetailsSearchRepository;
import com.app.backpackdaddy.service.dto.AttachmentDetailsDTO;
import com.app.backpackdaddy.service.mapper.AttachmentDetailsMapper;
import com.app.backpackdaddy.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.app.backpackdaddy.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AttachmentDetailsResource REST controller.
 *
 * @see AttachmentDetailsResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BackpackdaddyApp.class)
public class AttachmentDetailsResourceIntTest {

    private static final String DEFAULT_ATTACHMENT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_ATTACHMENT_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_ATTACHMENT_PATH = "AAAAAAAAAA";
    private static final String UPDATED_ATTACHMENT_PATH = "BBBBBBBBBB";

    @Autowired
    private AttachmentDetailsRepository attachmentDetailsRepository;

    @Autowired
    private AttachmentDetailsMapper attachmentDetailsMapper;

    @Autowired
    private AttachmentDetailsService attachmentDetailsService;

    @Autowired
    private AttachmentDetailsSearchRepository attachmentDetailsSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAttachmentDetailsMockMvc;

    private AttachmentDetails attachmentDetails;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AttachmentDetailsResource attachmentDetailsResource = new AttachmentDetailsResource(attachmentDetailsService);
        this.restAttachmentDetailsMockMvc = MockMvcBuilders.standaloneSetup(attachmentDetailsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AttachmentDetails createEntity(EntityManager em) {
        AttachmentDetails attachmentDetails = new AttachmentDetails()
            .attachmentDescription(DEFAULT_ATTACHMENT_DESCRIPTION)
            .attachmentPath(DEFAULT_ATTACHMENT_PATH);
        return attachmentDetails;
    }

    @Before
    public void initTest() {
        attachmentDetailsSearchRepository.deleteAll();
        attachmentDetails = createEntity(em);
    }

    @Test
    @Transactional
    public void createAttachmentDetails() throws Exception {
        int databaseSizeBeforeCreate = attachmentDetailsRepository.findAll().size();

        // Create the AttachmentDetails
        AttachmentDetailsDTO attachmentDetailsDTO = attachmentDetailsMapper.toDto(attachmentDetails);
        restAttachmentDetailsMockMvc.perform(post("/api/attachment-details")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(attachmentDetailsDTO)))
            .andExpect(status().isCreated());

        // Validate the AttachmentDetails in the database
        List<AttachmentDetails> attachmentDetailsList = attachmentDetailsRepository.findAll();
        assertThat(attachmentDetailsList).hasSize(databaseSizeBeforeCreate + 1);
        AttachmentDetails testAttachmentDetails = attachmentDetailsList.get(attachmentDetailsList.size() - 1);
        assertThat(testAttachmentDetails.getAttachmentDescription()).isEqualTo(DEFAULT_ATTACHMENT_DESCRIPTION);
        assertThat(testAttachmentDetails.getAttachmentPath()).isEqualTo(DEFAULT_ATTACHMENT_PATH);

        // Validate the AttachmentDetails in Elasticsearch
        AttachmentDetails attachmentDetailsEs = attachmentDetailsSearchRepository.findOne(testAttachmentDetails.getId());
        assertThat(attachmentDetailsEs).isEqualToComparingFieldByField(testAttachmentDetails);
    }

    @Test
    @Transactional
    public void createAttachmentDetailsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = attachmentDetailsRepository.findAll().size();

        // Create the AttachmentDetails with an existing ID
        attachmentDetails.setId(1L);
        AttachmentDetailsDTO attachmentDetailsDTO = attachmentDetailsMapper.toDto(attachmentDetails);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAttachmentDetailsMockMvc.perform(post("/api/attachment-details")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(attachmentDetailsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AttachmentDetails in the database
        List<AttachmentDetails> attachmentDetailsList = attachmentDetailsRepository.findAll();
        assertThat(attachmentDetailsList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkAttachmentDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = attachmentDetailsRepository.findAll().size();
        // set the field null
        attachmentDetails.setAttachmentDescription(null);

        // Create the AttachmentDetails, which fails.
        AttachmentDetailsDTO attachmentDetailsDTO = attachmentDetailsMapper.toDto(attachmentDetails);

        restAttachmentDetailsMockMvc.perform(post("/api/attachment-details")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(attachmentDetailsDTO)))
            .andExpect(status().isBadRequest());

        List<AttachmentDetails> attachmentDetailsList = attachmentDetailsRepository.findAll();
        assertThat(attachmentDetailsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAttachmentPathIsRequired() throws Exception {
        int databaseSizeBeforeTest = attachmentDetailsRepository.findAll().size();
        // set the field null
        attachmentDetails.setAttachmentPath(null);

        // Create the AttachmentDetails, which fails.
        AttachmentDetailsDTO attachmentDetailsDTO = attachmentDetailsMapper.toDto(attachmentDetails);

        restAttachmentDetailsMockMvc.perform(post("/api/attachment-details")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(attachmentDetailsDTO)))
            .andExpect(status().isBadRequest());

        List<AttachmentDetails> attachmentDetailsList = attachmentDetailsRepository.findAll();
        assertThat(attachmentDetailsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAttachmentDetails() throws Exception {
        // Initialize the database
        attachmentDetailsRepository.saveAndFlush(attachmentDetails);

        // Get all the attachmentDetailsList
        restAttachmentDetailsMockMvc.perform(get("/api/attachment-details?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(attachmentDetails.getId().intValue())))
            .andExpect(jsonPath("$.[*].attachmentDescription").value(hasItem(DEFAULT_ATTACHMENT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].attachmentPath").value(hasItem(DEFAULT_ATTACHMENT_PATH.toString())));
    }

    @Test
    @Transactional
    public void getAttachmentDetails() throws Exception {
        // Initialize the database
        attachmentDetailsRepository.saveAndFlush(attachmentDetails);

        // Get the attachmentDetails
        restAttachmentDetailsMockMvc.perform(get("/api/attachment-details/{id}", attachmentDetails.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(attachmentDetails.getId().intValue()))
            .andExpect(jsonPath("$.attachmentDescription").value(DEFAULT_ATTACHMENT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.attachmentPath").value(DEFAULT_ATTACHMENT_PATH.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAttachmentDetails() throws Exception {
        // Get the attachmentDetails
        restAttachmentDetailsMockMvc.perform(get("/api/attachment-details/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAttachmentDetails() throws Exception {
        // Initialize the database
        attachmentDetailsRepository.saveAndFlush(attachmentDetails);
        attachmentDetailsSearchRepository.save(attachmentDetails);
        int databaseSizeBeforeUpdate = attachmentDetailsRepository.findAll().size();

        // Update the attachmentDetails
        AttachmentDetails updatedAttachmentDetails = attachmentDetailsRepository.findOne(attachmentDetails.getId());
        updatedAttachmentDetails
            .attachmentDescription(UPDATED_ATTACHMENT_DESCRIPTION)
            .attachmentPath(UPDATED_ATTACHMENT_PATH);
        AttachmentDetailsDTO attachmentDetailsDTO = attachmentDetailsMapper.toDto(updatedAttachmentDetails);

        restAttachmentDetailsMockMvc.perform(put("/api/attachment-details")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(attachmentDetailsDTO)))
            .andExpect(status().isOk());

        // Validate the AttachmentDetails in the database
        List<AttachmentDetails> attachmentDetailsList = attachmentDetailsRepository.findAll();
        assertThat(attachmentDetailsList).hasSize(databaseSizeBeforeUpdate);
        AttachmentDetails testAttachmentDetails = attachmentDetailsList.get(attachmentDetailsList.size() - 1);
        assertThat(testAttachmentDetails.getAttachmentDescription()).isEqualTo(UPDATED_ATTACHMENT_DESCRIPTION);
        assertThat(testAttachmentDetails.getAttachmentPath()).isEqualTo(UPDATED_ATTACHMENT_PATH);

        // Validate the AttachmentDetails in Elasticsearch
        AttachmentDetails attachmentDetailsEs = attachmentDetailsSearchRepository.findOne(testAttachmentDetails.getId());
        assertThat(attachmentDetailsEs).isEqualToComparingFieldByField(testAttachmentDetails);
    }

    @Test
    @Transactional
    public void updateNonExistingAttachmentDetails() throws Exception {
        int databaseSizeBeforeUpdate = attachmentDetailsRepository.findAll().size();

        // Create the AttachmentDetails
        AttachmentDetailsDTO attachmentDetailsDTO = attachmentDetailsMapper.toDto(attachmentDetails);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAttachmentDetailsMockMvc.perform(put("/api/attachment-details")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(attachmentDetailsDTO)))
            .andExpect(status().isCreated());

        // Validate the AttachmentDetails in the database
        List<AttachmentDetails> attachmentDetailsList = attachmentDetailsRepository.findAll();
        assertThat(attachmentDetailsList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAttachmentDetails() throws Exception {
        // Initialize the database
        attachmentDetailsRepository.saveAndFlush(attachmentDetails);
        attachmentDetailsSearchRepository.save(attachmentDetails);
        int databaseSizeBeforeDelete = attachmentDetailsRepository.findAll().size();

        // Get the attachmentDetails
        restAttachmentDetailsMockMvc.perform(delete("/api/attachment-details/{id}", attachmentDetails.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean attachmentDetailsExistsInEs = attachmentDetailsSearchRepository.exists(attachmentDetails.getId());
        assertThat(attachmentDetailsExistsInEs).isFalse();

        // Validate the database is empty
        List<AttachmentDetails> attachmentDetailsList = attachmentDetailsRepository.findAll();
        assertThat(attachmentDetailsList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchAttachmentDetails() throws Exception {
        // Initialize the database
        attachmentDetailsRepository.saveAndFlush(attachmentDetails);
        attachmentDetailsSearchRepository.save(attachmentDetails);

        // Search the attachmentDetails
        restAttachmentDetailsMockMvc.perform(get("/api/_search/attachment-details?query=id:" + attachmentDetails.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(attachmentDetails.getId().intValue())))
            .andExpect(jsonPath("$.[*].attachmentDescription").value(hasItem(DEFAULT_ATTACHMENT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].attachmentPath").value(hasItem(DEFAULT_ATTACHMENT_PATH.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AttachmentDetails.class);
        AttachmentDetails attachmentDetails1 = new AttachmentDetails();
        attachmentDetails1.setId(1L);
        AttachmentDetails attachmentDetails2 = new AttachmentDetails();
        attachmentDetails2.setId(attachmentDetails1.getId());
        assertThat(attachmentDetails1).isEqualTo(attachmentDetails2);
        attachmentDetails2.setId(2L);
        assertThat(attachmentDetails1).isNotEqualTo(attachmentDetails2);
        attachmentDetails1.setId(null);
        assertThat(attachmentDetails1).isNotEqualTo(attachmentDetails2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AttachmentDetailsDTO.class);
        AttachmentDetailsDTO attachmentDetailsDTO1 = new AttachmentDetailsDTO();
        attachmentDetailsDTO1.setId(1L);
        AttachmentDetailsDTO attachmentDetailsDTO2 = new AttachmentDetailsDTO();
        assertThat(attachmentDetailsDTO1).isNotEqualTo(attachmentDetailsDTO2);
        attachmentDetailsDTO2.setId(attachmentDetailsDTO1.getId());
        assertThat(attachmentDetailsDTO1).isEqualTo(attachmentDetailsDTO2);
        attachmentDetailsDTO2.setId(2L);
        assertThat(attachmentDetailsDTO1).isNotEqualTo(attachmentDetailsDTO2);
        attachmentDetailsDTO1.setId(null);
        assertThat(attachmentDetailsDTO1).isNotEqualTo(attachmentDetailsDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(attachmentDetailsMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(attachmentDetailsMapper.fromId(null)).isNull();
    }
}
