package com.app.backpackdaddy.web.rest;

import com.app.backpackdaddy.BackpackdaddyApp;

import com.app.backpackdaddy.domain.Request;
import com.app.backpackdaddy.repository.RequestRepository;
import com.app.backpackdaddy.service.RequestService;
import com.app.backpackdaddy.repository.search.RequestSearchRepository;
import com.app.backpackdaddy.service.dto.RequestDTO;
import com.app.backpackdaddy.service.mapper.RequestMapper;
import com.app.backpackdaddy.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.app.backpackdaddy.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.app.backpackdaddy.domain.enumeration.ShippingMethod;
import com.app.backpackdaddy.domain.enumeration.PreferencePayment;
/**
 * Test class for the RequestResource REST controller.
 *
 * @see RequestResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BackpackdaddyApp.class)
public class RequestResourceIntTest {

    private static final ShippingMethod DEFAULT_SHIPPING_METHOD = ShippingMethod.DIRECT_TRAVELLER;
    private static final ShippingMethod UPDATED_SHIPPING_METHOD = ShippingMethod.COMPANY_SERVICE;

    private static final LocalDate DEFAULT_REQUEST_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_REQUEST_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final Double DEFAULT_PRODUCT_PRICE = 1D;
    private static final Double UPDATED_PRODUCT_PRICE = 2D;

    private static final Integer DEFAULT_PRODUCT_QUANTITY = 1;
    private static final Integer UPDATED_PRODUCT_QUANTITY = 2;

    private static final PreferencePayment DEFAULT_PREFERENCE_PAYMENT = PreferencePayment.NEGOTIABLE;
    private static final PreferencePayment UPDATED_PREFERENCE_PAYMENT = PreferencePayment.RANGE;

    private static final Double DEFAULT_TIP_AMOUNT = 1D;
    private static final Double UPDATED_TIP_AMOUNT = 2D;

    private static final Double DEFAULT_COMMISION_FEE = 1D;
    private static final Double UPDATED_COMMISION_FEE = 2D;

    private static final Double DEFAULT_BAGGAGE_CHARGE = 1D;
    private static final Double UPDATED_BAGGAGE_CHARGE = 2D;

    private static final Double DEFAULT_SHIPPING_CHARGE = 1D;
    private static final Double UPDATED_SHIPPING_CHARGE = 2D;

    private static final String DEFAULT_REMARKS = "AAAAAAAAAA";
    private static final String UPDATED_REMARKS = "BBBBBBBBBB";

    private static final String DEFAULT_IS_DRAFT = "AAAAAAAAAA";
    private static final String UPDATED_IS_DRAFT = "BBBBBBBBBB";

    @Autowired
    private RequestRepository requestRepository;

    @Autowired
    private RequestMapper requestMapper;

    @Autowired
    private RequestService requestService;

    @Autowired
    private RequestSearchRepository requestSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restRequestMockMvc;

    private Request request;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RequestResource requestResource = new RequestResource(requestService);
        this.restRequestMockMvc = MockMvcBuilders.standaloneSetup(requestResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Request createEntity(EntityManager em) {
        Request request = new Request()
            .shippingMethod(DEFAULT_SHIPPING_METHOD)
            .requestDate(DEFAULT_REQUEST_DATE)
            .name(DEFAULT_NAME)
            .address(DEFAULT_ADDRESS)
            .productPrice(DEFAULT_PRODUCT_PRICE)
            .productQuantity(DEFAULT_PRODUCT_QUANTITY)
            .preferencePayment(DEFAULT_PREFERENCE_PAYMENT)
            .tipAmount(DEFAULT_TIP_AMOUNT)
            .commisionFee(DEFAULT_COMMISION_FEE)
            .baggageCharge(DEFAULT_BAGGAGE_CHARGE)
            .shippingCharge(DEFAULT_SHIPPING_CHARGE)
            .remarks(DEFAULT_REMARKS)
            .isDraft(DEFAULT_IS_DRAFT);
        return request;
    }

    @Before
    public void initTest() {
        requestSearchRepository.deleteAll();
        request = createEntity(em);
    }

    @Test
    @Transactional
    public void createRequest() throws Exception {
        int databaseSizeBeforeCreate = requestRepository.findAll().size();

        // Create the Request
        RequestDTO requestDTO = requestMapper.toDto(request);
        restRequestMockMvc.perform(post("/api/requests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(requestDTO)))
            .andExpect(status().isCreated());

        // Validate the Request in the database
        List<Request> requestList = requestRepository.findAll();
        assertThat(requestList).hasSize(databaseSizeBeforeCreate + 1);
        Request testRequest = requestList.get(requestList.size() - 1);
        assertThat(testRequest.getShippingMethod()).isEqualTo(DEFAULT_SHIPPING_METHOD);
        assertThat(testRequest.getRequestDate()).isEqualTo(DEFAULT_REQUEST_DATE);
        assertThat(testRequest.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testRequest.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testRequest.getProductPrice()).isEqualTo(DEFAULT_PRODUCT_PRICE);
        assertThat(testRequest.getProductQuantity()).isEqualTo(DEFAULT_PRODUCT_QUANTITY);
        assertThat(testRequest.getPreferencePayment()).isEqualTo(DEFAULT_PREFERENCE_PAYMENT);
        assertThat(testRequest.getTipAmount()).isEqualTo(DEFAULT_TIP_AMOUNT);
        assertThat(testRequest.getCommisionFee()).isEqualTo(DEFAULT_COMMISION_FEE);
        assertThat(testRequest.getBaggageCharge()).isEqualTo(DEFAULT_BAGGAGE_CHARGE);
        assertThat(testRequest.getShippingCharge()).isEqualTo(DEFAULT_SHIPPING_CHARGE);
        assertThat(testRequest.getRemarks()).isEqualTo(DEFAULT_REMARKS);
        assertThat(testRequest.getIsDraft()).isEqualTo(DEFAULT_IS_DRAFT);

        // Validate the Request in Elasticsearch
        Request requestEs = requestSearchRepository.findOne(testRequest.getId());
        assertThat(requestEs).isEqualToComparingFieldByField(testRequest);
    }

    @Test
    @Transactional
    public void createRequestWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = requestRepository.findAll().size();

        // Create the Request with an existing ID
        request.setId(1L);
        RequestDTO requestDTO = requestMapper.toDto(request);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRequestMockMvc.perform(post("/api/requests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(requestDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Request in the database
        List<Request> requestList = requestRepository.findAll();
        assertThat(requestList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = requestRepository.findAll().size();
        // set the field null
        request.setName(null);

        // Create the Request, which fails.
        RequestDTO requestDTO = requestMapper.toDto(request);

        restRequestMockMvc.perform(post("/api/requests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(requestDTO)))
            .andExpect(status().isBadRequest());

        List<Request> requestList = requestRepository.findAll();
        assertThat(requestList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAddressIsRequired() throws Exception {
        int databaseSizeBeforeTest = requestRepository.findAll().size();
        // set the field null
        request.setAddress(null);

        // Create the Request, which fails.
        RequestDTO requestDTO = requestMapper.toDto(request);

        restRequestMockMvc.perform(post("/api/requests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(requestDTO)))
            .andExpect(status().isBadRequest());

        List<Request> requestList = requestRepository.findAll();
        assertThat(requestList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkProductPriceIsRequired() throws Exception {
        int databaseSizeBeforeTest = requestRepository.findAll().size();
        // set the field null
        request.setProductPrice(null);

        // Create the Request, which fails.
        RequestDTO requestDTO = requestMapper.toDto(request);

        restRequestMockMvc.perform(post("/api/requests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(requestDTO)))
            .andExpect(status().isBadRequest());

        List<Request> requestList = requestRepository.findAll();
        assertThat(requestList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkProductQuantityIsRequired() throws Exception {
        int databaseSizeBeforeTest = requestRepository.findAll().size();
        // set the field null
        request.setProductQuantity(null);

        // Create the Request, which fails.
        RequestDTO requestDTO = requestMapper.toDto(request);

        restRequestMockMvc.perform(post("/api/requests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(requestDTO)))
            .andExpect(status().isBadRequest());

        List<Request> requestList = requestRepository.findAll();
        assertThat(requestList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPreferencePaymentIsRequired() throws Exception {
        int databaseSizeBeforeTest = requestRepository.findAll().size();
        // set the field null
        request.setPreferencePayment(null);

        // Create the Request, which fails.
        RequestDTO requestDTO = requestMapper.toDto(request);

        restRequestMockMvc.perform(post("/api/requests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(requestDTO)))
            .andExpect(status().isBadRequest());

        List<Request> requestList = requestRepository.findAll();
        assertThat(requestList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTipAmountIsRequired() throws Exception {
        int databaseSizeBeforeTest = requestRepository.findAll().size();
        // set the field null
        request.setTipAmount(null);

        // Create the Request, which fails.
        RequestDTO requestDTO = requestMapper.toDto(request);

        restRequestMockMvc.perform(post("/api/requests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(requestDTO)))
            .andExpect(status().isBadRequest());

        List<Request> requestList = requestRepository.findAll();
        assertThat(requestList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRequests() throws Exception {
        // Initialize the database
        requestRepository.saveAndFlush(request);

        // Get all the requestList
        restRequestMockMvc.perform(get("/api/requests?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(request.getId().intValue())))
            .andExpect(jsonPath("$.[*].shippingMethod").value(hasItem(DEFAULT_SHIPPING_METHOD.toString())))
            .andExpect(jsonPath("$.[*].requestDate").value(hasItem(DEFAULT_REQUEST_DATE.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].productPrice").value(hasItem(DEFAULT_PRODUCT_PRICE.doubleValue())))
            .andExpect(jsonPath("$.[*].productQuantity").value(hasItem(DEFAULT_PRODUCT_QUANTITY)))
            .andExpect(jsonPath("$.[*].preferencePayment").value(hasItem(DEFAULT_PREFERENCE_PAYMENT.toString())))
            .andExpect(jsonPath("$.[*].tipAmount").value(hasItem(DEFAULT_TIP_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].commisionFee").value(hasItem(DEFAULT_COMMISION_FEE.doubleValue())))
            .andExpect(jsonPath("$.[*].baggageCharge").value(hasItem(DEFAULT_BAGGAGE_CHARGE.doubleValue())))
            .andExpect(jsonPath("$.[*].shippingCharge").value(hasItem(DEFAULT_SHIPPING_CHARGE.doubleValue())))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS.toString())))
            .andExpect(jsonPath("$.[*].isDraft").value(hasItem(DEFAULT_IS_DRAFT.toString())));
    }

    @Test
    @Transactional
    public void getRequest() throws Exception {
        // Initialize the database
        requestRepository.saveAndFlush(request);

        // Get the request
        restRequestMockMvc.perform(get("/api/requests/{id}", request.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(request.getId().intValue()))
            .andExpect(jsonPath("$.shippingMethod").value(DEFAULT_SHIPPING_METHOD.toString()))
            .andExpect(jsonPath("$.requestDate").value(DEFAULT_REQUEST_DATE.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS.toString()))
            .andExpect(jsonPath("$.productPrice").value(DEFAULT_PRODUCT_PRICE.doubleValue()))
            .andExpect(jsonPath("$.productQuantity").value(DEFAULT_PRODUCT_QUANTITY))
            .andExpect(jsonPath("$.preferencePayment").value(DEFAULT_PREFERENCE_PAYMENT.toString()))
            .andExpect(jsonPath("$.tipAmount").value(DEFAULT_TIP_AMOUNT.doubleValue()))
            .andExpect(jsonPath("$.commisionFee").value(DEFAULT_COMMISION_FEE.doubleValue()))
            .andExpect(jsonPath("$.baggageCharge").value(DEFAULT_BAGGAGE_CHARGE.doubleValue()))
            .andExpect(jsonPath("$.shippingCharge").value(DEFAULT_SHIPPING_CHARGE.doubleValue()))
            .andExpect(jsonPath("$.remarks").value(DEFAULT_REMARKS.toString()))
            .andExpect(jsonPath("$.isDraft").value(DEFAULT_IS_DRAFT.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingRequest() throws Exception {
        // Get the request
        restRequestMockMvc.perform(get("/api/requests/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRequest() throws Exception {
        // Initialize the database
        requestRepository.saveAndFlush(request);
        requestSearchRepository.save(request);
        int databaseSizeBeforeUpdate = requestRepository.findAll().size();

        // Update the request
        Request updatedRequest = requestRepository.findOne(request.getId());
        updatedRequest
            .shippingMethod(UPDATED_SHIPPING_METHOD)
            .requestDate(UPDATED_REQUEST_DATE)
            .name(UPDATED_NAME)
            .address(UPDATED_ADDRESS)
            .productPrice(UPDATED_PRODUCT_PRICE)
            .productQuantity(UPDATED_PRODUCT_QUANTITY)
            .preferencePayment(UPDATED_PREFERENCE_PAYMENT)
            .tipAmount(UPDATED_TIP_AMOUNT)
            .commisionFee(UPDATED_COMMISION_FEE)
            .baggageCharge(UPDATED_BAGGAGE_CHARGE)
            .shippingCharge(UPDATED_SHIPPING_CHARGE)
            .remarks(UPDATED_REMARKS)
            .isDraft(UPDATED_IS_DRAFT);
        RequestDTO requestDTO = requestMapper.toDto(updatedRequest);

        restRequestMockMvc.perform(put("/api/requests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(requestDTO)))
            .andExpect(status().isOk());

        // Validate the Request in the database
        List<Request> requestList = requestRepository.findAll();
        assertThat(requestList).hasSize(databaseSizeBeforeUpdate);
        Request testRequest = requestList.get(requestList.size() - 1);
        assertThat(testRequest.getShippingMethod()).isEqualTo(UPDATED_SHIPPING_METHOD);
        assertThat(testRequest.getRequestDate()).isEqualTo(UPDATED_REQUEST_DATE);
        assertThat(testRequest.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testRequest.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testRequest.getProductPrice()).isEqualTo(UPDATED_PRODUCT_PRICE);
        assertThat(testRequest.getProductQuantity()).isEqualTo(UPDATED_PRODUCT_QUANTITY);
        assertThat(testRequest.getPreferencePayment()).isEqualTo(UPDATED_PREFERENCE_PAYMENT);
        assertThat(testRequest.getTipAmount()).isEqualTo(UPDATED_TIP_AMOUNT);
        assertThat(testRequest.getCommisionFee()).isEqualTo(UPDATED_COMMISION_FEE);
        assertThat(testRequest.getBaggageCharge()).isEqualTo(UPDATED_BAGGAGE_CHARGE);
        assertThat(testRequest.getShippingCharge()).isEqualTo(UPDATED_SHIPPING_CHARGE);
        assertThat(testRequest.getRemarks()).isEqualTo(UPDATED_REMARKS);
        assertThat(testRequest.getIsDraft()).isEqualTo(UPDATED_IS_DRAFT);

        // Validate the Request in Elasticsearch
        Request requestEs = requestSearchRepository.findOne(testRequest.getId());
        assertThat(requestEs).isEqualToComparingFieldByField(testRequest);
    }

    @Test
    @Transactional
    public void updateNonExistingRequest() throws Exception {
        int databaseSizeBeforeUpdate = requestRepository.findAll().size();

        // Create the Request
        RequestDTO requestDTO = requestMapper.toDto(request);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restRequestMockMvc.perform(put("/api/requests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(requestDTO)))
            .andExpect(status().isCreated());

        // Validate the Request in the database
        List<Request> requestList = requestRepository.findAll();
        assertThat(requestList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteRequest() throws Exception {
        // Initialize the database
        requestRepository.saveAndFlush(request);
        requestSearchRepository.save(request);
        int databaseSizeBeforeDelete = requestRepository.findAll().size();

        // Get the request
        restRequestMockMvc.perform(delete("/api/requests/{id}", request.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean requestExistsInEs = requestSearchRepository.exists(request.getId());
        assertThat(requestExistsInEs).isFalse();

        // Validate the database is empty
        List<Request> requestList = requestRepository.findAll();
        assertThat(requestList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchRequest() throws Exception {
        // Initialize the database
        requestRepository.saveAndFlush(request);
        requestSearchRepository.save(request);

        // Search the request
        restRequestMockMvc.perform(get("/api/_search/requests?query=id:" + request.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(request.getId().intValue())))
            .andExpect(jsonPath("$.[*].shippingMethod").value(hasItem(DEFAULT_SHIPPING_METHOD.toString())))
            .andExpect(jsonPath("$.[*].requestDate").value(hasItem(DEFAULT_REQUEST_DATE.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].productPrice").value(hasItem(DEFAULT_PRODUCT_PRICE.doubleValue())))
            .andExpect(jsonPath("$.[*].productQuantity").value(hasItem(DEFAULT_PRODUCT_QUANTITY)))
            .andExpect(jsonPath("$.[*].preferencePayment").value(hasItem(DEFAULT_PREFERENCE_PAYMENT.toString())))
            .andExpect(jsonPath("$.[*].tipAmount").value(hasItem(DEFAULT_TIP_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].commisionFee").value(hasItem(DEFAULT_COMMISION_FEE.doubleValue())))
            .andExpect(jsonPath("$.[*].baggageCharge").value(hasItem(DEFAULT_BAGGAGE_CHARGE.doubleValue())))
            .andExpect(jsonPath("$.[*].shippingCharge").value(hasItem(DEFAULT_SHIPPING_CHARGE.doubleValue())))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS.toString())))
            .andExpect(jsonPath("$.[*].isDraft").value(hasItem(DEFAULT_IS_DRAFT.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Request.class);
        Request request1 = new Request();
        request1.setId(1L);
        Request request2 = new Request();
        request2.setId(request1.getId());
        assertThat(request1).isEqualTo(request2);
        request2.setId(2L);
        assertThat(request1).isNotEqualTo(request2);
        request1.setId(null);
        assertThat(request1).isNotEqualTo(request2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RequestDTO.class);
        RequestDTO requestDTO1 = new RequestDTO();
        requestDTO1.setId(1L);
        RequestDTO requestDTO2 = new RequestDTO();
        assertThat(requestDTO1).isNotEqualTo(requestDTO2);
        requestDTO2.setId(requestDTO1.getId());
        assertThat(requestDTO1).isEqualTo(requestDTO2);
        requestDTO2.setId(2L);
        assertThat(requestDTO1).isNotEqualTo(requestDTO2);
        requestDTO1.setId(null);
        assertThat(requestDTO1).isNotEqualTo(requestDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(requestMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(requestMapper.fromId(null)).isNull();
    }
}
