package com.app.backpackdaddy.web.rest;

import com.app.backpackdaddy.BackpackdaddyApp;

import com.app.backpackdaddy.domain.TransactionStatus;
import com.app.backpackdaddy.repository.TransactionStatusRepository;
import com.app.backpackdaddy.service.TransactionStatusService;
import com.app.backpackdaddy.repository.search.TransactionStatusSearchRepository;
import com.app.backpackdaddy.service.dto.TransactionStatusDTO;
import com.app.backpackdaddy.service.mapper.TransactionStatusMapper;
import com.app.backpackdaddy.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.app.backpackdaddy.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TransactionStatusResource REST controller.
 *
 * @see TransactionStatusResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BackpackdaddyApp.class)
public class TransactionStatusResourceIntTest {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_FLAG_ID = "AAAAAAAAAA";
    private static final String UPDATED_FLAG_ID = "BBBBBBBBBB";

    @Autowired
    private TransactionStatusRepository transactionStatusRepository;

    @Autowired
    private TransactionStatusMapper transactionStatusMapper;

    @Autowired
    private TransactionStatusService transactionStatusService;

    @Autowired
    private TransactionStatusSearchRepository transactionStatusSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTransactionStatusMockMvc;

    private TransactionStatus transactionStatus;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TransactionStatusResource transactionStatusResource = new TransactionStatusResource(transactionStatusService);
        this.restTransactionStatusMockMvc = MockMvcBuilders.standaloneSetup(transactionStatusResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TransactionStatus createEntity(EntityManager em) {
        TransactionStatus transactionStatus = new TransactionStatus()
            .description(DEFAULT_DESCRIPTION)
            .flagId(DEFAULT_FLAG_ID);
        return transactionStatus;
    }

    @Before
    public void initTest() {
        transactionStatusSearchRepository.deleteAll();
        transactionStatus = createEntity(em);
    }

    @Test
    @Transactional
    public void createTransactionStatus() throws Exception {
        int databaseSizeBeforeCreate = transactionStatusRepository.findAll().size();

        // Create the TransactionStatus
        TransactionStatusDTO transactionStatusDTO = transactionStatusMapper.toDto(transactionStatus);
        restTransactionStatusMockMvc.perform(post("/api/transaction-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(transactionStatusDTO)))
            .andExpect(status().isCreated());

        // Validate the TransactionStatus in the database
        List<TransactionStatus> transactionStatusList = transactionStatusRepository.findAll();
        assertThat(transactionStatusList).hasSize(databaseSizeBeforeCreate + 1);
        TransactionStatus testTransactionStatus = transactionStatusList.get(transactionStatusList.size() - 1);
        assertThat(testTransactionStatus.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testTransactionStatus.getFlagId()).isEqualTo(DEFAULT_FLAG_ID);

        // Validate the TransactionStatus in Elasticsearch
        TransactionStatus transactionStatusEs = transactionStatusSearchRepository.findOne(testTransactionStatus.getId());
        assertThat(transactionStatusEs).isEqualToComparingFieldByField(testTransactionStatus);
    }

    @Test
    @Transactional
    public void createTransactionStatusWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = transactionStatusRepository.findAll().size();

        // Create the TransactionStatus with an existing ID
        transactionStatus.setId(1L);
        TransactionStatusDTO transactionStatusDTO = transactionStatusMapper.toDto(transactionStatus);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTransactionStatusMockMvc.perform(post("/api/transaction-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(transactionStatusDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TransactionStatus in the database
        List<TransactionStatus> transactionStatusList = transactionStatusRepository.findAll();
        assertThat(transactionStatusList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllTransactionStatuses() throws Exception {
        // Initialize the database
        transactionStatusRepository.saveAndFlush(transactionStatus);

        // Get all the transactionStatusList
        restTransactionStatusMockMvc.perform(get("/api/transaction-statuses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(transactionStatus.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].flagId").value(hasItem(DEFAULT_FLAG_ID.toString())));
    }

    @Test
    @Transactional
    public void getTransactionStatus() throws Exception {
        // Initialize the database
        transactionStatusRepository.saveAndFlush(transactionStatus);

        // Get the transactionStatus
        restTransactionStatusMockMvc.perform(get("/api/transaction-statuses/{id}", transactionStatus.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(transactionStatus.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.flagId").value(DEFAULT_FLAG_ID.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTransactionStatus() throws Exception {
        // Get the transactionStatus
        restTransactionStatusMockMvc.perform(get("/api/transaction-statuses/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTransactionStatus() throws Exception {
        // Initialize the database
        transactionStatusRepository.saveAndFlush(transactionStatus);
        transactionStatusSearchRepository.save(transactionStatus);
        int databaseSizeBeforeUpdate = transactionStatusRepository.findAll().size();

        // Update the transactionStatus
        TransactionStatus updatedTransactionStatus = transactionStatusRepository.findOne(transactionStatus.getId());
        updatedTransactionStatus
            .description(UPDATED_DESCRIPTION)
            .flagId(UPDATED_FLAG_ID);
        TransactionStatusDTO transactionStatusDTO = transactionStatusMapper.toDto(updatedTransactionStatus);

        restTransactionStatusMockMvc.perform(put("/api/transaction-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(transactionStatusDTO)))
            .andExpect(status().isOk());

        // Validate the TransactionStatus in the database
        List<TransactionStatus> transactionStatusList = transactionStatusRepository.findAll();
        assertThat(transactionStatusList).hasSize(databaseSizeBeforeUpdate);
        TransactionStatus testTransactionStatus = transactionStatusList.get(transactionStatusList.size() - 1);
        assertThat(testTransactionStatus.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testTransactionStatus.getFlagId()).isEqualTo(UPDATED_FLAG_ID);

        // Validate the TransactionStatus in Elasticsearch
        TransactionStatus transactionStatusEs = transactionStatusSearchRepository.findOne(testTransactionStatus.getId());
        assertThat(transactionStatusEs).isEqualToComparingFieldByField(testTransactionStatus);
    }

    @Test
    @Transactional
    public void updateNonExistingTransactionStatus() throws Exception {
        int databaseSizeBeforeUpdate = transactionStatusRepository.findAll().size();

        // Create the TransactionStatus
        TransactionStatusDTO transactionStatusDTO = transactionStatusMapper.toDto(transactionStatus);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTransactionStatusMockMvc.perform(put("/api/transaction-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(transactionStatusDTO)))
            .andExpect(status().isCreated());

        // Validate the TransactionStatus in the database
        List<TransactionStatus> transactionStatusList = transactionStatusRepository.findAll();
        assertThat(transactionStatusList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTransactionStatus() throws Exception {
        // Initialize the database
        transactionStatusRepository.saveAndFlush(transactionStatus);
        transactionStatusSearchRepository.save(transactionStatus);
        int databaseSizeBeforeDelete = transactionStatusRepository.findAll().size();

        // Get the transactionStatus
        restTransactionStatusMockMvc.perform(delete("/api/transaction-statuses/{id}", transactionStatus.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean transactionStatusExistsInEs = transactionStatusSearchRepository.exists(transactionStatus.getId());
        assertThat(transactionStatusExistsInEs).isFalse();

        // Validate the database is empty
        List<TransactionStatus> transactionStatusList = transactionStatusRepository.findAll();
        assertThat(transactionStatusList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchTransactionStatus() throws Exception {
        // Initialize the database
        transactionStatusRepository.saveAndFlush(transactionStatus);
        transactionStatusSearchRepository.save(transactionStatus);

        // Search the transactionStatus
        restTransactionStatusMockMvc.perform(get("/api/_search/transaction-statuses?query=id:" + transactionStatus.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(transactionStatus.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].flagId").value(hasItem(DEFAULT_FLAG_ID.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TransactionStatus.class);
        TransactionStatus transactionStatus1 = new TransactionStatus();
        transactionStatus1.setId(1L);
        TransactionStatus transactionStatus2 = new TransactionStatus();
        transactionStatus2.setId(transactionStatus1.getId());
        assertThat(transactionStatus1).isEqualTo(transactionStatus2);
        transactionStatus2.setId(2L);
        assertThat(transactionStatus1).isNotEqualTo(transactionStatus2);
        transactionStatus1.setId(null);
        assertThat(transactionStatus1).isNotEqualTo(transactionStatus2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TransactionStatusDTO.class);
        TransactionStatusDTO transactionStatusDTO1 = new TransactionStatusDTO();
        transactionStatusDTO1.setId(1L);
        TransactionStatusDTO transactionStatusDTO2 = new TransactionStatusDTO();
        assertThat(transactionStatusDTO1).isNotEqualTo(transactionStatusDTO2);
        transactionStatusDTO2.setId(transactionStatusDTO1.getId());
        assertThat(transactionStatusDTO1).isEqualTo(transactionStatusDTO2);
        transactionStatusDTO2.setId(2L);
        assertThat(transactionStatusDTO1).isNotEqualTo(transactionStatusDTO2);
        transactionStatusDTO1.setId(null);
        assertThat(transactionStatusDTO1).isNotEqualTo(transactionStatusDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(transactionStatusMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(transactionStatusMapper.fromId(null)).isNull();
    }
}
