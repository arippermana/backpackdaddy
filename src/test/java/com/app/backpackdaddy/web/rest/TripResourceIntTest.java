package com.app.backpackdaddy.web.rest;

import com.app.backpackdaddy.BackpackdaddyApp;

import com.app.backpackdaddy.domain.Trip;
import com.app.backpackdaddy.repository.TripRepository;
import com.app.backpackdaddy.service.TripService;
import com.app.backpackdaddy.repository.search.TripSearchRepository;
import com.app.backpackdaddy.service.dto.TripDTO;
import com.app.backpackdaddy.service.mapper.TripMapper;
import com.app.backpackdaddy.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.app.backpackdaddy.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.app.backpackdaddy.domain.enumeration.PreferencePayment;
/**
 * Test class for the TripResource REST controller.
 *
 * @see TripResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BackpackdaddyApp.class)
public class TripResourceIntTest {

    private static final Integer DEFAULT_WEIGHT = 1;
    private static final Integer UPDATED_WEIGHT = 2;

    private static final String DEFAULT_NOTICE = "AAAAAAAAAA";
    private static final String UPDATED_NOTICE = "BBBBBBBBBB";

    private static final PreferencePayment DEFAULT_PREFERENCE_PAYMENT = PreferencePayment.NEGOTIABLE;
    private static final PreferencePayment UPDATED_PREFERENCE_PAYMENT = PreferencePayment.RANGE;

    private static final Double DEFAULT_RANGE_FROM = 1D;
    private static final Double UPDATED_RANGE_FROM = 2D;

    private static final Double DEFAULT_RANGE_TO = 1D;
    private static final Double UPDATED_RANGE_TO = 2D;

    private static final Double DEFAULT_FIXED_AMOUNT = 1D;
    private static final Double UPDATED_FIXED_AMOUNT = 2D;

    private static final Long DEFAULT_PERCENTAGE_AMOUNT = 1L;
    private static final Long UPDATED_PERCENTAGE_AMOUNT = 2L;

    private static final String DEFAULT_BAGGAGE_INFORMATION = "AAAAAAAAAA";
    private static final String UPDATED_BAGGAGE_INFORMATION = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_SHIP_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_SHIP_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_REMARKS = "AAAAAAAAAA";
    private static final String UPDATED_REMARKS = "BBBBBBBBBB";

    private static final String DEFAULT_IS_DRAFT = "AAAAAAAAAA";
    private static final String UPDATED_IS_DRAFT = "BBBBBBBBBB";

    @Autowired
    private TripRepository tripRepository;

    @Autowired
    private TripMapper tripMapper;

    @Autowired
    private TripService tripService;

    @Autowired
    private TripSearchRepository tripSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTripMockMvc;

    private Trip trip;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TripResource tripResource = new TripResource(tripService);
        this.restTripMockMvc = MockMvcBuilders.standaloneSetup(tripResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Trip createEntity(EntityManager em) {
        Trip trip = new Trip()
            .weight(DEFAULT_WEIGHT)
            .notice(DEFAULT_NOTICE)
            .preferencePayment(DEFAULT_PREFERENCE_PAYMENT)
            .rangeFrom(DEFAULT_RANGE_FROM)
            .rangeTo(DEFAULT_RANGE_TO)
            .fixedAmount(DEFAULT_FIXED_AMOUNT)
            .percentageAmount(DEFAULT_PERCENTAGE_AMOUNT)
            .baggageInformation(DEFAULT_BAGGAGE_INFORMATION)
            .shipDate(DEFAULT_SHIP_DATE)
            .remarks(DEFAULT_REMARKS)
            .isDraft(DEFAULT_IS_DRAFT);
        return trip;
    }

    @Before
    public void initTest() {
        tripSearchRepository.deleteAll();
        trip = createEntity(em);
    }

    @Test
    @Transactional
    public void createTrip() throws Exception {
        int databaseSizeBeforeCreate = tripRepository.findAll().size();

        // Create the Trip
        TripDTO tripDTO = tripMapper.toDto(trip);
        restTripMockMvc.perform(post("/api/trips")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tripDTO)))
            .andExpect(status().isCreated());

        // Validate the Trip in the database
        List<Trip> tripList = tripRepository.findAll();
        assertThat(tripList).hasSize(databaseSizeBeforeCreate + 1);
        Trip testTrip = tripList.get(tripList.size() - 1);
        assertThat(testTrip.getWeight()).isEqualTo(DEFAULT_WEIGHT);
        assertThat(testTrip.getNotice()).isEqualTo(DEFAULT_NOTICE);
        assertThat(testTrip.getPreferencePayment()).isEqualTo(DEFAULT_PREFERENCE_PAYMENT);
        assertThat(testTrip.getRangeFrom()).isEqualTo(DEFAULT_RANGE_FROM);
        assertThat(testTrip.getRangeTo()).isEqualTo(DEFAULT_RANGE_TO);
        assertThat(testTrip.getFixedAmount()).isEqualTo(DEFAULT_FIXED_AMOUNT);
        assertThat(testTrip.getPercentageAmount()).isEqualTo(DEFAULT_PERCENTAGE_AMOUNT);
        assertThat(testTrip.getBaggageInformation()).isEqualTo(DEFAULT_BAGGAGE_INFORMATION);
        assertThat(testTrip.getShipDate()).isEqualTo(DEFAULT_SHIP_DATE);
        assertThat(testTrip.getRemarks()).isEqualTo(DEFAULT_REMARKS);
        assertThat(testTrip.getIsDraft()).isEqualTo(DEFAULT_IS_DRAFT);

        // Validate the Trip in Elasticsearch
        Trip tripEs = tripSearchRepository.findOne(testTrip.getId());
        assertThat(tripEs).isEqualToComparingFieldByField(testTrip);
    }

    @Test
    @Transactional
    public void createTripWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tripRepository.findAll().size();

        // Create the Trip with an existing ID
        trip.setId(1L);
        TripDTO tripDTO = tripMapper.toDto(trip);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTripMockMvc.perform(post("/api/trips")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tripDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Trip in the database
        List<Trip> tripList = tripRepository.findAll();
        assertThat(tripList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkPreferencePaymentIsRequired() throws Exception {
        int databaseSizeBeforeTest = tripRepository.findAll().size();
        // set the field null
        trip.setPreferencePayment(null);

        // Create the Trip, which fails.
        TripDTO tripDTO = tripMapper.toDto(trip);

        restTripMockMvc.perform(post("/api/trips")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tripDTO)))
            .andExpect(status().isBadRequest());

        List<Trip> tripList = tripRepository.findAll();
        assertThat(tripList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTrips() throws Exception {
        // Initialize the database
        tripRepository.saveAndFlush(trip);

        // Get all the tripList
        restTripMockMvc.perform(get("/api/trips?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(trip.getId().intValue())))
            .andExpect(jsonPath("$.[*].weight").value(hasItem(DEFAULT_WEIGHT)))
            .andExpect(jsonPath("$.[*].notice").value(hasItem(DEFAULT_NOTICE.toString())))
            .andExpect(jsonPath("$.[*].preferencePayment").value(hasItem(DEFAULT_PREFERENCE_PAYMENT.toString())))
            .andExpect(jsonPath("$.[*].rangeFrom").value(hasItem(DEFAULT_RANGE_FROM.doubleValue())))
            .andExpect(jsonPath("$.[*].rangeTo").value(hasItem(DEFAULT_RANGE_TO.doubleValue())))
            .andExpect(jsonPath("$.[*].fixedAmount").value(hasItem(DEFAULT_FIXED_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].percentageAmount").value(hasItem(DEFAULT_PERCENTAGE_AMOUNT.intValue())))
            .andExpect(jsonPath("$.[*].baggageInformation").value(hasItem(DEFAULT_BAGGAGE_INFORMATION.toString())))
            .andExpect(jsonPath("$.[*].shipDate").value(hasItem(DEFAULT_SHIP_DATE.toString())))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS.toString())))
            .andExpect(jsonPath("$.[*].isDraft").value(hasItem(DEFAULT_IS_DRAFT.toString())));
    }

    @Test
    @Transactional
    public void getTrip() throws Exception {
        // Initialize the database
        tripRepository.saveAndFlush(trip);

        // Get the trip
        restTripMockMvc.perform(get("/api/trips/{id}", trip.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(trip.getId().intValue()))
            .andExpect(jsonPath("$.weight").value(DEFAULT_WEIGHT))
            .andExpect(jsonPath("$.notice").value(DEFAULT_NOTICE.toString()))
            .andExpect(jsonPath("$.preferencePayment").value(DEFAULT_PREFERENCE_PAYMENT.toString()))
            .andExpect(jsonPath("$.rangeFrom").value(DEFAULT_RANGE_FROM.doubleValue()))
            .andExpect(jsonPath("$.rangeTo").value(DEFAULT_RANGE_TO.doubleValue()))
            .andExpect(jsonPath("$.fixedAmount").value(DEFAULT_FIXED_AMOUNT.doubleValue()))
            .andExpect(jsonPath("$.percentageAmount").value(DEFAULT_PERCENTAGE_AMOUNT.intValue()))
            .andExpect(jsonPath("$.baggageInformation").value(DEFAULT_BAGGAGE_INFORMATION.toString()))
            .andExpect(jsonPath("$.shipDate").value(DEFAULT_SHIP_DATE.toString()))
            .andExpect(jsonPath("$.remarks").value(DEFAULT_REMARKS.toString()))
            .andExpect(jsonPath("$.isDraft").value(DEFAULT_IS_DRAFT.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTrip() throws Exception {
        // Get the trip
        restTripMockMvc.perform(get("/api/trips/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTrip() throws Exception {
        // Initialize the database
        tripRepository.saveAndFlush(trip);
        tripSearchRepository.save(trip);
        int databaseSizeBeforeUpdate = tripRepository.findAll().size();

        // Update the trip
        Trip updatedTrip = tripRepository.findOne(trip.getId());
        updatedTrip
            .weight(UPDATED_WEIGHT)
            .notice(UPDATED_NOTICE)
            .preferencePayment(UPDATED_PREFERENCE_PAYMENT)
            .rangeFrom(UPDATED_RANGE_FROM)
            .rangeTo(UPDATED_RANGE_TO)
            .fixedAmount(UPDATED_FIXED_AMOUNT)
            .percentageAmount(UPDATED_PERCENTAGE_AMOUNT)
            .baggageInformation(UPDATED_BAGGAGE_INFORMATION)
            .shipDate(UPDATED_SHIP_DATE)
            .remarks(UPDATED_REMARKS)
            .isDraft(UPDATED_IS_DRAFT);
        TripDTO tripDTO = tripMapper.toDto(updatedTrip);

        restTripMockMvc.perform(put("/api/trips")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tripDTO)))
            .andExpect(status().isOk());

        // Validate the Trip in the database
        List<Trip> tripList = tripRepository.findAll();
        assertThat(tripList).hasSize(databaseSizeBeforeUpdate);
        Trip testTrip = tripList.get(tripList.size() - 1);
        assertThat(testTrip.getWeight()).isEqualTo(UPDATED_WEIGHT);
        assertThat(testTrip.getNotice()).isEqualTo(UPDATED_NOTICE);
        assertThat(testTrip.getPreferencePayment()).isEqualTo(UPDATED_PREFERENCE_PAYMENT);
        assertThat(testTrip.getRangeFrom()).isEqualTo(UPDATED_RANGE_FROM);
        assertThat(testTrip.getRangeTo()).isEqualTo(UPDATED_RANGE_TO);
        assertThat(testTrip.getFixedAmount()).isEqualTo(UPDATED_FIXED_AMOUNT);
        assertThat(testTrip.getPercentageAmount()).isEqualTo(UPDATED_PERCENTAGE_AMOUNT);
        assertThat(testTrip.getBaggageInformation()).isEqualTo(UPDATED_BAGGAGE_INFORMATION);
        assertThat(testTrip.getShipDate()).isEqualTo(UPDATED_SHIP_DATE);
        assertThat(testTrip.getRemarks()).isEqualTo(UPDATED_REMARKS);
        assertThat(testTrip.getIsDraft()).isEqualTo(UPDATED_IS_DRAFT);

        // Validate the Trip in Elasticsearch
        Trip tripEs = tripSearchRepository.findOne(testTrip.getId());
        assertThat(tripEs).isEqualToComparingFieldByField(testTrip);
    }

    @Test
    @Transactional
    public void updateNonExistingTrip() throws Exception {
        int databaseSizeBeforeUpdate = tripRepository.findAll().size();

        // Create the Trip
        TripDTO tripDTO = tripMapper.toDto(trip);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTripMockMvc.perform(put("/api/trips")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tripDTO)))
            .andExpect(status().isCreated());

        // Validate the Trip in the database
        List<Trip> tripList = tripRepository.findAll();
        assertThat(tripList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTrip() throws Exception {
        // Initialize the database
        tripRepository.saveAndFlush(trip);
        tripSearchRepository.save(trip);
        int databaseSizeBeforeDelete = tripRepository.findAll().size();

        // Get the trip
        restTripMockMvc.perform(delete("/api/trips/{id}", trip.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean tripExistsInEs = tripSearchRepository.exists(trip.getId());
        assertThat(tripExistsInEs).isFalse();

        // Validate the database is empty
        List<Trip> tripList = tripRepository.findAll();
        assertThat(tripList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchTrip() throws Exception {
        // Initialize the database
        tripRepository.saveAndFlush(trip);
        tripSearchRepository.save(trip);

        // Search the trip
        restTripMockMvc.perform(get("/api/_search/trips?query=id:" + trip.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(trip.getId().intValue())))
            .andExpect(jsonPath("$.[*].weight").value(hasItem(DEFAULT_WEIGHT)))
            .andExpect(jsonPath("$.[*].notice").value(hasItem(DEFAULT_NOTICE.toString())))
            .andExpect(jsonPath("$.[*].preferencePayment").value(hasItem(DEFAULT_PREFERENCE_PAYMENT.toString())))
            .andExpect(jsonPath("$.[*].rangeFrom").value(hasItem(DEFAULT_RANGE_FROM.doubleValue())))
            .andExpect(jsonPath("$.[*].rangeTo").value(hasItem(DEFAULT_RANGE_TO.doubleValue())))
            .andExpect(jsonPath("$.[*].fixedAmount").value(hasItem(DEFAULT_FIXED_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].percentageAmount").value(hasItem(DEFAULT_PERCENTAGE_AMOUNT.intValue())))
            .andExpect(jsonPath("$.[*].baggageInformation").value(hasItem(DEFAULT_BAGGAGE_INFORMATION.toString())))
            .andExpect(jsonPath("$.[*].shipDate").value(hasItem(DEFAULT_SHIP_DATE.toString())))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS.toString())))
            .andExpect(jsonPath("$.[*].isDraft").value(hasItem(DEFAULT_IS_DRAFT.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Trip.class);
        Trip trip1 = new Trip();
        trip1.setId(1L);
        Trip trip2 = new Trip();
        trip2.setId(trip1.getId());
        assertThat(trip1).isEqualTo(trip2);
        trip2.setId(2L);
        assertThat(trip1).isNotEqualTo(trip2);
        trip1.setId(null);
        assertThat(trip1).isNotEqualTo(trip2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TripDTO.class);
        TripDTO tripDTO1 = new TripDTO();
        tripDTO1.setId(1L);
        TripDTO tripDTO2 = new TripDTO();
        assertThat(tripDTO1).isNotEqualTo(tripDTO2);
        tripDTO2.setId(tripDTO1.getId());
        assertThat(tripDTO1).isEqualTo(tripDTO2);
        tripDTO2.setId(2L);
        assertThat(tripDTO1).isNotEqualTo(tripDTO2);
        tripDTO1.setId(null);
        assertThat(tripDTO1).isNotEqualTo(tripDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(tripMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(tripMapper.fromId(null)).isNull();
    }
}
