import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('AdmAuditTrail e2e test', () => {

    let navBarPage: NavBarPage;
    let admAuditTrailDialogPage: AdmAuditTrailDialogPage;
    let admAuditTrailComponentsPage: AdmAuditTrailComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load AdmAuditTrails', () => {
        navBarPage.goToEntity('adm-audit-trail');
        admAuditTrailComponentsPage = new AdmAuditTrailComponentsPage();
        expect(admAuditTrailComponentsPage.getTitle()).toMatch(/backpackdaddyApp.admAuditTrail.home.title/);

    });

    it('should load create AdmAuditTrail dialog', () => {
        admAuditTrailComponentsPage.clickOnCreateButton();
        admAuditTrailDialogPage = new AdmAuditTrailDialogPage();
        expect(admAuditTrailDialogPage.getModalTitle()).toMatch(/backpackdaddyApp.admAuditTrail.home.createOrEditLabel/);
        admAuditTrailDialogPage.close();
    });

    it('should create and save AdmAuditTrails', () => {
        admAuditTrailComponentsPage.clickOnCreateButton();
        admAuditTrailDialogPage.setTableNameInput('tableName');
        expect(admAuditTrailDialogPage.getTableNameInput()).toMatch('tableName');
        admAuditTrailDialogPage.setAuditStatusInput('auditStatus');
        expect(admAuditTrailDialogPage.getAuditStatusInput()).toMatch('auditStatus');
        admAuditTrailDialogPage.setOldValuesInput('oldValues');
        expect(admAuditTrailDialogPage.getOldValuesInput()).toMatch('oldValues');
        admAuditTrailDialogPage.setNewValuesInput('newValues');
        expect(admAuditTrailDialogPage.getNewValuesInput()).toMatch('newValues');
        admAuditTrailDialogPage.save();
        expect(admAuditTrailDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class AdmAuditTrailComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-adm-audit-trail div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class AdmAuditTrailDialogPage {
    modalTitle = element(by.css('h4#myAdmAuditTrailLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    tableNameInput = element(by.css('input#field_tableName'));
    auditStatusInput = element(by.css('input#field_auditStatus'));
    oldValuesInput = element(by.css('input#field_oldValues'));
    newValuesInput = element(by.css('input#field_newValues'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setTableNameInput = function (tableName) {
        this.tableNameInput.sendKeys(tableName);
    }

    getTableNameInput = function () {
        return this.tableNameInput.getAttribute('value');
    }

    setAuditStatusInput = function (auditStatus) {
        this.auditStatusInput.sendKeys(auditStatus);
    }

    getAuditStatusInput = function () {
        return this.auditStatusInput.getAttribute('value');
    }

    setOldValuesInput = function (oldValues) {
        this.oldValuesInput.sendKeys(oldValues);
    }

    getOldValuesInput = function () {
        return this.oldValuesInput.getAttribute('value');
    }

    setNewValuesInput = function (newValues) {
        this.newValuesInput.sendKeys(newValues);
    }

    getNewValuesInput = function () {
        return this.newValuesInput.getAttribute('value');
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
