import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('AdmRefBranch e2e test', () => {

    let navBarPage: NavBarPage;
    let admRefBranchDialogPage: AdmRefBranchDialogPage;
    let admRefBranchComponentsPage: AdmRefBranchComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load AdmRefBranches', () => {
        navBarPage.goToEntity('adm-ref-branch');
        admRefBranchComponentsPage = new AdmRefBranchComponentsPage();
        expect(admRefBranchComponentsPage.getTitle()).toMatch(/backpackdaddyApp.admRefBranch.home.title/);

    });

    it('should load create AdmRefBranch dialog', () => {
        admRefBranchComponentsPage.clickOnCreateButton();
        admRefBranchDialogPage = new AdmRefBranchDialogPage();
        expect(admRefBranchDialogPage.getModalTitle()).toMatch(/backpackdaddyApp.admRefBranch.home.createOrEditLabel/);
        admRefBranchDialogPage.close();
    });

    it('should create and save AdmRefBranches', () => {
        admRefBranchComponentsPage.clickOnCreateButton();
        admRefBranchDialogPage.setBranchNameInput('branchName');
        expect(admRefBranchDialogPage.getBranchNameInput()).toMatch('branchName');
        admRefBranchDialogPage.save();
        expect(admRefBranchDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class AdmRefBranchComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-adm-ref-branch div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class AdmRefBranchDialogPage {
    modalTitle = element(by.css('h4#myAdmRefBranchLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    branchNameInput = element(by.css('input#field_branchName'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setBranchNameInput = function (branchName) {
        this.branchNameInput.sendKeys(branchName);
    }

    getBranchNameInput = function () {
        return this.branchNameInput.getAttribute('value');
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
