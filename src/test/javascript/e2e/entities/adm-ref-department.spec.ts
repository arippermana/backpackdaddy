import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('AdmRefDepartment e2e test', () => {

    let navBarPage: NavBarPage;
    let admRefDepartmentDialogPage: AdmRefDepartmentDialogPage;
    let admRefDepartmentComponentsPage: AdmRefDepartmentComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load AdmRefDepartments', () => {
        navBarPage.goToEntity('adm-ref-department');
        admRefDepartmentComponentsPage = new AdmRefDepartmentComponentsPage();
        expect(admRefDepartmentComponentsPage.getTitle()).toMatch(/backpackdaddyApp.admRefDepartment.home.title/);

    });

    it('should load create AdmRefDepartment dialog', () => {
        admRefDepartmentComponentsPage.clickOnCreateButton();
        admRefDepartmentDialogPage = new AdmRefDepartmentDialogPage();
        expect(admRefDepartmentDialogPage.getModalTitle()).toMatch(/backpackdaddyApp.admRefDepartment.home.createOrEditLabel/);
        admRefDepartmentDialogPage.close();
    });

    it('should create and save AdmRefDepartments', () => {
        admRefDepartmentComponentsPage.clickOnCreateButton();
        admRefDepartmentDialogPage.setDepartmentNameInput('departmentName');
        expect(admRefDepartmentDialogPage.getDepartmentNameInput()).toMatch('departmentName');
        admRefDepartmentDialogPage.save();
        expect(admRefDepartmentDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class AdmRefDepartmentComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-adm-ref-department div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class AdmRefDepartmentDialogPage {
    modalTitle = element(by.css('h4#myAdmRefDepartmentLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    departmentNameInput = element(by.css('input#field_departmentName'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setDepartmentNameInput = function (departmentName) {
        this.departmentNameInput.sendKeys(departmentName);
    }

    getDepartmentNameInput = function () {
        return this.departmentNameInput.getAttribute('value');
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
