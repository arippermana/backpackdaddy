import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('AdmRefGroup e2e test', () => {

    let navBarPage: NavBarPage;
    let admRefGroupDialogPage: AdmRefGroupDialogPage;
    let admRefGroupComponentsPage: AdmRefGroupComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load AdmRefGroups', () => {
        navBarPage.goToEntity('adm-ref-group');
        admRefGroupComponentsPage = new AdmRefGroupComponentsPage();
        expect(admRefGroupComponentsPage.getTitle()).toMatch(/backpackdaddyApp.admRefGroup.home.title/);

    });

    it('should load create AdmRefGroup dialog', () => {
        admRefGroupComponentsPage.clickOnCreateButton();
        admRefGroupDialogPage = new AdmRefGroupDialogPage();
        expect(admRefGroupDialogPage.getModalTitle()).toMatch(/backpackdaddyApp.admRefGroup.home.createOrEditLabel/);
        admRefGroupDialogPage.close();
    });

    it('should create and save AdmRefGroups', () => {
        admRefGroupComponentsPage.clickOnCreateButton();
        admRefGroupDialogPage.setGroupNameInput('groupName');
        expect(admRefGroupDialogPage.getGroupNameInput()).toMatch('groupName');
        admRefGroupDialogPage.save();
        expect(admRefGroupDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class AdmRefGroupComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-adm-ref-group div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class AdmRefGroupDialogPage {
    modalTitle = element(by.css('h4#myAdmRefGroupLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    groupNameInput = element(by.css('input#field_groupName'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setGroupNameInput = function (groupName) {
        this.groupNameInput.sendKeys(groupName);
    }

    getGroupNameInput = function () {
        return this.groupNameInput.getAttribute('value');
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
