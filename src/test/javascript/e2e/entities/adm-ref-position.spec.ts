import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('AdmRefPosition e2e test', () => {

    let navBarPage: NavBarPage;
    let admRefPositionDialogPage: AdmRefPositionDialogPage;
    let admRefPositionComponentsPage: AdmRefPositionComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load AdmRefPositions', () => {
        navBarPage.goToEntity('adm-ref-position');
        admRefPositionComponentsPage = new AdmRefPositionComponentsPage();
        expect(admRefPositionComponentsPage.getTitle()).toMatch(/backpackdaddyApp.admRefPosition.home.title/);

    });

    it('should load create AdmRefPosition dialog', () => {
        admRefPositionComponentsPage.clickOnCreateButton();
        admRefPositionDialogPage = new AdmRefPositionDialogPage();
        expect(admRefPositionDialogPage.getModalTitle()).toMatch(/backpackdaddyApp.admRefPosition.home.createOrEditLabel/);
        admRefPositionDialogPage.close();
    });

    it('should create and save AdmRefPositions', () => {
        admRefPositionComponentsPage.clickOnCreateButton();
        admRefPositionDialogPage.setPositionNameInput('positionName');
        expect(admRefPositionDialogPage.getPositionNameInput()).toMatch('positionName');
        admRefPositionDialogPage.save();
        expect(admRefPositionDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class AdmRefPositionComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-adm-ref-position div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class AdmRefPositionDialogPage {
    modalTitle = element(by.css('h4#myAdmRefPositionLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    positionNameInput = element(by.css('input#field_positionName'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setPositionNameInput = function (positionName) {
        this.positionNameInput.sendKeys(positionName);
    }

    getPositionNameInput = function () {
        return this.positionNameInput.getAttribute('value');
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
