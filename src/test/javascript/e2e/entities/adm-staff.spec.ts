import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('AdmStaff e2e test', () => {

    let navBarPage: NavBarPage;
    let admStaffDialogPage: AdmStaffDialogPage;
    let admStaffComponentsPage: AdmStaffComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load AdmStaffs', () => {
        navBarPage.goToEntity('adm-staff');
        admStaffComponentsPage = new AdmStaffComponentsPage();
        expect(admStaffComponentsPage.getTitle()).toMatch(/backpackdaddyApp.admStaff.home.title/);

    });

    it('should load create AdmStaff dialog', () => {
        admStaffComponentsPage.clickOnCreateButton();
        admStaffDialogPage = new AdmStaffDialogPage();
        expect(admStaffDialogPage.getModalTitle()).toMatch(/backpackdaddyApp.admStaff.home.createOrEditLabel/);
        admStaffDialogPage.close();
    });

   /* it('should create and save AdmStaffs', () => {
        admStaffComponentsPage.clickOnCreateButton();
        admStaffDialogPage.setEmailInput('email');
        expect(admStaffDialogPage.getEmailInput()).toMatch('email');
        admStaffDialogPage.setStaffNameInput('staffName');
        expect(admStaffDialogPage.getStaffNameInput()).toMatch('staffName');
        admStaffDialogPage.setPasswordInput('password');
        expect(admStaffDialogPage.getPasswordInput()).toMatch('password');
        admStaffDialogPage.setHireDateInput(12310020012301);
        expect(admStaffDialogPage.getHireDateInput()).toMatch('2001-12-31T02:30');
        admStaffDialogPage.getIsActivatedInput().isSelected().then(function (selected) {
            if (selected) {
                admStaffDialogPage.getIsActivatedInput().click();
                expect(admStaffDialogPage.getIsActivatedInput().isSelected()).toBeFalsy();
            } else {
                admStaffDialogPage.getIsActivatedInput().click();
                expect(admStaffDialogPage.getIsActivatedInput().isSelected()).toBeTruthy();
            }
        });
        admStaffDialogPage.getIsActiveInput().isSelected().then(function (selected) {
            if (selected) {
                admStaffDialogPage.getIsActiveInput().click();
                expect(admStaffDialogPage.getIsActiveInput().isSelected()).toBeFalsy();
            } else {
                admStaffDialogPage.getIsActiveInput().click();
                expect(admStaffDialogPage.getIsActiveInput().isSelected()).toBeTruthy();
            }
        });
        admStaffDialogPage.branchSelectLastOption();
        admStaffDialogPage.departmentSelectLastOption();
        admStaffDialogPage.groupSelectLastOption();
        admStaffDialogPage.positionSelectLastOption();
        admStaffDialogPage.save();
        expect(admStaffDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); */

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class AdmStaffComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-adm-staff div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class AdmStaffDialogPage {
    modalTitle = element(by.css('h4#myAdmStaffLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    emailInput = element(by.css('input#field_email'));
    staffNameInput = element(by.css('input#field_staffName'));
    passwordInput = element(by.css('input#field_password'));
    hireDateInput = element(by.css('input#field_hireDate'));
    isActivatedInput = element(by.css('input#field_isActivated'));
    isActiveInput = element(by.css('input#field_isActive'));
    branchSelect = element(by.css('select#field_branch'));
    departmentSelect = element(by.css('select#field_department'));
    groupSelect = element(by.css('select#field_group'));
    positionSelect = element(by.css('select#field_position'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setEmailInput = function (email) {
        this.emailInput.sendKeys(email);
    }

    getEmailInput = function () {
        return this.emailInput.getAttribute('value');
    }

    setStaffNameInput = function (staffName) {
        this.staffNameInput.sendKeys(staffName);
    }

    getStaffNameInput = function () {
        return this.staffNameInput.getAttribute('value');
    }

    setPasswordInput = function (password) {
        this.passwordInput.sendKeys(password);
    }

    getPasswordInput = function () {
        return this.passwordInput.getAttribute('value');
    }

    setHireDateInput = function (hireDate) {
        this.hireDateInput.sendKeys(hireDate);
    }

    getHireDateInput = function () {
        return this.hireDateInput.getAttribute('value');
    }

    getIsActivatedInput = function () {
        return this.isActivatedInput;
    }
    getIsActiveInput = function () {
        return this.isActiveInput;
    }
    branchSelectLastOption = function () {
        this.branchSelect.all(by.tagName('option')).last().click();
    }

    branchSelectOption = function (option) {
        this.branchSelect.sendKeys(option);
    }

    getBranchSelect = function () {
        return this.branchSelect;
    }

    getBranchSelectedOption = function () {
        return this.branchSelect.element(by.css('option:checked')).getText();
    }

    departmentSelectLastOption = function () {
        this.departmentSelect.all(by.tagName('option')).last().click();
    }

    departmentSelectOption = function (option) {
        this.departmentSelect.sendKeys(option);
    }

    getDepartmentSelect = function () {
        return this.departmentSelect;
    }

    getDepartmentSelectedOption = function () {
        return this.departmentSelect.element(by.css('option:checked')).getText();
    }

    groupSelectLastOption = function () {
        this.groupSelect.all(by.tagName('option')).last().click();
    }

    groupSelectOption = function (option) {
        this.groupSelect.sendKeys(option);
    }

    getGroupSelect = function () {
        return this.groupSelect;
    }

    getGroupSelectedOption = function () {
        return this.groupSelect.element(by.css('option:checked')).getText();
    }

    positionSelectLastOption = function () {
        this.positionSelect.all(by.tagName('option')).last().click();
    }

    positionSelectOption = function (option) {
        this.positionSelect.sendKeys(option);
    }

    getPositionSelect = function () {
        return this.positionSelect;
    }

    getPositionSelectedOption = function () {
        return this.positionSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
