import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('AttachmentDetails e2e test', () => {

    let navBarPage: NavBarPage;
    let attachmentDetailsDialogPage: AttachmentDetailsDialogPage;
    let attachmentDetailsComponentsPage: AttachmentDetailsComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load AttachmentDetails', () => {
        navBarPage.goToEntity('attachment-details');
        attachmentDetailsComponentsPage = new AttachmentDetailsComponentsPage();
        expect(attachmentDetailsComponentsPage.getTitle()).toMatch(/backpackdaddyApp.attachmentDetails.home.title/);

    });

    it('should load create AttachmentDetails dialog', () => {
        attachmentDetailsComponentsPage.clickOnCreateButton();
        attachmentDetailsDialogPage = new AttachmentDetailsDialogPage();
        expect(attachmentDetailsDialogPage.getModalTitle()).toMatch(/backpackdaddyApp.attachmentDetails.home.createOrEditLabel/);
        attachmentDetailsDialogPage.close();
    });

    it('should create and save AttachmentDetails', () => {
        attachmentDetailsComponentsPage.clickOnCreateButton();
        attachmentDetailsDialogPage.setAttachmentDescriptionInput('attachmentDescription');
        expect(attachmentDetailsDialogPage.getAttachmentDescriptionInput()).toMatch('attachmentDescription');
        attachmentDetailsDialogPage.setAttachmentPathInput('attachmentPath');
        expect(attachmentDetailsDialogPage.getAttachmentPathInput()).toMatch('attachmentPath');
        attachmentDetailsDialogPage.trackSelectLastOption();
        attachmentDetailsDialogPage.save();
        expect(attachmentDetailsDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class AttachmentDetailsComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-attachment-details div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class AttachmentDetailsDialogPage {
    modalTitle = element(by.css('h4#myAttachmentDetailsLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    attachmentDescriptionInput = element(by.css('input#field_attachmentDescription'));
    attachmentPathInput = element(by.css('input#field_attachmentPath'));
    trackSelect = element(by.css('select#field_track'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setAttachmentDescriptionInput = function (attachmentDescription) {
        this.attachmentDescriptionInput.sendKeys(attachmentDescription);
    }

    getAttachmentDescriptionInput = function () {
        return this.attachmentDescriptionInput.getAttribute('value');
    }

    setAttachmentPathInput = function (attachmentPath) {
        this.attachmentPathInput.sendKeys(attachmentPath);
    }

    getAttachmentPathInput = function () {
        return this.attachmentPathInput.getAttribute('value');
    }

    trackSelectLastOption = function () {
        this.trackSelect.all(by.tagName('option')).last().click();
    }

    trackSelectOption = function (option) {
        this.trackSelect.sendKeys(option);
    }

    getTrackSelect = function () {
        return this.trackSelect;
    }

    getTrackSelectedOption = function () {
        return this.trackSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
