import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('Category e2e test', () => {

    let navBarPage: NavBarPage;
    let categoryDialogPage: CategoryDialogPage;
    let categoryComponentsPage: CategoryComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Categories', () => {
        navBarPage.goToEntity('category');
        categoryComponentsPage = new CategoryComponentsPage();
        expect(categoryComponentsPage.getTitle()).toMatch(/backpackdaddyApp.category.home.title/);

    });

    it('should load create Category dialog', () => {
        categoryComponentsPage.clickOnCreateButton();
        categoryDialogPage = new CategoryDialogPage();
        expect(categoryDialogPage.getModalTitle()).toMatch(/backpackdaddyApp.category.home.createOrEditLabel/);
        categoryDialogPage.close();
    });

    it('should create and save Categories', () => {
        categoryComponentsPage.clickOnCreateButton();
        categoryDialogPage.setCategoryDescriptionInput('categoryDescription');
        expect(categoryDialogPage.getCategoryDescriptionInput()).toMatch('categoryDescription');
        categoryDialogPage.setLevelInput('5');
        expect(categoryDialogPage.getLevelInput()).toMatch('5');
        categoryDialogPage.setParentIdInput('5');
        expect(categoryDialogPage.getParentIdInput()).toMatch('5');
        categoryDialogPage.setOrderInput('5');
        expect(categoryDialogPage.getOrderInput()).toMatch('5');
        categoryDialogPage.save();
        expect(categoryDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class CategoryComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-category div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class CategoryDialogPage {
    modalTitle = element(by.css('h4#myCategoryLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    categoryDescriptionInput = element(by.css('input#field_categoryDescription'));
    levelInput = element(by.css('input#field_level'));
    parentIdInput = element(by.css('input#field_parentId'));
    orderInput = element(by.css('input#field_order'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setCategoryDescriptionInput = function (categoryDescription) {
        this.categoryDescriptionInput.sendKeys(categoryDescription);
    }

    getCategoryDescriptionInput = function () {
        return this.categoryDescriptionInput.getAttribute('value');
    }

    setLevelInput = function (level) {
        this.levelInput.sendKeys(level);
    }

    getLevelInput = function () {
        return this.levelInput.getAttribute('value');
    }

    setParentIdInput = function (parentId) {
        this.parentIdInput.sendKeys(parentId);
    }

    getParentIdInput = function () {
        return this.parentIdInput.getAttribute('value');
    }

    setOrderInput = function (order) {
        this.orderInput.sendKeys(order);
    }

    getOrderInput = function () {
        return this.orderInput.getAttribute('value');
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
