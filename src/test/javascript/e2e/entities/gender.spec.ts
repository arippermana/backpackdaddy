import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('Gender e2e test', () => {

    let navBarPage: NavBarPage;
    let genderDialogPage: GenderDialogPage;
    let genderComponentsPage: GenderComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Genders', () => {
        navBarPage.goToEntity('gender');
        genderComponentsPage = new GenderComponentsPage();
        expect(genderComponentsPage.getTitle()).toMatch(/backpackdaddyApp.gender.home.title/);

    });

    it('should load create Gender dialog', () => {
        genderComponentsPage.clickOnCreateButton();
        genderDialogPage = new GenderDialogPage();
        expect(genderDialogPage.getModalTitle()).toMatch(/backpackdaddyApp.gender.home.createOrEditLabel/);
        genderDialogPage.close();
    });

    it('should create and save Genders', () => {
        genderComponentsPage.clickOnCreateButton();
        genderDialogPage.setNameInput('name');
        expect(genderDialogPage.getNameInput()).toMatch('name');
        genderDialogPage.save();
        expect(genderDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class GenderComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-gender div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class GenderDialogPage {
    modalTitle = element(by.css('h4#myGenderLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nameInput = element(by.css('input#field_name'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setNameInput = function (name) {
        this.nameInput.sendKeys(name);
    }

    getNameInput = function () {
        return this.nameInput.getAttribute('value');
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
