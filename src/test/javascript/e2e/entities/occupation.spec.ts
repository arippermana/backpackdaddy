import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('Occupation e2e test', () => {

    let navBarPage: NavBarPage;
    let occupationDialogPage: OccupationDialogPage;
    let occupationComponentsPage: OccupationComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Occupations', () => {
        navBarPage.goToEntity('occupation');
        occupationComponentsPage = new OccupationComponentsPage();
        expect(occupationComponentsPage.getTitle()).toMatch(/backpackdaddyApp.occupation.home.title/);

    });

    it('should load create Occupation dialog', () => {
        occupationComponentsPage.clickOnCreateButton();
        occupationDialogPage = new OccupationDialogPage();
        expect(occupationDialogPage.getModalTitle()).toMatch(/backpackdaddyApp.occupation.home.createOrEditLabel/);
        occupationDialogPage.close();
    });

    it('should create and save Occupations', () => {
        occupationComponentsPage.clickOnCreateButton();
        occupationDialogPage.setNameInput('name');
        expect(occupationDialogPage.getNameInput()).toMatch('name');
        occupationDialogPage.save();
        expect(occupationDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class OccupationComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-occupation div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class OccupationDialogPage {
    modalTitle = element(by.css('h4#myOccupationLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nameInput = element(by.css('input#field_name'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setNameInput = function (name) {
        this.nameInput.sendKeys(name);
    }

    getNameInput = function () {
        return this.nameInput.getAttribute('value');
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
