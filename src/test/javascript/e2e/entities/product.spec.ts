import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('Product e2e test', () => {

    let navBarPage: NavBarPage;
    let productDialogPage: ProductDialogPage;
    let productComponentsPage: ProductComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Products', () => {
        navBarPage.goToEntity('product');
        productComponentsPage = new ProductComponentsPage();
        expect(productComponentsPage.getTitle()).toMatch(/backpackdaddyApp.product.home.title/);

    });

    it('should load create Product dialog', () => {
        productComponentsPage.clickOnCreateButton();
        productDialogPage = new ProductDialogPage();
        expect(productDialogPage.getModalTitle()).toMatch(/backpackdaddyApp.product.home.createOrEditLabel/);
        productDialogPage.close();
    });

    it('should create and save Products', () => {
        productComponentsPage.clickOnCreateButton();
        productDialogPage.setProductNameInput('productName');
        expect(productDialogPage.getProductNameInput()).toMatch('productName');
        productDialogPage.setImage1Input('image1');
        expect(productDialogPage.getImage1Input()).toMatch('image1');
        productDialogPage.setImage2Input('image2');
        expect(productDialogPage.getImage2Input()).toMatch('image2');
        productDialogPage.setImage3Input('image3');
        expect(productDialogPage.getImage3Input()).toMatch('image3');
        productDialogPage.setImage4Input('image4');
        expect(productDialogPage.getImage4Input()).toMatch('image4');
        productDialogPage.setImage5Input('image5');
        expect(productDialogPage.getImage5Input()).toMatch('image5');
        productDialogPage.setSpecificationInput('specification');
        expect(productDialogPage.getSpecificationInput()).toMatch('specification');
        productDialogPage.categorySelectLastOption();
        productDialogPage.madeInSelectLastOption();
        productDialogPage.save();
        expect(productDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class ProductComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-product div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class ProductDialogPage {
    modalTitle = element(by.css('h4#myProductLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    productNameInput = element(by.css('input#field_productName'));
    image1Input = element(by.css('input#field_image1'));
    image2Input = element(by.css('input#field_image2'));
    image3Input = element(by.css('input#field_image3'));
    image4Input = element(by.css('input#field_image4'));
    image5Input = element(by.css('input#field_image5'));
    specificationInput = element(by.css('input#field_specification'));
    categorySelect = element(by.css('select#field_category'));
    madeInSelect = element(by.css('select#field_madeIn'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setProductNameInput = function (productName) {
        this.productNameInput.sendKeys(productName);
    }

    getProductNameInput = function () {
        return this.productNameInput.getAttribute('value');
    }

    setImage1Input = function (image1) {
        this.image1Input.sendKeys(image1);
    }

    getImage1Input = function () {
        return this.image1Input.getAttribute('value');
    }

    setImage2Input = function (image2) {
        this.image2Input.sendKeys(image2);
    }

    getImage2Input = function () {
        return this.image2Input.getAttribute('value');
    }

    setImage3Input = function (image3) {
        this.image3Input.sendKeys(image3);
    }

    getImage3Input = function () {
        return this.image3Input.getAttribute('value');
    }

    setImage4Input = function (image4) {
        this.image4Input.sendKeys(image4);
    }

    getImage4Input = function () {
        return this.image4Input.getAttribute('value');
    }

    setImage5Input = function (image5) {
        this.image5Input.sendKeys(image5);
    }

    getImage5Input = function () {
        return this.image5Input.getAttribute('value');
    }

    setSpecificationInput = function (specification) {
        this.specificationInput.sendKeys(specification);
    }

    getSpecificationInput = function () {
        return this.specificationInput.getAttribute('value');
    }

    categorySelectLastOption = function () {
        this.categorySelect.all(by.tagName('option')).last().click();
    }

    categorySelectOption = function (option) {
        this.categorySelect.sendKeys(option);
    }

    getCategorySelect = function () {
        return this.categorySelect;
    }

    getCategorySelectedOption = function () {
        return this.categorySelect.element(by.css('option:checked')).getText();
    }

    madeInSelectLastOption = function () {
        this.madeInSelect.all(by.tagName('option')).last().click();
    }

    madeInSelectOption = function (option) {
        this.madeInSelect.sendKeys(option);
    }

    getMadeInSelect = function () {
        return this.madeInSelect;
    }

    getMadeInSelectedOption = function () {
        return this.madeInSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
