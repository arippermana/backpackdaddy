import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('Request e2e test', () => {

    let navBarPage: NavBarPage;
    let requestDialogPage: RequestDialogPage;
    let requestComponentsPage: RequestComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Requests', () => {
        navBarPage.goToEntity('request');
        requestComponentsPage = new RequestComponentsPage();
        expect(requestComponentsPage.getTitle()).toMatch(/backpackdaddyApp.request.home.title/);

    });

    it('should load create Request dialog', () => {
        requestComponentsPage.clickOnCreateButton();
        requestDialogPage = new RequestDialogPage();
        expect(requestDialogPage.getModalTitle()).toMatch(/backpackdaddyApp.request.home.createOrEditLabel/);
        requestDialogPage.close();
    });

    it('should create and save Requests', () => {
        requestComponentsPage.clickOnCreateButton();
        requestDialogPage.shippingMethodSelectLastOption();
        requestDialogPage.setRequestDateInput('2000-12-31');
        expect(requestDialogPage.getRequestDateInput()).toMatch('2000-12-31');
        requestDialogPage.setNameInput('name');
        expect(requestDialogPage.getNameInput()).toMatch('name');
        requestDialogPage.setAddressInput('address');
        expect(requestDialogPage.getAddressInput()).toMatch('address');
        requestDialogPage.setProductPriceInput('5');
        expect(requestDialogPage.getProductPriceInput()).toMatch('5');
        requestDialogPage.setProductQuantityInput('5');
        expect(requestDialogPage.getProductQuantityInput()).toMatch('5');
        requestDialogPage.preferencePaymentSelectLastOption();
        requestDialogPage.setTipAmountInput('5');
        expect(requestDialogPage.getTipAmountInput()).toMatch('5');
        requestDialogPage.setCommisionFeeInput('5');
        expect(requestDialogPage.getCommisionFeeInput()).toMatch('5');
        requestDialogPage.setBaggageChargeInput('5');
        expect(requestDialogPage.getBaggageChargeInput()).toMatch('5');
        requestDialogPage.setShippingChargeInput('5');
        expect(requestDialogPage.getShippingChargeInput()).toMatch('5');
        requestDialogPage.setRemarksInput('remarks');
        expect(requestDialogPage.getRemarksInput()).toMatch('remarks');
        requestDialogPage.setIsDraftInput('isDraft');
        expect(requestDialogPage.getIsDraftInput()).toMatch('isDraft');
        requestDialogPage.productSelectLastOption();
        requestDialogPage.userSelectLastOption();
        requestDialogPage.citySelectLastOption();
        requestDialogPage.tipCurrencySelectLastOption();
        requestDialogPage.save();
        expect(requestDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class RequestComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-request div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class RequestDialogPage {
    modalTitle = element(by.css('h4#myRequestLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    shippingMethodSelect = element(by.css('select#field_shippingMethod'));
    requestDateInput = element(by.css('input#field_requestDate'));
    nameInput = element(by.css('input#field_name'));
    addressInput = element(by.css('input#field_address'));
    productPriceInput = element(by.css('input#field_productPrice'));
    productQuantityInput = element(by.css('input#field_productQuantity'));
    preferencePaymentSelect = element(by.css('select#field_preferencePayment'));
    tipAmountInput = element(by.css('input#field_tipAmount'));
    commisionFeeInput = element(by.css('input#field_commisionFee'));
    baggageChargeInput = element(by.css('input#field_baggageCharge'));
    shippingChargeInput = element(by.css('input#field_shippingCharge'));
    remarksInput = element(by.css('input#field_remarks'));
    isDraftInput = element(by.css('input#field_isDraft'));
    productSelect = element(by.css('select#field_product'));
    userSelect = element(by.css('select#field_user'));
    citySelect = element(by.css('select#field_city'));
    tipCurrencySelect = element(by.css('select#field_tipCurrency'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setShippingMethodSelect = function (shippingMethod) {
        this.shippingMethodSelect.sendKeys(shippingMethod);
    }

    getShippingMethodSelect = function () {
        return this.shippingMethodSelect.element(by.css('option:checked')).getText();
    }

    shippingMethodSelectLastOption = function () {
        this.shippingMethodSelect.all(by.tagName('option')).last().click();
    }
    setRequestDateInput = function (requestDate) {
        this.requestDateInput.sendKeys(requestDate);
    }

    getRequestDateInput = function () {
        return this.requestDateInput.getAttribute('value');
    }

    setNameInput = function (name) {
        this.nameInput.sendKeys(name);
    }

    getNameInput = function () {
        return this.nameInput.getAttribute('value');
    }

    setAddressInput = function (address) {
        this.addressInput.sendKeys(address);
    }

    getAddressInput = function () {
        return this.addressInput.getAttribute('value');
    }

    setProductPriceInput = function (productPrice) {
        this.productPriceInput.sendKeys(productPrice);
    }

    getProductPriceInput = function () {
        return this.productPriceInput.getAttribute('value');
    }

    setProductQuantityInput = function (productQuantity) {
        this.productQuantityInput.sendKeys(productQuantity);
    }

    getProductQuantityInput = function () {
        return this.productQuantityInput.getAttribute('value');
    }

    setPreferencePaymentSelect = function (preferencePayment) {
        this.preferencePaymentSelect.sendKeys(preferencePayment);
    }

    getPreferencePaymentSelect = function () {
        return this.preferencePaymentSelect.element(by.css('option:checked')).getText();
    }

    preferencePaymentSelectLastOption = function () {
        this.preferencePaymentSelect.all(by.tagName('option')).last().click();
    }
    setTipAmountInput = function (tipAmount) {
        this.tipAmountInput.sendKeys(tipAmount);
    }

    getTipAmountInput = function () {
        return this.tipAmountInput.getAttribute('value');
    }

    setCommisionFeeInput = function (commisionFee) {
        this.commisionFeeInput.sendKeys(commisionFee);
    }

    getCommisionFeeInput = function () {
        return this.commisionFeeInput.getAttribute('value');
    }

    setBaggageChargeInput = function (baggageCharge) {
        this.baggageChargeInput.sendKeys(baggageCharge);
    }

    getBaggageChargeInput = function () {
        return this.baggageChargeInput.getAttribute('value');
    }

    setShippingChargeInput = function (shippingCharge) {
        this.shippingChargeInput.sendKeys(shippingCharge);
    }

    getShippingChargeInput = function () {
        return this.shippingChargeInput.getAttribute('value');
    }

    setRemarksInput = function (remarks) {
        this.remarksInput.sendKeys(remarks);
    }

    getRemarksInput = function () {
        return this.remarksInput.getAttribute('value');
    }

    setIsDraftInput = function (isDraft) {
        this.isDraftInput.sendKeys(isDraft);
    }

    getIsDraftInput = function () {
        return this.isDraftInput.getAttribute('value');
    }

    productSelectLastOption = function () {
        this.productSelect.all(by.tagName('option')).last().click();
    }

    productSelectOption = function (option) {
        this.productSelect.sendKeys(option);
    }

    getProductSelect = function () {
        return this.productSelect;
    }

    getProductSelectedOption = function () {
        return this.productSelect.element(by.css('option:checked')).getText();
    }

    userSelectLastOption = function () {
        this.userSelect.all(by.tagName('option')).last().click();
    }

    userSelectOption = function (option) {
        this.userSelect.sendKeys(option);
    }

    getUserSelect = function () {
        return this.userSelect;
    }

    getUserSelectedOption = function () {
        return this.userSelect.element(by.css('option:checked')).getText();
    }

    citySelectLastOption = function () {
        this.citySelect.all(by.tagName('option')).last().click();
    }

    citySelectOption = function (option) {
        this.citySelect.sendKeys(option);
    }

    getCitySelect = function () {
        return this.citySelect;
    }

    getCitySelectedOption = function () {
        return this.citySelect.element(by.css('option:checked')).getText();
    }

    tipCurrencySelectLastOption = function () {
        this.tipCurrencySelect.all(by.tagName('option')).last().click();
    }

    tipCurrencySelectOption = function (option) {
        this.tipCurrencySelect.sendKeys(option);
    }

    getTipCurrencySelect = function () {
        return this.tipCurrencySelect;
    }

    getTipCurrencySelectedOption = function () {
        return this.tipCurrencySelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
