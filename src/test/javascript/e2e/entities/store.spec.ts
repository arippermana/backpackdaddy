import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('Store e2e test', () => {

    let navBarPage: NavBarPage;
    let storeDialogPage: StoreDialogPage;
    let storeComponentsPage: StoreComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Stores', () => {
        navBarPage.goToEntity('store');
        storeComponentsPage = new StoreComponentsPage();
        expect(storeComponentsPage.getTitle()).toMatch(/backpackdaddyApp.store.home.title/);

    });

    it('should load create Store dialog', () => {
        storeComponentsPage.clickOnCreateButton();
        storeDialogPage = new StoreDialogPage();
        expect(storeDialogPage.getModalTitle()).toMatch(/backpackdaddyApp.store.home.createOrEditLabel/);
        storeDialogPage.close();
    });

    it('should create and save Stores', () => {
        storeComponentsPage.clickOnCreateButton();
        storeDialogPage.setOfflineShopInput('offlineShop');
        expect(storeDialogPage.getOfflineShopInput()).toMatch('offlineShop');
        storeDialogPage.setOnlineShopInput('onlineShop');
        expect(storeDialogPage.getOnlineShopInput()).toMatch('onlineShop');
        storeDialogPage.setPriceInput('5');
        expect(storeDialogPage.getPriceInput()).toMatch('5');
        storeDialogPage.countrySelectLastOption();
        storeDialogPage.currencySelectLastOption();
        storeDialogPage.productSelectLastOption();
        storeDialogPage.save();
        expect(storeDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class StoreComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-store div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class StoreDialogPage {
    modalTitle = element(by.css('h4#myStoreLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    offlineShopInput = element(by.css('input#field_offlineShop'));
    onlineShopInput = element(by.css('input#field_onlineShop'));
    priceInput = element(by.css('input#field_price'));
    countrySelect = element(by.css('select#field_country'));
    currencySelect = element(by.css('select#field_currency'));
    productSelect = element(by.css('select#field_product'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setOfflineShopInput = function (offlineShop) {
        this.offlineShopInput.sendKeys(offlineShop);
    }

    getOfflineShopInput = function () {
        return this.offlineShopInput.getAttribute('value');
    }

    setOnlineShopInput = function (onlineShop) {
        this.onlineShopInput.sendKeys(onlineShop);
    }

    getOnlineShopInput = function () {
        return this.onlineShopInput.getAttribute('value');
    }

    setPriceInput = function (price) {
        this.priceInput.sendKeys(price);
    }

    getPriceInput = function () {
        return this.priceInput.getAttribute('value');
    }

    countrySelectLastOption = function () {
        this.countrySelect.all(by.tagName('option')).last().click();
    }

    countrySelectOption = function (option) {
        this.countrySelect.sendKeys(option);
    }

    getCountrySelect = function () {
        return this.countrySelect;
    }

    getCountrySelectedOption = function () {
        return this.countrySelect.element(by.css('option:checked')).getText();
    }

    currencySelectLastOption = function () {
        this.currencySelect.all(by.tagName('option')).last().click();
    }

    currencySelectOption = function (option) {
        this.currencySelect.sendKeys(option);
    }

    getCurrencySelect = function () {
        return this.currencySelect;
    }

    getCurrencySelectedOption = function () {
        return this.currencySelect.element(by.css('option:checked')).getText();
    }

    productSelectLastOption = function () {
        this.productSelect.all(by.tagName('option')).last().click();
    }

    productSelectOption = function (option) {
        this.productSelect.sendKeys(option);
    }

    getProductSelect = function () {
        return this.productSelect;
    }

    getProductSelectedOption = function () {
        return this.productSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
