import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';


describe('Track e2e test', () => {

    let navBarPage: NavBarPage;
    let trackDialogPage: TrackDialogPage;
    let trackComponentsPage: TrackComponentsPage;


    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Tracks', () => {
        navBarPage.goToEntity('track');
        trackComponentsPage = new TrackComponentsPage();
        expect(trackComponentsPage.getTitle()).toMatch(/backpackdaddyApp.track.home.title/);

    });

    it('should load create Track dialog', () => {
        trackComponentsPage.clickOnCreateButton();
        trackDialogPage = new TrackDialogPage();
        expect(trackDialogPage.getModalTitle()).toMatch(/backpackdaddyApp.track.home.createOrEditLabel/);
        trackDialogPage.close();
    });

    it('should create and save Tracks', () => {
        trackComponentsPage.clickOnCreateButton();
        trackDialogPage.statusSelectLastOption();
        trackDialogPage.save();
        expect(trackDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class TrackComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-track div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class TrackDialogPage {
    modalTitle = element(by.css('h4#myTrackLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    statusSelect = element(by.css('select#field_status'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    statusSelectLastOption = function () {
        this.statusSelect.all(by.tagName('option')).last().click();
    }

    statusSelectOption = function (option) {
        this.statusSelect.sendKeys(option);
    }

    getStatusSelect = function () {
        return this.statusSelect;
    }

    getStatusSelectedOption = function () {
        return this.statusSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
