import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('TransactionStatus e2e test', () => {

    let navBarPage: NavBarPage;
    let transactionStatusDialogPage: TransactionStatusDialogPage;
    let transactionStatusComponentsPage: TransactionStatusComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load TransactionStatuses', () => {
        navBarPage.goToEntity('transaction-status');
        transactionStatusComponentsPage = new TransactionStatusComponentsPage();
        expect(transactionStatusComponentsPage.getTitle()).toMatch(/backpackdaddyApp.transactionStatus.home.title/);

    });

    it('should load create TransactionStatus dialog', () => {
        transactionStatusComponentsPage.clickOnCreateButton();
        transactionStatusDialogPage = new TransactionStatusDialogPage();
        expect(transactionStatusDialogPage.getModalTitle()).toMatch(/backpackdaddyApp.transactionStatus.home.createOrEditLabel/);
        transactionStatusDialogPage.close();
    });

    it('should create and save TransactionStatuses', () => {
        transactionStatusComponentsPage.clickOnCreateButton();
        transactionStatusDialogPage.setDescriptionInput('description');
        expect(transactionStatusDialogPage.getDescriptionInput()).toMatch('description');
        transactionStatusDialogPage.setFlagIdInput('flagId');
        expect(transactionStatusDialogPage.getFlagIdInput()).toMatch('flagId');
        transactionStatusDialogPage.save();
        expect(transactionStatusDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class TransactionStatusComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-transaction-status div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class TransactionStatusDialogPage {
    modalTitle = element(by.css('h4#myTransactionStatusLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    descriptionInput = element(by.css('input#field_description'));
    flagIdInput = element(by.css('input#field_flagId'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setDescriptionInput = function (description) {
        this.descriptionInput.sendKeys(description);
    }

    getDescriptionInput = function () {
        return this.descriptionInput.getAttribute('value');
    }

    setFlagIdInput = function (flagId) {
        this.flagIdInput.sendKeys(flagId);
    }

    getFlagIdInput = function () {
        return this.flagIdInput.getAttribute('value');
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
