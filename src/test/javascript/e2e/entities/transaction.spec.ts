import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';


describe('Transaction e2e test', () => {

    let navBarPage: NavBarPage;
    let transactionDialogPage: TransactionDialogPage;
    let transactionComponentsPage: TransactionComponentsPage;


    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Transactions', () => {
        navBarPage.goToEntity('transaction');
        transactionComponentsPage = new TransactionComponentsPage();
        expect(transactionComponentsPage.getTitle()).toMatch(/backpackdaddyApp.transaction.home.title/);

    });

    it('should load create Transaction dialog', () => {
        transactionComponentsPage.clickOnCreateButton();
        transactionDialogPage = new TransactionDialogPage();
        expect(transactionDialogPage.getModalTitle()).toMatch(/backpackdaddyApp.transaction.home.createOrEditLabel/);
        transactionDialogPage.close();
    });

    it('should create and save Transactions', () => {
        transactionComponentsPage.clickOnCreateButton();
        transactionDialogPage.trackSelectLastOption();
        transactionDialogPage.requestSelectLastOption();
        transactionDialogPage.tripSelectLastOption();
        transactionDialogPage.save();
        expect(transactionDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class TransactionComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-transaction div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class TransactionDialogPage {
    modalTitle = element(by.css('h4#myTransactionLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    trackSelect = element(by.css('select#field_track'));
    requestSelect = element(by.css('select#field_request'));
    tripSelect = element(by.css('select#field_trip'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    trackSelectLastOption = function () {
        this.trackSelect.all(by.tagName('option')).last().click();
    }

    trackSelectOption = function (option) {
        this.trackSelect.sendKeys(option);
    }

    getTrackSelect = function () {
        return this.trackSelect;
    }

    getTrackSelectedOption = function () {
        return this.trackSelect.element(by.css('option:checked')).getText();
    }

    requestSelectLastOption = function () {
        this.requestSelect.all(by.tagName('option')).last().click();
    }

    requestSelectOption = function (option) {
        this.requestSelect.sendKeys(option);
    }

    getRequestSelect = function () {
        return this.requestSelect;
    }

    getRequestSelectedOption = function () {
        return this.requestSelect.element(by.css('option:checked')).getText();
    }

    tripSelectLastOption = function () {
        this.tripSelect.all(by.tagName('option')).last().click();
    }

    tripSelectOption = function (option) {
        this.tripSelect.sendKeys(option);
    }

    getTripSelect = function () {
        return this.tripSelect;
    }

    getTripSelectedOption = function () {
        return this.tripSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
