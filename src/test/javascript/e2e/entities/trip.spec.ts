import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('Trip e2e test', () => {

    let navBarPage: NavBarPage;
    let tripDialogPage: TripDialogPage;
    let tripComponentsPage: TripComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Trips', () => {
        navBarPage.goToEntity('trip');
        tripComponentsPage = new TripComponentsPage();
        expect(tripComponentsPage.getTitle()).toMatch(/backpackdaddyApp.trip.home.title/);

    });

    it('should load create Trip dialog', () => {
        tripComponentsPage.clickOnCreateButton();
        tripDialogPage = new TripDialogPage();
        expect(tripDialogPage.getModalTitle()).toMatch(/backpackdaddyApp.trip.home.createOrEditLabel/);
        tripDialogPage.close();
    });

    it('should create and save Trips', () => {
        tripComponentsPage.clickOnCreateButton();
        tripDialogPage.setWeightInput('5');
        expect(tripDialogPage.getWeightInput()).toMatch('5');
        tripDialogPage.setNoticeInput('notice');
        expect(tripDialogPage.getNoticeInput()).toMatch('notice');
        tripDialogPage.preferencePaymentSelectLastOption();
        tripDialogPage.setRangeFromInput('5');
        expect(tripDialogPage.getRangeFromInput()).toMatch('5');
        tripDialogPage.setRangeToInput('5');
        expect(tripDialogPage.getRangeToInput()).toMatch('5');
        tripDialogPage.setFixedAmountInput('5');
        expect(tripDialogPage.getFixedAmountInput()).toMatch('5');
        tripDialogPage.setPercentageAmountInput('5');
        expect(tripDialogPage.getPercentageAmountInput()).toMatch('5');
        tripDialogPage.setBaggageInformationInput('baggageInformation');
        expect(tripDialogPage.getBaggageInformationInput()).toMatch('baggageInformation');
        tripDialogPage.setShipDateInput('2000-12-31');
        expect(tripDialogPage.getShipDateInput()).toMatch('2000-12-31');
        tripDialogPage.setRemarksInput('remarks');
        expect(tripDialogPage.getRemarksInput()).toMatch('remarks');
        tripDialogPage.setIsDraftInput('isDraft');
        expect(tripDialogPage.getIsDraftInput()).toMatch('isDraft');
        tripDialogPage.userSelectLastOption();
        tripDialogPage.fromCountrySelectLastOption();
        tripDialogPage.toCountrySelectLastOption();
        tripDialogPage.categorySelectLastOption();
        tripDialogPage.paymentCurrencySelectLastOption();
        tripDialogPage.shippingFromSelectLastOption();
        tripDialogPage.save();
        expect(tripDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class TripComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-trip div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class TripDialogPage {
    modalTitle = element(by.css('h4#myTripLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    weightInput = element(by.css('input#field_weight'));
    noticeInput = element(by.css('input#field_notice'));
    preferencePaymentSelect = element(by.css('select#field_preferencePayment'));
    rangeFromInput = element(by.css('input#field_rangeFrom'));
    rangeToInput = element(by.css('input#field_rangeTo'));
    fixedAmountInput = element(by.css('input#field_fixedAmount'));
    percentageAmountInput = element(by.css('input#field_percentageAmount'));
    baggageInformationInput = element(by.css('input#field_baggageInformation'));
    shipDateInput = element(by.css('input#field_shipDate'));
    remarksInput = element(by.css('input#field_remarks'));
    isDraftInput = element(by.css('input#field_isDraft'));
    userSelect = element(by.css('select#field_user'));
    fromCountrySelect = element(by.css('select#field_fromCountry'));
    toCountrySelect = element(by.css('select#field_toCountry'));
    categorySelect = element(by.css('select#field_category'));
    paymentCurrencySelect = element(by.css('select#field_paymentCurrency'));
    shippingFromSelect = element(by.css('select#field_shippingFrom'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setWeightInput = function (weight) {
        this.weightInput.sendKeys(weight);
    }

    getWeightInput = function () {
        return this.weightInput.getAttribute('value');
    }

    setNoticeInput = function (notice) {
        this.noticeInput.sendKeys(notice);
    }

    getNoticeInput = function () {
        return this.noticeInput.getAttribute('value');
    }

    setPreferencePaymentSelect = function (preferencePayment) {
        this.preferencePaymentSelect.sendKeys(preferencePayment);
    }

    getPreferencePaymentSelect = function () {
        return this.preferencePaymentSelect.element(by.css('option:checked')).getText();
    }

    preferencePaymentSelectLastOption = function () {
        this.preferencePaymentSelect.all(by.tagName('option')).last().click();
    }
    setRangeFromInput = function (rangeFrom) {
        this.rangeFromInput.sendKeys(rangeFrom);
    }

    getRangeFromInput = function () {
        return this.rangeFromInput.getAttribute('value');
    }

    setRangeToInput = function (rangeTo) {
        this.rangeToInput.sendKeys(rangeTo);
    }

    getRangeToInput = function () {
        return this.rangeToInput.getAttribute('value');
    }

    setFixedAmountInput = function (fixedAmount) {
        this.fixedAmountInput.sendKeys(fixedAmount);
    }

    getFixedAmountInput = function () {
        return this.fixedAmountInput.getAttribute('value');
    }

    setPercentageAmountInput = function (percentageAmount) {
        this.percentageAmountInput.sendKeys(percentageAmount);
    }

    getPercentageAmountInput = function () {
        return this.percentageAmountInput.getAttribute('value');
    }

    setBaggageInformationInput = function (baggageInformation) {
        this.baggageInformationInput.sendKeys(baggageInformation);
    }

    getBaggageInformationInput = function () {
        return this.baggageInformationInput.getAttribute('value');
    }

    setShipDateInput = function (shipDate) {
        this.shipDateInput.sendKeys(shipDate);
    }

    getShipDateInput = function () {
        return this.shipDateInput.getAttribute('value');
    }

    setRemarksInput = function (remarks) {
        this.remarksInput.sendKeys(remarks);
    }

    getRemarksInput = function () {
        return this.remarksInput.getAttribute('value');
    }

    setIsDraftInput = function (isDraft) {
        this.isDraftInput.sendKeys(isDraft);
    }

    getIsDraftInput = function () {
        return this.isDraftInput.getAttribute('value');
    }

    userSelectLastOption = function () {
        this.userSelect.all(by.tagName('option')).last().click();
    }

    userSelectOption = function (option) {
        this.userSelect.sendKeys(option);
    }

    getUserSelect = function () {
        return this.userSelect;
    }

    getUserSelectedOption = function () {
        return this.userSelect.element(by.css('option:checked')).getText();
    }

    fromCountrySelectLastOption = function () {
        this.fromCountrySelect.all(by.tagName('option')).last().click();
    }

    fromCountrySelectOption = function (option) {
        this.fromCountrySelect.sendKeys(option);
    }

    getFromCountrySelect = function () {
        return this.fromCountrySelect;
    }

    getFromCountrySelectedOption = function () {
        return this.fromCountrySelect.element(by.css('option:checked')).getText();
    }

    toCountrySelectLastOption = function () {
        this.toCountrySelect.all(by.tagName('option')).last().click();
    }

    toCountrySelectOption = function (option) {
        this.toCountrySelect.sendKeys(option);
    }

    getToCountrySelect = function () {
        return this.toCountrySelect;
    }

    getToCountrySelectedOption = function () {
        return this.toCountrySelect.element(by.css('option:checked')).getText();
    }

    categorySelectLastOption = function () {
        this.categorySelect.all(by.tagName('option')).last().click();
    }

    categorySelectOption = function (option) {
        this.categorySelect.sendKeys(option);
    }

    getCategorySelect = function () {
        return this.categorySelect;
    }

    getCategorySelectedOption = function () {
        return this.categorySelect.element(by.css('option:checked')).getText();
    }

    paymentCurrencySelectLastOption = function () {
        this.paymentCurrencySelect.all(by.tagName('option')).last().click();
    }

    paymentCurrencySelectOption = function (option) {
        this.paymentCurrencySelect.sendKeys(option);
    }

    getPaymentCurrencySelect = function () {
        return this.paymentCurrencySelect;
    }

    getPaymentCurrencySelectedOption = function () {
        return this.paymentCurrencySelect.element(by.css('option:checked')).getText();
    }

    shippingFromSelectLastOption = function () {
        this.shippingFromSelect.all(by.tagName('option')).last().click();
    }

    shippingFromSelectOption = function (option) {
        this.shippingFromSelect.sendKeys(option);
    }

    getShippingFromSelect = function () {
        return this.shippingFromSelect;
    }

    getShippingFromSelectedOption = function () {
        return this.shippingFromSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
