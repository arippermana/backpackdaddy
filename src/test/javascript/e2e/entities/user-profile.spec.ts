import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('UserProfile e2e test', () => {

    let navBarPage: NavBarPage;
    let userProfileDialogPage: UserProfileDialogPage;
    let userProfileComponentsPage: UserProfileComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load UserProfiles', () => {
        navBarPage.goToEntity('user-profile');
        userProfileComponentsPage = new UserProfileComponentsPage();
        expect(userProfileComponentsPage.getTitle()).toMatch(/backpackdaddyApp.userProfile.home.title/);

    });

    it('should load create UserProfile dialog', () => {
        userProfileComponentsPage.clickOnCreateButton();
        userProfileDialogPage = new UserProfileDialogPage();
        expect(userProfileDialogPage.getModalTitle()).toMatch(/backpackdaddyApp.userProfile.home.createOrEditLabel/);
        userProfileDialogPage.close();
    });

   /* it('should create and save UserProfiles', () => {
        userProfileComponentsPage.clickOnCreateButton();
        userProfileDialogPage.setNameInput('name');
        expect(userProfileDialogPage.getNameInput()).toMatch('name');
        userProfileDialogPage.setBirthdateInput('2000-12-31');
        expect(userProfileDialogPage.getBirthdateInput()).toMatch('2000-12-31');
        userProfileDialogPage.getIsAgeShowInput().isSelected().then(function (selected) {
            if (selected) {
                userProfileDialogPage.getIsAgeShowInput().click();
                expect(userProfileDialogPage.getIsAgeShowInput().isSelected()).toBeFalsy();
            } else {
                userProfileDialogPage.getIsAgeShowInput().click();
                expect(userProfileDialogPage.getIsAgeShowInput().isSelected()).toBeTruthy();
            }
        });
        userProfileDialogPage.getIsWorkingOverseaInput().isSelected().then(function (selected) {
            if (selected) {
                userProfileDialogPage.getIsWorkingOverseaInput().click();
                expect(userProfileDialogPage.getIsWorkingOverseaInput().isSelected()).toBeFalsy();
            } else {
                userProfileDialogPage.getIsWorkingOverseaInput().click();
                expect(userProfileDialogPage.getIsWorkingOverseaInput().isSelected()).toBeTruthy();
            }
        });
        userProfileDialogPage.setProfileImageInput('profileImage');
        expect(userProfileDialogPage.getProfileImageInput()).toMatch('profileImage');
        userProfileDialogPage.genderSelectLastOption();
        userProfileDialogPage.nationalitySelectLastOption();
        userProfileDialogPage.currentLocationSelectLastOption();
        userProfileDialogPage.occupationSelectLastOption();
        userProfileDialogPage.languageSelectLastOption();
        userProfileDialogPage.currencySelectLastOption();
        userProfileDialogPage.userSelectLastOption();
        userProfileDialogPage.save();
        expect(userProfileDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); */

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class UserProfileComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-user-profile div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class UserProfileDialogPage {
    modalTitle = element(by.css('h4#myUserProfileLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nameInput = element(by.css('input#field_name'));
    birthdateInput = element(by.css('input#field_birthdate'));
    isAgeShowInput = element(by.css('input#field_isAgeShow'));
    isWorkingOverseaInput = element(by.css('input#field_isWorkingOversea'));
    profileImageInput = element(by.css('input#field_profileImage'));
    genderSelect = element(by.css('select#field_gender'));
    nationalitySelect = element(by.css('select#field_nationality'));
    currentLocationSelect = element(by.css('select#field_currentLocation'));
    occupationSelect = element(by.css('select#field_occupation'));
    languageSelect = element(by.css('select#field_language'));
    currencySelect = element(by.css('select#field_currency'));
    userSelect = element(by.css('select#field_user'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setNameInput = function (name) {
        this.nameInput.sendKeys(name);
    }

    getNameInput = function () {
        return this.nameInput.getAttribute('value');
    }

    setBirthdateInput = function (birthdate) {
        this.birthdateInput.sendKeys(birthdate);
    }

    getBirthdateInput = function () {
        return this.birthdateInput.getAttribute('value');
    }

    getIsAgeShowInput = function () {
        return this.isAgeShowInput;
    }
    getIsWorkingOverseaInput = function () {
        return this.isWorkingOverseaInput;
    }
    setProfileImageInput = function (profileImage) {
        this.profileImageInput.sendKeys(profileImage);
    }

    getProfileImageInput = function () {
        return this.profileImageInput.getAttribute('value');
    }

    genderSelectLastOption = function () {
        this.genderSelect.all(by.tagName('option')).last().click();
    }

    genderSelectOption = function (option) {
        this.genderSelect.sendKeys(option);
    }

    getGenderSelect = function () {
        return this.genderSelect;
    }

    getGenderSelectedOption = function () {
        return this.genderSelect.element(by.css('option:checked')).getText();
    }

    nationalitySelectLastOption = function () {
        this.nationalitySelect.all(by.tagName('option')).last().click();
    }

    nationalitySelectOption = function (option) {
        this.nationalitySelect.sendKeys(option);
    }

    getNationalitySelect = function () {
        return this.nationalitySelect;
    }

    getNationalitySelectedOption = function () {
        return this.nationalitySelect.element(by.css('option:checked')).getText();
    }

    currentLocationSelectLastOption = function () {
        this.currentLocationSelect.all(by.tagName('option')).last().click();
    }

    currentLocationSelectOption = function (option) {
        this.currentLocationSelect.sendKeys(option);
    }

    getCurrentLocationSelect = function () {
        return this.currentLocationSelect;
    }

    getCurrentLocationSelectedOption = function () {
        return this.currentLocationSelect.element(by.css('option:checked')).getText();
    }

    occupationSelectLastOption = function () {
        this.occupationSelect.all(by.tagName('option')).last().click();
    }

    occupationSelectOption = function (option) {
        this.occupationSelect.sendKeys(option);
    }

    getOccupationSelect = function () {
        return this.occupationSelect;
    }

    getOccupationSelectedOption = function () {
        return this.occupationSelect.element(by.css('option:checked')).getText();
    }

    languageSelectLastOption = function () {
        this.languageSelect.all(by.tagName('option')).last().click();
    }

    languageSelectOption = function (option) {
        this.languageSelect.sendKeys(option);
    }

    getLanguageSelect = function () {
        return this.languageSelect;
    }

    getLanguageSelectedOption = function () {
        return this.languageSelect.element(by.css('option:checked')).getText();
    }

    currencySelectLastOption = function () {
        this.currencySelect.all(by.tagName('option')).last().click();
    }

    currencySelectOption = function (option) {
        this.currencySelect.sendKeys(option);
    }

    getCurrencySelect = function () {
        return this.currencySelect;
    }

    getCurrencySelectedOption = function () {
        return this.currencySelect.element(by.css('option:checked')).getText();
    }

    userSelectLastOption = function () {
        this.userSelect.all(by.tagName('option')).last().click();
    }

    userSelectOption = function (option) {
        this.userSelect.sendKeys(option);
    }

    getUserSelect = function () {
        return this.userSelect;
    }

    getUserSelectedOption = function () {
        return this.userSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
