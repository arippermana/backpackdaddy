/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { BackpackdaddyTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { AdmAuditTrailDetailComponent } from '../../../../../../main/webapp/app/entities/adm-audit-trail/adm-audit-trail-detail.component';
import { AdmAuditTrailService } from '../../../../../../main/webapp/app/entities/adm-audit-trail/adm-audit-trail.service';
import { AdmAuditTrail } from '../../../../../../main/webapp/app/entities/adm-audit-trail/adm-audit-trail.model';

describe('Component Tests', () => {

    describe('AdmAuditTrail Management Detail Component', () => {
        let comp: AdmAuditTrailDetailComponent;
        let fixture: ComponentFixture<AdmAuditTrailDetailComponent>;
        let service: AdmAuditTrailService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [BackpackdaddyTestModule],
                declarations: [AdmAuditTrailDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    AdmAuditTrailService,
                    JhiEventManager
                ]
            }).overrideTemplate(AdmAuditTrailDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AdmAuditTrailDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AdmAuditTrailService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new AdmAuditTrail(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.admAuditTrail).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
