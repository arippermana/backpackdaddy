/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { BackpackdaddyTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { AdmRefBranchDetailComponent } from '../../../../../../main/webapp/app/entities/adm-ref-branch/adm-ref-branch-detail.component';
import { AdmRefBranchService } from '../../../../../../main/webapp/app/entities/adm-ref-branch/adm-ref-branch.service';
import { AdmRefBranch } from '../../../../../../main/webapp/app/entities/adm-ref-branch/adm-ref-branch.model';

describe('Component Tests', () => {

    describe('AdmRefBranch Management Detail Component', () => {
        let comp: AdmRefBranchDetailComponent;
        let fixture: ComponentFixture<AdmRefBranchDetailComponent>;
        let service: AdmRefBranchService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [BackpackdaddyTestModule],
                declarations: [AdmRefBranchDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    AdmRefBranchService,
                    JhiEventManager
                ]
            }).overrideTemplate(AdmRefBranchDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AdmRefBranchDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AdmRefBranchService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new AdmRefBranch(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.admRefBranch).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
