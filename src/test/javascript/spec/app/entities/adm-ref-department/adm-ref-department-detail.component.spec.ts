/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { BackpackdaddyTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { AdmRefDepartmentDetailComponent } from '../../../../../../main/webapp/app/entities/adm-ref-department/adm-ref-department-detail.component';
import { AdmRefDepartmentService } from '../../../../../../main/webapp/app/entities/adm-ref-department/adm-ref-department.service';
import { AdmRefDepartment } from '../../../../../../main/webapp/app/entities/adm-ref-department/adm-ref-department.model';

describe('Component Tests', () => {

    describe('AdmRefDepartment Management Detail Component', () => {
        let comp: AdmRefDepartmentDetailComponent;
        let fixture: ComponentFixture<AdmRefDepartmentDetailComponent>;
        let service: AdmRefDepartmentService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [BackpackdaddyTestModule],
                declarations: [AdmRefDepartmentDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    AdmRefDepartmentService,
                    JhiEventManager
                ]
            }).overrideTemplate(AdmRefDepartmentDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AdmRefDepartmentDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AdmRefDepartmentService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new AdmRefDepartment(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.admRefDepartment).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
