/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { BackpackdaddyTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { AdmRefGroupDetailComponent } from '../../../../../../main/webapp/app/entities/adm-ref-group/adm-ref-group-detail.component';
import { AdmRefGroupService } from '../../../../../../main/webapp/app/entities/adm-ref-group/adm-ref-group.service';
import { AdmRefGroup } from '../../../../../../main/webapp/app/entities/adm-ref-group/adm-ref-group.model';

describe('Component Tests', () => {

    describe('AdmRefGroup Management Detail Component', () => {
        let comp: AdmRefGroupDetailComponent;
        let fixture: ComponentFixture<AdmRefGroupDetailComponent>;
        let service: AdmRefGroupService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [BackpackdaddyTestModule],
                declarations: [AdmRefGroupDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    AdmRefGroupService,
                    JhiEventManager
                ]
            }).overrideTemplate(AdmRefGroupDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AdmRefGroupDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AdmRefGroupService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new AdmRefGroup(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.admRefGroup).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
