/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { BackpackdaddyTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { AdmRefPositionDetailComponent } from '../../../../../../main/webapp/app/entities/adm-ref-position/adm-ref-position-detail.component';
import { AdmRefPositionService } from '../../../../../../main/webapp/app/entities/adm-ref-position/adm-ref-position.service';
import { AdmRefPosition } from '../../../../../../main/webapp/app/entities/adm-ref-position/adm-ref-position.model';

describe('Component Tests', () => {

    describe('AdmRefPosition Management Detail Component', () => {
        let comp: AdmRefPositionDetailComponent;
        let fixture: ComponentFixture<AdmRefPositionDetailComponent>;
        let service: AdmRefPositionService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [BackpackdaddyTestModule],
                declarations: [AdmRefPositionDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    AdmRefPositionService,
                    JhiEventManager
                ]
            }).overrideTemplate(AdmRefPositionDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AdmRefPositionDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AdmRefPositionService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new AdmRefPosition(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.admRefPosition).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
