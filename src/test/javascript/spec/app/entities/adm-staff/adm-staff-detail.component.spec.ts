/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { BackpackdaddyTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { AdmStaffDetailComponent } from '../../../../../../main/webapp/app/entities/adm-staff/adm-staff-detail.component';
import { AdmStaffService } from '../../../../../../main/webapp/app/entities/adm-staff/adm-staff.service';
import { AdmStaff } from '../../../../../../main/webapp/app/entities/adm-staff/adm-staff.model';

describe('Component Tests', () => {

    describe('AdmStaff Management Detail Component', () => {
        let comp: AdmStaffDetailComponent;
        let fixture: ComponentFixture<AdmStaffDetailComponent>;
        let service: AdmStaffService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [BackpackdaddyTestModule],
                declarations: [AdmStaffDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    AdmStaffService,
                    JhiEventManager
                ]
            }).overrideTemplate(AdmStaffDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AdmStaffDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AdmStaffService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new AdmStaff(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.admStaff).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
