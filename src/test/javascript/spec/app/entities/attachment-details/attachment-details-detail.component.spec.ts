/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { BackpackdaddyTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { AttachmentDetailsDetailComponent } from '../../../../../../main/webapp/app/entities/attachment-details/attachment-details-detail.component';
import { AttachmentDetailsService } from '../../../../../../main/webapp/app/entities/attachment-details/attachment-details.service';
import { AttachmentDetails } from '../../../../../../main/webapp/app/entities/attachment-details/attachment-details.model';

describe('Component Tests', () => {

    describe('AttachmentDetails Management Detail Component', () => {
        let comp: AttachmentDetailsDetailComponent;
        let fixture: ComponentFixture<AttachmentDetailsDetailComponent>;
        let service: AttachmentDetailsService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [BackpackdaddyTestModule],
                declarations: [AttachmentDetailsDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    AttachmentDetailsService,
                    JhiEventManager
                ]
            }).overrideTemplate(AttachmentDetailsDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AttachmentDetailsDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AttachmentDetailsService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new AttachmentDetails(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.attachmentDetails).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
