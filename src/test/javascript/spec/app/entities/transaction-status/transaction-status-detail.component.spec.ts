/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { BackpackdaddyTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { TransactionStatusDetailComponent } from '../../../../../../main/webapp/app/entities/transaction-status/transaction-status-detail.component';
import { TransactionStatusService } from '../../../../../../main/webapp/app/entities/transaction-status/transaction-status.service';
import { TransactionStatus } from '../../../../../../main/webapp/app/entities/transaction-status/transaction-status.model';

describe('Component Tests', () => {

    describe('TransactionStatus Management Detail Component', () => {
        let comp: TransactionStatusDetailComponent;
        let fixture: ComponentFixture<TransactionStatusDetailComponent>;
        let service: TransactionStatusService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [BackpackdaddyTestModule],
                declarations: [TransactionStatusDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    TransactionStatusService,
                    JhiEventManager
                ]
            }).overrideTemplate(TransactionStatusDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TransactionStatusDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TransactionStatusService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new TransactionStatus(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.transactionStatus).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
